package menus;

import edu.bo.TutorBusiness;

import java.util.Scanner;

/**
 * 导师子页面
 */

public class TutorMenu {

    public static void printMenu() {
        System.out.println("[1]审核学术交流情况");
        System.out.println("[2]填报学生经费数量");
        System.out.println("[3]成果出审认定");

    }
    public static void funSelect(TutorBusiness business) {
        printMenu();

        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();
        while (!(choice == 1 | choice == 2 | choice == 3)) {
            System.out.println("您输入的选项有误，请重新输入!");
            choice = in.nextInt();
        }

        switch (choice) {
            case 1:
                business.testUpdateCheck();
                break;
            case 2:
                business.expense();
                break;
            case 3:
                business.TutorExam();
                break;
        }
    }
}