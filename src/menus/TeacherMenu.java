package menus;

import edu.dao.course.CourseDaoImpl;
import edu.subsystem.AssistantSystem;
import edu.vo.Course;
import edu.vo.Teacher;

import java.util.Scanner;

/**
 * 授课教师子页面
 */
public class TeacherMenu {
    private final Teacher teacher;

    public TeacherMenu(Teacher teacher) {
        this.teacher = teacher;
    }


    /**
     * 打印授课教师页面
     */
    public  void printMenu(Teacher teacher) {
        System.out.println("[1] 录入课程信息");
        System.out.println("[2] 选择课程助教");
        System.out.println("[3] 助教工作评价");

        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();
        while (!(choice == 1 | choice == 2 | choice == 3)) {
            System.out.println("您输入的选项有误，请重新输入!");
            choice = in.nextInt();
        }

        if (choice == 1) {
           entryCourse(teacher);

        } else if (choice == 2) {
            AssistantSystem assistantSystem=new AssistantSystem();
            assistantSystem.selectAssistant(teacher);
        } else {
            AssistantSystem assistantSystem=new AssistantSystem();
            assistantSystem.fillAssessment(teacher);

        }

    }

    /**
     * 录入课程信息
     */
    public static void entryCourse(Teacher teacher) {
        Scanner in = new Scanner(System.in);
        System.out.println("请输入需要助教的课程信息，形式如下:");
        System.out.println("课程号,课程名,授课人数,学科,授课对象,课程性质,授课时间,课程学时");
        //K001,数据库系统,113,计算机,本科生,必修课,2022-9-1,64
        String[] courseInfo = in.next().split(",");
        Course course = new Course(courseInfo[0], courseInfo[1], Integer.parseInt(courseInfo[2]), courseInfo[3], courseInfo[4], courseInfo[5], teacher.getId(),courseInfo[7],
                Integer.parseInt(courseInfo[8]));
        // TODO:构造Assistant和course对象并入库
        CourseDaoImpl cdi=new CourseDaoImpl();
        cdi.addCourse(course);
    }
}
