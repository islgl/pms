package menus;

import edu.bo.ManagerExam;
import edu.dao.DaoFactory;
import edu.searchcriteria.PostgraduateSearchCriteria;
import edu.searchcriteria.SubjectLeaderSearchCriteria;
import edu.searchcriteria.TeacherSearchCriteria;
import edu.searchcriteria.TutorSearchCriteria;
import edu.vo.*;
import users.UserManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ManagerMenu {
    public static void Administrator_Menu(String userid){
        System.out.println("[1]建立基本信息");
        System.out.println("[2]建立登录信息");
        System.out.println("[3]建立师生关系");
        System.out.println("[4]录入导师项目");
        System.out.println("[5]终审成果");
        System.out.println("[6]退出");
        Scanner in=new Scanner(System.in);
        String choice=in.next();

        while (!(choice.equals("1") | choice.equals("2") | choice.equals("3") | choice.equals("4") | choice.equals("5") | choice.equals("6"))) {
            System.out.println("您输入的选项有误!请重新输入:");
            choice = in.next();
        }

        if (choice.equals("1")) {
            Administrator_InfoMenu(userid);
        } else if (choice.equals("2")) {
            Administrator_LoginMenu(userid);
        } else if (choice.equals("3")) {
            Administrator_Tutor_student(userid);
        } else if (choice.equals("4")) {
            Tutor_ProjectMenu(userid);
        } else if (choice.equals("5")) {
            Check_AchievementMenu(userid);
        }else {
            System.out.println("欢迎下次使用，再见!");
            System.exit(0);
        }

    }
    public static void Administrator_InfoMenu(String userid){
        System.out.println("[1]建立学科负责人的基本信息");
        System.out.println("[2]建立授课教师的基本信息");
        System.out.println("[3]建立导师的基本信息");
        System.out.println("[4]建立学生的基本信息");
        System.out.println("[5]退出");
        Scanner in=new Scanner(System.in);
        String choice=in.next();

        while (!(choice.equals("1") | choice.equals("2") | choice.equals("3") | choice.equals("4") | choice.equals("5"))) {
            System.out.println("您输入的选项有误!请重新输入:");
            choice = in.next();
        }
        if (choice.equals("5")) {
            Administrator_Menu(userid);
        } else {
            Administrator_Sub_InfoMenu(userid, choice);
        }

    }

    public static void Administrator_Sub_InfoMenu(String userid, String cho){
        System.out.println("[1]增添信息");
        System.out.println("[2]删除信息");
        System.out.println("[3]修改信息");
        System.out.println("[4]查询信息");
        System.out.println("[5]退出");
        Scanner in=new Scanner(System.in);
        String choice=in.next();
        while (!(choice.equals("1") | choice.equals("2") | choice.equals("3") | choice.equals("4") | choice.equals("5"))) {
            System.out.println("您输入的选项有误!请重新输入:");
            choice = in.next();
        }

        if(cho.equals("1")){//学科负责人 TODO:学科负责人的增删改查

            String id = null;
            String name = null;
            String subject = null;
            if (choice.equals("1")) {//增添
                SubjectLeader subjectLeader = new SubjectLeader();
                //id
                System.out.println("请输入学科负责人的id");
                id = in.next();
                User user = DaoFactory.getUserDao().get(id);
                if(user!=null){
                    System.out.println("录入失败,已存在该编号");
                }else{
                    subjectLeader.setSubjectleader_no(id);
                    //name
                    System.out.println("请输入学科负责人的姓名");
                    name = in.next();
                    subjectLeader.setSubjectleader_name(name);
                    //subject
                    System.out.println("请输入学科负责人的学科编号");
                    subject = in.next();
                    //TODO:判断是否存在该学科
                    Subject subJect = DaoFactory.getInstance().getSubjectDao().get(subject);
                    while(subJect == null){
                        System.out.println("不存在该专业,请重新输入:");
                        subject = in.next();
                        subJect = DaoFactory.getInstance().getSubjectDao().get(subject);
                    }
                    subjectLeader.setSubject(subject);
                    try {
                        DaoFactory.getSubjectLeaderDao().addSubjectLeader(subjectLeader);
                        //创建该学科负责人账号
                        UserManager.register(id,id,String.valueOf(cho));
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if(choice.equals("2")){//删除
                SubjectLeaderSearchCriteria searchCriteria = new SubjectLeaderSearchCriteria();
                List<SubjectLeader> subjectLeader = DaoFactory.getSubjectLeaderDao().findSubjectLeaders(searchCriteria);
                for(SubjectLeader subjectleader : subjectLeader){
                    System.out.println(subjectleader.toString());
                }
                //id
                System.out.println("请输入删除的学科负责人的编号");
                id = in.next();
                SubjectLeader subjectleader = DaoFactory.getSubjectLeaderDao().getSubjectLeader(id);
                if(subjectleader == null){
                    System.out.println("查无此学科负责人!");
                }
                else {
                    DaoFactory.getSubjectLeaderDao().deleteSubjectLeader(id);
                    //删除该学科负责人账号
                    DaoFactory.getUserDao().delete(id);
                }
            } else if(choice.equals("3")){//修改
                SubjectLeaderSearchCriteria searchCriteria = new SubjectLeaderSearchCriteria();
                List<SubjectLeader> SubjectLeader = DaoFactory.getSubjectLeaderDao().findSubjectLeaders(searchCriteria);
                for(SubjectLeader subjectleader : SubjectLeader){
                    System.out.println(subjectleader.toString());
                }
                //id
                System.out.println("请输入修改的学科负责人的编号");
                id = in.next();
                SubjectLeader subjectLeader = DaoFactory.getSubjectLeaderDao().getSubjectLeader(id);
                if(subjectLeader == null){
                    System.out.println("查无此学科负责人!");
                }
                else {
                    System.out.println(subjectLeader.toString());

                    //具体修改字段
                    System.out.println("请输入修改的学科负责人的对应属性");
                    System.out.println("[1]姓名");
                    System.out.println("[2]学科编号");
                    System.out.println("[3]退出");
                    String choice1 = in.next();
                    char[] ch = choice1.toCharArray();
                    for (int i = 0; i < ch.length; i++) {
                        if (!(ch[i] == '1' | ch[i] == '2' | ch[i] == '3')) {
                            System.out.println("您输入的选项有误!请重新输入:");
                            choice1 = in.next();
                            ch = choice1.toCharArray();
                            break;
                        }
                    }

                    for (int i = 0; i < ch.length; i++) {
                        if (ch[i] == '1') {
                            System.out.println("请输入修改的学科负责人的姓名");
                            name = in.next();
                            subjectLeader.setSubjectleader_name(name);
                        } else if (ch[i] == '2') {
                            System.out.println("请输入修改的学科负责人的专业编号");
                            subject = in.next();
                            Subject subJect = DaoFactory.getInstance().getSubjectDao().get(subject);
                            while(subJect == null){
                                System.out.println("不存在该专业,请重新输入:");
                                subject = in.next();
                                subJect = DaoFactory.getInstance().getSubjectDao().get(subject);
                            }
                            subjectLeader.setSubject(subject);
                        } else {
                            Administrator_Sub_InfoMenu(userid, cho);
                        }
                    }
                    try {
                        DaoFactory.getSubjectLeaderDao().updateSubjectLeader(id, subjectLeader);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if(choice.equals("4")){//查询
                System.out.println("[1]查询全部信息");
                System.out.println("[2]查询部分信息");
                String choose=in.next();
                while (!(choose.equals("1") | choose.equals("2"))) {
                    System.out.println("您输入的选项有误!请重新输入:");
                    choose = in.next();
                }
                if(choose.equals("1")){
                    SubjectLeaderSearchCriteria searchCriteria = new SubjectLeaderSearchCriteria();
                    List<SubjectLeader> SubjectLeader = DaoFactory.getSubjectLeaderDao().findSubjectLeaders(searchCriteria);
                    for(SubjectLeader subjectleader : SubjectLeader){
                        System.out.println(subjectleader.toString());
                    }
                } else {
                    //id
                    System.out.println("请输入查询的学科负责人的id");
                    id = in.next();
                    SubjectLeader subjectLeader = DaoFactory.getSubjectLeaderDao().getSubjectLeader(id);
                    if (subjectLeader == null) {
                        System.out.println("查无此学科负责人");
                    }
                    else {
                        System.out.println(subjectLeader.toString());
                    }
                }
            } else {
                Administrator_InfoMenu(userid);
            }
        } else if (cho.equals("2")) {//授课教师 TODO:授课教授的增删改查

            String id = null;
            String name = null;
            String sex = null;
            int age = 0;
            if (choice.equals("1")) {//增添
                Teacher teacher = new Teacher();
                //id
                System.out.println("请输入授课教师的id");
                id = in.next();
                User user = DaoFactory.getUserDao().get(id);
                if(user!=null){
                    System.out.println("录入失败,已存在该id");
                }else {
                    teacher.setId(id);
                    //name
                    System.out.println("请输入授课教师的姓名");
                    name = in.next();
                    teacher.setName(name);
                    //sex
                    System.out.println("请输入授课教师的性别");
                    sex = in.next();
                    while(!(sex.equals("男") | sex.equals("女"))){
                        System.out.println("输入有误，请输入’男‘或‘女’:");
                        sex = in.next();
                    }
                    teacher.setSex(sex);
                    //age
                    System.out.println("请输入授课教师的年龄");
                    age = in.nextInt();
                    teacher.setAge(age);
                    try {
                        DaoFactory.getTeacherDao().addTeacher(teacher);
                        System.out.println("增添成功!");
                        //创建该授课教师账号
                        UserManager.register(id,id,String.valueOf(cho));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if(choice.equals("2")){//删除
                //id
                System.out.println("请输入删除的授课教师的id");
                id = in.next();
                Teacher teacher = DaoFactory.getTeacherDao().getTeacher(id);
                if(teacher == null){
                    System.out.println("查无此授课教师");
                }
                else {
                    DaoFactory.getTeacherDao().deleteTeacher(id);
                    System.out.println("删除成功!");
                    //删除该授课教师账号
                    DaoFactory.getUserDao().delete(id);
                }
            } else if(choice.equals("3")){//修改
                //id
                System.out.println("请输入修改的授课教师的id");
                id = in.next();
                Teacher teacher = DaoFactory.getTeacherDao().getTeacher(id);
                if(teacher == null){
                    System.out.println("查无此授课教师");
                }
                else {
                    System.out.println(teacher.toString());

                    //具体修改字段
                    System.out.println("请输入修改的授课教师的对应属性");
                    System.out.println("[1]姓名");
                    System.out.println("[2]性别");
                    System.out.println("[3]年龄");
                    System.out.println("[4]退出");
                    String choice1 = in.next();
                    char[] ch = choice1.toCharArray();
                    for (int i = 0; i < ch.length; i++) {
                        if (!(ch[i] == '1' | ch[i] == '2' | ch[i] == '3' | ch[i] == '4')) {
                            System.out.println("您输入的选项有误!请重新输入:");
                            choice1 = in.next();
                            ch = choice1.toCharArray();
                            break;
                        }
                    }

                    for (int i = 0; i < ch.length; i++) {
                        if (ch[i] == '1') {
                            System.out.println("请输入修改的授课教师的姓名");
                            name = in.next();
                            teacher.setName(name);
                        } else if (ch[i] == '2') {
                            System.out.println("请输入修改的授课教师的性别");
                            sex = in.next();
                            while(!(sex.equals("男") | sex.equals("女"))){
                                System.out.println("输入有误，请输入’男‘或‘女’:");
                                sex = in.next();
                            }
                            teacher.setSex(sex);
                        } else if (ch[i] == '3') {
                            System.out.println("请输入修改的授课教师的年龄");
                            age = in.nextInt();
                            teacher.setAge(age);
                        } else {
                            Administrator_Sub_InfoMenu(userid, cho);
                        }
                    }
                    try {
                        DaoFactory.getTeacherDao().updateTeacher(id, teacher);
                        System.out.println("修改成功!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if(choice.equals("4")){//查询
                System.out.println("[1]查询全部信息");
                System.out.println("[2]查询部分信息");
                String choose=in.next();
                while (!(choose.equals("1") | choose.equals("2"))) {
                    System.out.println("您输入的选项有误!请重新输入:");
                    choose = in.next();
                }
                if(choose.equals("1")){
                    TeacherSearchCriteria searchCriteria = new TeacherSearchCriteria();
                    List<Teacher> Teacher = DaoFactory.getTeacherDao().findTeachers(searchCriteria);
                    for(Teacher teacher : Teacher){
                        System.out.println(teacher.toString());
                    }
                } else {
                    //id
                    System.out.println("请输入查询的授课教师的id");
                    id = in.next();
                    Teacher teacher = DaoFactory.getTeacherDao().getTeacher(id);
                    if (teacher == null) {
                        System.out.println("查无此授课教师");
                    } else {
                        System.out.println(teacher.toString());
                    }
                }
            } else {
                Administrator_InfoMenu(userid);
            }
        } else if (cho.equals("3")) {//导师 TODO:导师的增删改查
            String id = null;
            String name = null;
            String sex = null;
            String project = null;
            String major = null;
            if (choice.equals("1")) {//增添
                Tutor tutor = new Tutor();
                //id
                System.out.println("请输入导师的id");
                id = in.next();
                User user = DaoFactory.getUserDao().get(id);
                if(user!=null){
                    System.out.println("录入失败,已存在该id");
                }else {
                    tutor.setTutor_no(id);
                    //name
                    System.out.println("请输入导师的姓名");
                    name = in.next();
                    tutor.setTutor_name(name);
                    //sex
                    System.out.println("请输入导师的性别");
                    sex = in.next();
                    while(!(sex.equals("男") | sex.equals("女"))){
                        System.out.println("输入有误，请输入’男‘或‘女’:");
                        sex = in.next();
                    }
                    tutor.setTutor_sex(sex);
                    //project
                    System.out.println("请输入导师的专业");
                    major = in.next();
                    Subject subject = DaoFactory.getInstance().getSubjectDao().get(major);
                    while(subject == null){
                        System.out.println("不存在该专业,请重新输入:");
                        major = in.next();
                        subject = DaoFactory.getInstance().getSubjectDao().get(major);
                    }
                    tutor.setTutor_major(major);
                    try {
                        DaoFactory.getTutorDao().addTutor(tutor);
                        //创建该导师账号
                        UserManager.register(id,id,String.valueOf(cho));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if(choice.equals("2")){//删除
                //id
                System.out.println("请输入删除的导师的id");
                id = in.next();
                Tutor tutor = DaoFactory.getTutorDao().getTutor(id);
                System.out.println(tutor.toString());
                if(tutor == null){
                    System.out.println("查无此导师");
                }
                else {
                    DaoFactory.getTutorDao().deleteTutor(id);
                    //删除该导师账号
                    DaoFactory.getUserDao().delete(id);
                }
            } else if(choice.equals("3")) {//修改
                //id
                System.out.println("请输入修改的导师的id");
                id = in.next();
                Tutor tutor = DaoFactory.getTutorDao().getTutor(id);
                if (tutor == null) {
                    System.out.println("查无此导师");
                } else {
                    System.out.println(tutor.toString());

                    //具体修改字段
                    System.out.println("请输入修改的导师的对应属性");
                    System.out.println("[1]姓名");
                    System.out.println("[2]性别");
                    System.out.println("[3]专业");
                    System.out.println("[4]退出");
                    String choice1 = in.next();
                    char[] ch = choice1.toCharArray();
                    for (int i = 0; i < ch.length; i++) {
                        if (!(ch[i] == '1' | ch[i] == '2' | ch[i] == '3' | ch[i] == '4')) {
                            System.out.println("您输入的选项有误!请重新输入:");
                            choice1 = in.next();
                            ch = choice1.toCharArray();
                            break;
                        }
                    }

                    for (int i = 0; i < ch.length; i++) {
                        if (ch[i] == '1') {
                            System.out.println("请输入修改的导师的姓名");
                            name = in.next();
                            tutor.setTutor_name(name);
                        } else if (ch[i] == '2') {
                            System.out.println("请输入修改的导师的性别");
                            sex = in.next();
                            while(!(sex.equals("男") | sex.equals("女"))){
                                System.out.println("输入有误，请输入’男‘或‘女’:");
                                sex = in.next();
                            }
                            tutor.setTutor_sex(sex);
                        } else if (ch[i] == '3') {
                            System.out.println("请输入修改的导师的专业编号");
                            major = in.next();
                            Subject subject = DaoFactory.getInstance().getSubjectDao().get(major);
                            while(subject == null){
                                System.out.println("不存在该专业,请重新输入:");
                                major = in.next();
                                subject = DaoFactory.getInstance().getSubjectDao().get(major);
                            }
                            tutor.setTutor_major(major);
                        } else {
                            Administrator_Sub_InfoMenu(userid, cho);
                        }
                    }
                    try {
                        DaoFactory.getTutorDao().updateTutor(tutor, id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if(choice.equals("4")){//查询
                System.out.println("[1]查询全部信息");
                System.out.println("[2]查询部分信息");
                String choose=in.next();
                while (!(choose.equals("1") | choose.equals("2"))) {
                    System.out.println("您输入的选项有误!请重新输入:");
                    choose = in.next();
                }
                if(choose.equals("1")) {
                    TutorSearchCriteria searchCriteria = new TutorSearchCriteria();
                    List<Tutor> Tutor = DaoFactory.getTutorDao().findTutors(searchCriteria);
                    for (Tutor tutor : Tutor) {
                        System.out.println(tutor.toString());
                    }
                }
                else {
                    //id
                    System.out.println("请输入查询的导师的id");
                    id = in.next();
                    Tutor tutor = DaoFactory.getTutorDao().getTutor(id);
                    if (tutor == null) {
                        System.out.println("查无此导师");
                    } else {
                        System.out.println(tutor.toString());
                    }
                }
            } else {
                Administrator_InfoMenu(userid);
            }
        } else if (cho.equals("4")) {//学生 TODO:学生的增删改查

            String id = null;
            String name = null;
            String major = null;
            String[] applications = new String[2];
            /**
             * 研究生类型
             * 1 博士
             * 2 学硕
             * 3 专硕
             */
            int type;
            /**
             * 是否已分配助教工作
             * 0 未分配
             * 1 已分配
             */
            int allot;

            if (choice.equals("1")) {//增添
                Postgraduate postgraduate = new Postgraduate();
                //id
                System.out.println("请输入学生的id");
                id = in.next();
                User user = DaoFactory.getUserDao().get(id);
                if(user!=null){
                    System.out.println("录入失败,已存在该id");
                }else {
                    postgraduate.setId(id);
                    //name
                    System.out.println("请输入学生的姓名");
                    name = in.next();
                    postgraduate.setName(name);
                    //major
                    System.out.println("请输入学生的专业");
                    major = in.next();
                    Subject subject = DaoFactory.getInstance().getSubjectDao().get(major);
                    while(subject == null){
                        System.out.println("不存在该专业,请重新输入:");
                        major = in.next();
                        subject = DaoFactory.getInstance().getSubjectDao().get(major);
                    }
                    postgraduate.setMajor(major);
                    //助教志愿 新建学生未开始助教选择，志愿默认为无
                    applications[0] = "无";
                    applications[1] = "无";
                    //研究生类型
                    /**
                     * 研究生类型
                     * 1 博士
                     * 2 学硕
                     * 3 专硕
                     */
                    System.out.println("请输入学生的研究生类型(1: 博士, 2: 学硕, 3: 专硕):");
                    type = in.nextInt();
                    postgraduate.setType(type);
                    //是否已分配助教工作
                    /**
                     * 是否已分配助教工作
                     * 0 未分配
                     * 1 已分配
                     */
                    //未开始助教选择，默认未分配
                    postgraduate.setAllot(0);
                    try {
                        DaoFactory.getPostgraduateDao().add(postgraduate);
                        System.out.println("增添成功!");
                        //创建该学生账号
                        UserManager.register(id,id,String.valueOf(cho));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if(choice.equals("2")){//删除
                //id
                System.out.println("请输入删除的学生的id");
                id = in.next();
                Postgraduate postgraduate = DaoFactory.getPostgraduateDao().get(id);
                if(postgraduate == null){
                    System.out.println("查无此学生");
                }
                else {
                    DaoFactory.getPostgraduateDao().delete(id);
                    System.out.println("删除成功!");
                    //删除该学生账号
                    DaoFactory.getUserDao().delete(id);
                }
            } else if(choice.equals("3")){//修改
                //id
                System.out.println("请输入修改的学生的id");
                id = in.next();
                Postgraduate postgraduate = DaoFactory.getPostgraduateDao().get(id);
                if(postgraduate == null){
                    System.out.println("查无此学生");
                }
                else {
                    System.out.println(postgraduate.toString());

                    //具体修改字段
                    System.out.println("请输入修改的授课教师的对应属性");
                    System.out.println("[1]姓名");
                    System.out.println("[2]专业");
                    System.out.println("[3]助教志愿1");
                    System.out.println("[4]助教志愿2");
                    System.out.println("[5]研究生类型");
                    System.out.println("[6]是否已分配助教工作");
                    System.out.println("[7]退出");
                    String choice1 = in.next();
                    char[] ch = choice1.toCharArray();
                    for (int i = 0; i < ch.length; i++) {
                        if (!(ch[i] == '1' | ch[i] == '2' | ch[i] == '3' | ch[i] == '4' | ch[i] == '5' | ch[i] == '6' | ch[i] == '7')) {
                            System.out.println("您输入的选项有误!请重新输入:");
                            choice1 = in.next();
                            ch = choice1.toCharArray();
                            break;
                        }
                    }
                    String id_new = null;
                    for (int i = 0; i < ch.length; i++) {
                        if (ch[i] == '1') {
                            System.out.println("请输入修改的学生的姓名");
                            name = in.next();
                            postgraduate.setName(name);
                        } else if (ch[i] == '2') {
                            System.out.println("请输入修改的学生的专业");
                            major = in.next();
                            Subject subject = DaoFactory.getInstance().getSubjectDao().get(major);
                            while(subject == null){
                                System.out.println("不存在该专业,请重新输入:");
                                major = in.next();
                                subject = DaoFactory.getInstance().getSubjectDao().get(major);
                            }
                            postgraduate.setMajor(major);
                        } else if (ch[i] == '3') {
                            System.out.println("请输入修改的学生的助教志愿1");
                            applications[0] = in.next();
                            Course course = DaoFactory.getCourseDao().getCourse(applications[0]);
                            while(course == null){
                                System.out.println("暂无此课程，请重新输入:");
                                applications[0] = in.next();
                                course = DaoFactory.getCourseDao().getCourse(applications[0]);
                            }
                            postgraduate.setApplications(applications);
                        } else if (ch[i] == '4') {
                            System.out.println("请输入修改的学生的助教志愿2");
                            applications[1] = in.next();
                            Course course = DaoFactory.getCourseDao().getCourse(applications[1]);
                            while(course == null){
                                System.out.println("暂无此课程，请重新输入:");
                                applications[0] = in.next();
                                course = DaoFactory.getCourseDao().getCourse(applications[1]);
                            }
                            postgraduate.setApplications(applications);
                        } else if (ch[i] == '5') {
                            System.out.println("请输入修改的学生的研究生类型");
                            type = in.nextInt();
                            while(!(type == 1 | type == 2 | type == 3)){
                                System.out.println("输入非法，请选择对应序号(1: 博士, 2: 学硕, 3: 专硕):");
                                type = in.nextInt();
                            }
                            postgraduate.setType(type);
                        } else if (ch[i] == '6') {
                            System.out.println("请输入修改的学生是否已分配助教工作");
                            allot = in.nextInt();
                            while(!(allot == 1 | allot == 2)){
                                System.out.println("输入非法，请选择对应序号(0: 未分配, 1: 已分配):");
                                allot = in.nextInt();
                            }
                            postgraduate.setAllot(allot);
                        } else {
                            Administrator_Sub_InfoMenu(userid, cho);
                        }
                    }
                    try {
                        DaoFactory.getPostgraduateDao().update(id, postgraduate);
                        System.out.println("修改成功!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if(choice.equals("4")){//查询
                System.out.println("[1]查询全部信息");
                System.out.println("[2]查询部分信息");
                String choose=in.next();
                while (!(choose.equals("1") | choose.equals("2"))) {
                    System.out.println("您输入的选项有误!请重新输入:");
                    choose = in.next();
                }
                if(choose.equals("1")) {
                    PostgraduateSearchCriteria searchCriteria = new PostgraduateSearchCriteria();
                    List<Postgraduate> Postgraduate = DaoFactory.getPostgraduateDao().find_AND(searchCriteria);
                    for (Postgraduate postgraduate : Postgraduate) {
                        System.out.println(postgraduate.toString());
                    }
                }
                else {
                    //id
                    System.out.println("请输入查询的学生的id");
                    id = in.next();
                    Postgraduate postgraduate = DaoFactory.getPostgraduateDao().get(id);
                    if(postgraduate == null){
                        System.out.println("查无此学生");
                    }
                    else {
                        System.out.println(postgraduate.toString());
                    }
                }
            } else {
                Administrator_InfoMenu(userid);
            }
        }
        Administrator_Sub_InfoMenu(userid, cho);
    }

    public static void Administrator_LoginMenu(String userid){
        //TODO:账号的删改 增在建立基本信息时使用
        //System.out.println("[1]新建账号");
        System.out.println("[1]删除账号");
        System.out.println("[2]修改账号");
        System.out.println("[3]退出");
        Scanner in=new Scanner(System.in);
        String choice=in.next();

        while (!(choice.equals("1") | choice.equals("2") | choice.equals("3"))) {
            System.out.println("您输入的选项有误!请重新输入:");
            choice = in.next();
        }
        String id = null;
        String password = null;
        String roleCode = null;
         if (choice.equals("1")) {//删除
            //id
            System.out.println("请输入删除的账号的id");
            id = in.next();
            User user = DaoFactory.getUserDao().get(id);
            if(user == null){
                System.out.println("查无此账号");
            }
            else {
                SubjectLeader subjectLeader = DaoFactory.getSubjectLeaderDao().getSubjectLeader(id);
                if(subjectLeader != null){
                    DaoFactory.getSubjectLeaderDao().deleteSubjectLeader(id);
                }
                Teacher teacher = DaoFactory.getTeacherDao().getTeacher(id);
                if(teacher != null){
                    DaoFactory.getTeacherDao().deleteTeacher(id);
                }
                Tutor tutor = DaoFactory.getTutorDao().getTutor(id);
                if(tutor != null){
                    DaoFactory.getTutorDao().deleteTutor(id);
                }
                Postgraduate postgraduate = DaoFactory.getPostgraduateDao().get(id);
                if(postgraduate != null){
                    DaoFactory.getPostgraduateDao().delete(id);
                }
                DaoFactory.getUserDao().delete(id);
            }
        } else if (choice.equals("2")) {//修改
            //id
            System.out.println("请输入修改的账号的id");
            id = in.next();
            User user = DaoFactory.getUserDao().get(id);
            if(user == null){
                System.out.println("查无此账号");
            }
            else {
                //具体修改字段
                System.out.println("请输入修改的账号的对应属性");
                System.out.println("[1]密码");
                System.out.println("[2]职位");
                System.out.println("[3]退出");
                String choice1 = in.next();
                char[] ch = choice1.toCharArray();
                for (int i = 0; i < ch.length; i++) {
                    if (!(ch[i] == '1' | ch[i] == '2' | ch[i] == '3')) {
                        System.out.println("您输入的选项有误!请重新输入:");
                        choice1 = in.next();
                        ch = choice1.toCharArray();
                        break;
                    }
                }

                for (int i = 0; i < ch.length; i++) {
                    if (ch[i] == '1') {
                        System.out.println("请输入修改的账号的密码");
                        password = in.next();
                        // 对密码进行加密
                        password = UserManager.encryption(password);
                        user.setPassword(password);
                    } else if (ch[i] == '2') {
                        System.out.println("请输入修改的账号的职位");
                        roleCode = in.next();
                        char[] rolecode = roleCode.toCharArray();
                        Boolean flag = true;
                        for(char code : rolecode){
                            if(!(code=='1' | code=='2' | code=='3' | code=='4' | code=='5')){
                                System.out.println("非法输入");
                                flag = false;
                                break;
                            }
                        }
                        if(flag) {
                            user.setRoleCode(roleCode);
                        }
                    } else {
                        Administrator_Menu(userid);
                    }
                }
                try {
                    DaoFactory.getUserDao().update(id, user);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Administrator_LoginMenu(userid);
        }
    }
    
    public static void Administrator_Tutor_student(String userid) {
        PostgraduateSearchCriteria postgraduateSearchCriteria = new PostgraduateSearchCriteria(null, null, null, null, null, null, -1, -1);
        List<Postgraduate> postgraduates = DaoFactory.getPostgraduateDao().find_AND(postgraduateSearchCriteria);
        //显示所有学生信息
        for (Postgraduate postgraduate : postgraduates) {
            System.out.println(postgraduate.toString());
        }
        System.out.println("[1]建立导师学生关系");
        System.out.println("[2]退出");
        Scanner in = new Scanner(System.in);
        String choice = in.next();
        while (!(choice.equals("1") | choice.equals("2"))) {
            System.out.println("您输入的选项有误!请重新输入:");
            choice = in.next();
        }
        while(choice.equals("1")) {
            System.out.println("请输入录入的学生学号:");

            String id = in.next();
            Postgraduate postgraduate = DaoFactory.getPostgraduateDao().get(id);
            while (postgraduate == null) {
                System.out.println("查无此学生，请重新输入:");
                id = in.next();
            }
            System.out.println("请输入学生导师编号:");
            String tutorid = in.next();
            Tutor tutor = DaoFactory.getTutorDao().getTutor(tutorid);
            while (tutor == null) {
                System.out.println("查无此导师，请重新输入:");
                tutorid = in.next();
            }

            postgraduate.setTutorid(tutorid);
            DaoFactory.getPostgraduateDao().update(id, postgraduate);
            System.out.println("建立成功");
            //询问用户是否继续输入
            System.out.println("输入1继续建立，任意键退出");
            choice = in.next();
        }
    }

    public static void Tutor_ProjectMenu(String userid){
        Scanner in=new Scanner(System.in);
        System.out.println("请输入导师编号：");
        String tid;
        tid = in.next();

        Tutor tutor = DaoFactory.getTutorDao().getTutor(tid);

        if (tutor.getTutor_no() == null){
            System.out.println("没有该导师，请重新输入：");
            tid = in.next();
        }
        Project project = DaoFactory.getProjectDao().getProject_tutor(tid);
        if (project.getTid()==null){
            System.out.println("-----暂无数据-----");
        }
        System.out.println("是否继续录入（y/n:");
        String con = in.next();
        if (con.equals("y")){
            Tutor_ProjectMenu(userid);
        }
        else{
            Administrator_Menu(userid);
        }

    }
    public static void Check_AchievementMenu(String userid){
        System.out.println("[1]论文");
        System.out.println("[2]奖励");
        System.out.println("[3]标准");
        System.out.println("[4]报告");
        System.out.println("[5]专利");
        System.out.println("[6]软硬件平台");
        System.out.println("[7]教材");
        System.out.println("[8]返回上一级界面");
        Scanner in=new Scanner(System.in);
        int choice=in.nextInt();

        while (!(choice == 1 | choice == 2 | choice == 3 | choice == 4 | choice == 5 | choice == 6 | choice == 7 | choice == 8)) {
            System.out.println("您输入的选项有误!请重新输入:");
            choice = in.nextInt();
        }
        if (choice == 1){ //终审论文
            ManagerExam.exam_thesis();
        } else if (choice == 2) {//奖励
            ManagerExam.exam_reward();
        } else if (choice == 3) {//标准
            ManagerExam.exam_standard();
        } else if (choice == 4) {//报告
            ManagerExam.exam_report();
        } else if (choice == 5) {//专利
            ManagerExam.exam_patent();
        } else if (choice == 6) {//平台
            ManagerExam.exam_pingtai();
        } else if (choice == 7) {//教材
            ManagerExam.exam_textbook();
        } else {
            Administrator_Menu(userid);
        }

    }
}
