package menus;

import edu.bo.PostgraduateBusiness;
import edu.bo.SubjectLeaderBusiness;
import edu.bo.TutorBusiness;
import edu.dao.DaoFactory;
import edu.vo.Postgraduate;
import edu.vo.SubjectLeader;
import edu.vo.Teacher;
import edu.vo.Tutor;
import users.UserManager;

import java.util.Scanner;

import static menus.ManagerMenu.Administrator_Menu;

/**
 * 用户登录注册模块
 */
public class UserMenu {
    /**
     * 打印登录注册页面
     */
    public static void printMenu() {
        System.out.println("---欢迎使用研究生培养环节和成果认定综合管理系统---");
        System.out.println("[1]用户登录");
        System.out.println("[2]退出系统");
        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();
        while (!(choice == 1 | choice == 2)) {
            System.out.println("您输入的选项有误!请重新输入:");
            choice = in.nextInt();
        }
        if (choice == 1) {
            printLoginMenu();
        } else {
            System.out.println("欢迎下次使用，再见!");
            System.exit(0);
        }
    }

    /**
     * 打印登录页面
     */
    public static void printLoginMenu() {
        System.out.println("请输入用户名:");
        Scanner in = new Scanner(System.in);
        String username = in.next();

        System.out.println("请输入密码:");
        String password = in.next();

        String roleCode = UserManager.login(username, password);

        // 登录失败则返回主页面
        if (roleCode == null) {
            printMenu();
        } else {
            roleSelectMenu(roleCode, username);
        }

    }

    /**
     * 根据用户的角色代码显示对应选择界面
     *
     * @param roleCode 用户的角色代码
     * @param username 用户的工号或学号
     */
    public static void roleSelectMenu(String roleCode, String username) {
        String[] roles = roleCode.split("");
        System.out.println("您的账号对应的角色如下，请选择一个角色:");
        for (int i = 0; i < roles.length; i++) {
            System.out.println("[" + (i + 1) + "] " + UserManager.getRole(roles[i]));
        }
        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();

        // 将用户选择转换为对应的角色代码
        String role = UserManager.getRole(roles[choice - 1]);
        // TODO:根据choice取出该账号对应的角色对象并跳转至不同子页面
        if (role.equals("研究生培养管理员")) {
            Administrator_Menu(username);
        }
        if (role.equals("学科负责人")) {
            subjectLeaderFun(username,roleCode);
        }
        if (role.equals("导师")) {
            tutorFun(username);
        }
        if(role.equals("授课教师")){
            teacherFun(username);
        }
        if (role.equals("研究生")){
            postgraduateFun(username);
        }
    }
    /**
     * 跳转至授课教师页面
     *
     * @param username 授课教师的工号
     */
    public static void teacherFun(String username) {
        Teacher teacher = DaoFactory.getTeacherDao().getTeacher(username);
        TeacherMenu tm=new TeacherMenu(teacher);
        tm.printMenu(teacher);
    }
    
    /**
     * 跳转至学科负责人页面
     *
     * @param username 学科负责人的工号
     * @param roleCode 该用户的角色代码
     */
    public static void subjectLeaderFun(String username,String roleCode) {
        SubjectLeader leader = DaoFactory.getInstance().getSubjectLeaderDAO().getSubjectLeader(username);
        SubjectLeaderBusiness business = new SubjectLeaderBusiness(leader);
        SubjectLeaderMenu.funSelect(business,username,roleCode);
    }


    /**
     * 跳转至导师页面
     *
     * @param username 导师的工号
     */

    public static void tutorFun(String username) {
        Tutor leader = DaoFactory.getTutorDao().getTutor(username);
        TutorBusiness business = new TutorBusiness(leader);
        TutorMenu.funSelect(business);
    }

    /**
     * 跳转至研究生页面
     *
     * @param username 研究生学号
     */
    public static void postgraduateFun(String username) {
        Postgraduate student = DaoFactory.getPostgraduateDao().get(username);
        PostgraduateBusiness business=new PostgraduateBusiness(student);
        PostgraduateMenu.funSelect(business);
    }

    public static void main(String[] args) {
        roleSelectMenu("2", "K002");
    }
}
