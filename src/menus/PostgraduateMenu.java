package menus;


import edu.bo.PostgraduateBusiness;

import java.util.Scanner;

/**
 * 研究生 子界面
 */
public class PostgraduateMenu {

    /**
     * 打印研究生功能选择界面
     */
    public static void studentMenu() {
        System.out.println("""
                \t\t[1].填报个人学术交流情况
                \t\t[2].申报助教课程
                \t\t[3].助教工作自述
                \t\t[4].填写《研究生参与项目认定表》
                \t\t[5].提交满足毕业成果认定标准的成果及佐证材料
                """);
//        System.out.println("\t\t[4].返回上一级");
    }

    /**
     * 选择对应功能并进入相关函数及界面
     */
    public static void funSelect(PostgraduateBusiness business){
        //打印研究生功能选择界面
        studentMenu();
        System.out.println("----------请选择您要进行的操作----------");

        Scanner input=new Scanner(System.in);
        String choice=input.next();
        switch (choice) {
            case "1":
                business.academicExchange();                    //填写个人学术交流情况
                break;
            case "2":
                business.applyAssistantCourse();                //申报助教课程
                break;
            case "3":
                business.workSelfReport();                      //助教工作自述
                break;
            case "4":
                business.projectTable();                        //填写《研究生参与项目认定表》
                break;
            case "5":
                PostgraduateBusiness.submitAchievement();       //提交满足毕业成果认证标准的成果及佐证材料
                break;
            case "6":
                return;
            default:
                System.out.println("输入错误！请重新输入");
                studentMenu();
                break;
        }
    }
}
