package menus;

import edu.bo.SubjectLeaderBusiness;

import java.util.Scanner;

/**
 * @author Liu Guoli
 * 学科负责人界面
 */
public class SubjectLeaderMenu {

    /**
     * 打印学科负责人界面
     */
    public static void printMenu() {
        System.out.println();
        System.out.println("[1]设置助教课程优先级");
        System.out.println("[2]设置助教可选志愿数量");
        System.out.println("[3]设置参与项目经费要求");
        System.out.println("[4]设置学术交流次数要求");
        System.out.println("[5]查看导师信息");
        System.out.println("[6]查看研究生培养情况");
        System.out.println("[7]返回上一级界面");
        System.out.println("[8]退出系统");
    }

    /**
     * 选择并进入对应的功能
     * @param business 学科负责人业务对象
     * @param username 用户名(即学科负责人工号)
     * @param roleCode 用户的角色代码
     */
    public static void funSelect(SubjectLeaderBusiness business,String username,String roleCode) {
        // 打印页面
        printMenu();

        // 选择功能
        Scanner in = new Scanner(System.in);
        System.out.println("请选择功能:");
        int choice = in.nextInt();
        while (choice < 1 || choice > 8) {
            System.out.println("您输入的选项有误，请重新输入:");
            choice = in.nextInt();
        }

        while (true){
            //根据选项进入对应的功能界面
            switch (choice) {
                case 1:
                    business.setPriority();
                    break;
                case 2:
                    business.setAspirationNum();
                    break;
                case 3:
                    business.setExpenditureDemand();
                    break;
                case 4:
                    business.setExchangeDemand();
                    break;
                case 5:
                    business.browseTutorInfo();
                    break;
                case 6:
                    business.browsePostgraduateInfo();
                    break;
                case 7:
                    UserMenu.roleSelectMenu(roleCode,username);
                    break;
                case 8:
                    System.exit(0);
                default:
                    System.out.println("您输入的选项有误，请重新输入:");
                    break;
            }
            printMenu();
            choice=in.nextInt();
        }

    }
}
