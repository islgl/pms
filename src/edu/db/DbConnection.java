package edu.db;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 数据库连接工具类
 * host: 43.143.169.18
 * port: 1433
 * username: SA
 * password: superStrongPwd123
 * database: Postgraduate
 */
public class DbConnection {
    private DataSource dataSource;

    public DbConnection() {
        dataSource = DruidUtil.getDataSource();
    }

    /**
     * 从连接池中获取Connection对象
     *
     * @return Connection对象
     */
    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println("连接失败!");
        }
        return connection;
    }

    /**
     * 关闭Connection对象
     *
     * @param connection 需要关闭的Connection对象
     */
    public void close(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试数据库连接是否成功
     *
     * @return 表示数据库是否连接成功的bool值
     */
    public boolean testConnection() {
        boolean flag = true;
        Connection connection = getConnection();
        if (connection == null) {
            flag = false;
        }
        close(connection);
        return flag;
    }

    @Override
    public String toString() {
        return "jdbc:sqlserver://43.143.169.18:1433;database=Postgraduate";
    }

    public static void main(String[] args) {
        //测试连接远程数据库
        DbConnection dbConnection = new DbConnection();
        System.out.println(dbConnection);
        if (dbConnection.testConnection()) {
            System.out.println("连接成功!");
        } else {
            System.out.println("连接失败!");
        }
    }
}
