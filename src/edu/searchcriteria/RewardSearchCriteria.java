package edu.searchcriteria;


public class RewardSearchCriteria extends SearchCriteria {
    private String reward_no;       //奖励编号
    private String name_winner;     //获奖者姓名
    private String name_reward;    //奖励名称
    private String level_reward;   //奖励等级
    private String level_award;    //获奖等级
    private int ranking;           //排名
    private String date_award;     //获奖时间
    private String support_materials;   //佐证材料
    private String tutor_exam;     //导师初审结果
    private String manager_exam;   //管理人员终审结果

    public RewardSearchCriteria(String reward_no, String name_winner, String name_reward,
                                String level_reward, String level_award, int ranking, String date_award,
                                String support_materials, String tutor_exam, String manager_exam) {
        this.reward_no = reward_no;
        this.name_winner = name_winner;
        this.name_reward = name_reward;
        this.level_reward = level_reward;
        this.level_award = level_award;
        this.ranking = ranking;
        this.date_award = date_award;
        this.support_materials = support_materials;
        this.tutor_exam = tutor_exam;
        this.manager_exam = manager_exam;
    }

    public String getName_winner() {
        return name_winner;
    }

    public void setName_winner(String name_winner) {
        this.name_winner = name_winner;
    }

    public String getReward_no() {
        return reward_no;
    }

    public void setReward_no(String reward_no) {
        this.reward_no = reward_no;
    }

    public String getName_reward() {
        return name_reward;
    }

    public void setName_reward(String name_reward) {
        this.name_reward = name_reward;
    }

    public String getLevel_reward() {
        return level_reward;
    }

    public void setLevel_reward(String level_reward) {
        this.level_reward = level_reward;
    }

    public String getLevel_award() {
        return level_award;
    }

    public void setLevel_award(String level_award) {
        this.level_award = level_award;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public String getDate_award() {
        return date_award;
    }

    public void setDate_award(String date_award) {
        this.date_award = date_award;
    }

    public String getSupport_materials() {
        return support_materials;
    }

    public void setSupport_materials(String support_materials) {
        this.support_materials = support_materials;
    }

    public String getTutor_exam() {
        return tutor_exam;
    }

    public void setTutor_exam(String tutor_exam) {
        this.tutor_exam = tutor_exam;
    }

    public String getManager_exam() {
        return manager_exam;
    }

    public void setManager_exam(String manager_exam) {
        this.manager_exam = manager_exam;
    }

    @Override
    public String toString() {
        return "RewardSearchCriteria{" +
                "reward_no='" + reward_no + '\'' +
                ", name_winner='" + name_winner + '\'' +
                ", name_reward='" + name_reward + '\'' +
                ", level_reward='" + level_reward + '\'' +
                ", level_award='" + level_award + '\'' +
                ", ranking=" + ranking +
                ", date_award='" + date_award + '\'' +
                ", support_materials='" + support_materials + '\'' +
                ", tutor_exam='" + tutor_exam + '\'' +
                ", manager_exam='" + manager_exam + '\'' +
                '}';
    }
}
