package edu.searchcriteria;

import edu.vo.Course;
import edu.vo.Postgraduate;
import edu.vo.Teacher;

/**
 * 课程助教列表的查询条件
 */
public class AssistantSearchCriteria extends SearchCriteria{
    private Course course;
    private Postgraduate postgraduate;
    private Teacher teacher;
    private String selfDescription;     //自述
    private String teacherEvaluation;   //教师评价
    private String evaluationGrade;     //评价结果
    private int priority;

    public AssistantSearchCriteria(Course course, Postgraduate postgraduate, Teacher teacher, String selfDescription, String teacherEvaluation, String evaluationGrade,int priority) {
        this.course = course;
        this.postgraduate = postgraduate;
        this.teacher = teacher;
        this.selfDescription = selfDescription;
        this.teacherEvaluation = teacherEvaluation;
        this.evaluationGrade = evaluationGrade;
        this.priority=priority;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Postgraduate getPostgraduate() {
        return postgraduate;
    }

    public void setPostgraduate(Postgraduate postgraduate) {
        this.postgraduate = postgraduate;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public String getSelfDescription() {
        return selfDescription;
    }

    public void setSelfDescription(String selfDescription) {
        this.selfDescription = selfDescription;
    }

    public String getTeacherEvaluation() {
        return teacherEvaluation;
    }

    public void setTeacherEvaluation(String teacherEvaluation) {
        this.teacherEvaluation = teacherEvaluation;
    }

    public String getEvaluationGrade() {
        return evaluationGrade;
    }

    public void setEvaluationGrade(String evaluationGrade) {
        this.evaluationGrade = evaluationGrade;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "AssistantSearchCriteria{" +
                "course=" + course +
                ", postgraduate=" + postgraduate +
                ", teacher=" + teacher +
                ", selfDescription='" + selfDescription + '\'' +
                ", teacherEvaluation='" + teacherEvaluation + '\'' +
                ", evaluationGrade='" + evaluationGrade + '\'' +
                '}';
    }
}
