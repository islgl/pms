package edu.searchcriteria;

public class AdSearchCriteria extends SearchCriteria {
    private String adminid;   //����Ա���
    private String adminame;  //����

    public AdSearchCriteria(String adminid, String adminame) {
        this.adminid = adminid;
        this.adminame = adminame;
    }

    public AdSearchCriteria() {

    }

    public String getAdminid() {
        return adminid;
    }

    public void setAdminid(String adminid) {
        this.adminid = adminid;
    }

    public String getAdminame() {
        return adminame;
    }

    public void setAdminame(String adminame) {
        this.adminame = adminame;
    }

    @Override
    public String toString() {
        return "AdSearchCriteria{" +
                "adminid='" + adminid + '\'' +
                ", adminame='" + adminame + '\'' +
                '}';
    }
}


