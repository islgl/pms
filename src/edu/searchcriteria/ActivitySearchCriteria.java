package edu.searchcriteria;

public class ActivitySearchCriteria extends SearchCriteria {
    private String id;
    private String name;
    private String major;
    private Integer a_count;
    private String a_name;
    private String a_place;
    private String a_time;
    private String CEname;
    private String a_note;
    private  Integer check;

    public ActivitySearchCriteria(String id, String name, String major, Integer a_count, String a_name, String a_place, String a_time, String CEname, String a_note, Integer check) {
        this.id = id;
        this.name = name;
        this.major = major;
        this.a_count = a_count;
        this.a_name = a_name;
        this.a_place = a_place;
        this.a_time = a_time;
        this.CEname = CEname;
        this.a_note = a_note;
        this.check = check;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Integer getA_count() {
        return a_count;
    }

    public void setA_count(Integer a_count) {
        this.a_count = a_count;
    }

    public String getA_name() {
        return a_name;
    }

    public void setA_name(String a_name) {
        this.a_name = a_name;
    }

    public String getA_place() {
        return a_place;
    }

    public void setA_place(String a_place) {
        this.a_place = a_place;
    }

    public String getA_time() {
        return a_time;
    }

    public void setA_time(String a_time) {
        this.a_time = a_time;
    }

    public String getCEname() {
        return CEname;
    }

    public void setCEname(String CEname) {
        this.CEname = CEname;
    }

    public String getA_note() {
        return a_note;
    }

    public void setA_note(String a_note) {
        this.a_note = a_note;
    }

    public Integer getCheck() {
        return check;
    }

    public void setCheck(Integer check) {
        this.check = check;
    }

    @Override
    public String toString() {
        return "ActivitySearchCriteria{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", major='" + major + '\'' +
                ", a_count=" + a_count +
                ", a_name='" + a_name + '\'' +
                ", a_place='" + a_place + '\'' +
                ", a_time='" + a_time + '\'' +
                ", CEname='" + CEname + '\'' +
                ", a_note='" + a_note + '\'' +
                ", check=" + check +
                '}';
    }
}
