package edu.searchcriteria;

public class TutorSearchCriteria extends SearchCriteria{
    //导师编号 导师姓名 导师性别 专业
    private String tutor_no;
    private String tutor_name;
    private String tutor_sex;
    private String tutor_project;
    private String tutor_major;

    public TutorSearchCriteria() {
        this.tutor_no = null;
        this.tutor_name = null;
        this.tutor_sex = null;
        this.tutor_major = null;
    }

    public TutorSearchCriteria(String tutor_no, String tutor_name, String tutor_sex, String tutor_major) {
        this.tutor_no = tutor_no;
        this.tutor_name = tutor_name;
        this.tutor_sex = tutor_sex;
        this.tutor_major = tutor_major;
    }

    public String getTutor_no() {
        return tutor_no;
    }

    public void setTutor_no(String tutor_no) {
        this.tutor_no = tutor_no;
    }

    public String getTutor_name() {
        return tutor_name;
    }

    public void setTutor_name(String tutor_name) {
        this.tutor_name = tutor_name;
    }

    public String getTutor_sex() {
        return tutor_sex;
    }

    public void setTutor_sex(String tutor_sex) {
        this.tutor_sex = tutor_sex;
    }

    public String getTutor_major() {
        return tutor_major;
    }

    public void setTutor_major(String tutor_major) {
        this.tutor_major = tutor_major;
    }

    @Override
    public String toString() {
        return "TutorSearchCriteria{" +
                "tutor_no='" + tutor_no + '\'' +
                ", tutor_name='" + tutor_name + '\'' +
                ", tutor_sex='" + tutor_sex + '\'' +
                ", tutor_major='" + tutor_major + '\'' +
                '}';
    }
}
