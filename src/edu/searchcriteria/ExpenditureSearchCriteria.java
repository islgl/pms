package edu.searchcriteria;

public class ExpenditureSearchCriteria extends SearchCriteria {
    private String id;                      //项目编号
    private float expenditure;             //经费数
    private int teacher_signature;       //指导老师签字 初始值-1 有签字1 无签字0
    private int principal_signature;     //项目负责人签字 初始值-1 有签字1 无签字0

    public ExpenditureSearchCriteria() {
        this.id = null;
        this.expenditure = 0;
        this.teacher_signature = -1;
        this.principal_signature = -1;
    }

    public ExpenditureSearchCriteria(String id, float expenditure, int teacher_signature, int principal_signature) {
        this.id = id;
        this.expenditure = expenditure;
        this.teacher_signature = teacher_signature;
        this.principal_signature = principal_signature;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getExpenditure() {
        return expenditure;
    }

    public void setExpenditure(float expenditure) {
        this.expenditure = expenditure;
    }

    public int getTeacher_signature() {
        return teacher_signature;
    }

    public void setTeacher_signature(int teacher_signature) {
        this.teacher_signature = teacher_signature;
    }

    public int getPrincipal_signature() {
        return principal_signature;
    }

    public void setPrincipal_signature(int principal_signature) {
        this.principal_signature = principal_signature;
    }

    @Override
    public String toString() {
        return "ExpenditureSearchCriteria{" +
                "项目编号='" + id + '\'' +
                ", 经费数=" + expenditure +
                ", 指导老师签字='" + teacher_signature + '\'' +
                ", 项目负责人签字='" + principal_signature + '\'' +
                '}';
    }
}
