package edu.searchcriteria;

/**
 * 数据库查询条件
 */
public class SearchCriteria {
    /**
     * 去除语句中多余的WHERE和AND关键字
     *
     * @param criteriaSql 需要进行处理的SQL语句
     * @return 处理完成的SQL语句
     */
    public String removeRedundancy(StringBuilder criteriaSql) {
        // 去除多余的WHERE和AND
        if (criteriaSql.substring(criteriaSql.length() - 5).equals(" AND ")) {
            criteriaSql.delete(criteriaSql.length() - 5, criteriaSql.length() - 1);
        }
        if (criteriaSql.substring(criteriaSql.length() - 7).equals(" WHERE ")) {
            criteriaSql.delete(criteriaSql.length() - 7, criteriaSql.length() - 1);
        }
        return String.valueOf(criteriaSql);
    }

    /**
     * 去除语句中多余的"OR"
     *
     * @param criteriaSql 需要进行处理的SQL语句
     * @return 处理完成的SQL语句
     */
    public String removeOR(StringBuilder criteriaSql) {
        // 去除多余的WHERE和OR
        if (criteriaSql.substring(criteriaSql.length() - 4).equals(" OR ")) {
            criteriaSql.delete(criteriaSql.length() - 4, criteriaSql.length() - 1);
        }
        if (criteriaSql.substring(criteriaSql.length() - 7).equals(" WHERE ")) {
            criteriaSql.delete(criteriaSql.length() - 7, criteriaSql.length() - 1);

        }
        return String.valueOf(criteriaSql);
    }

    /**
     * SQL语句文本规范化处理
     *
     * @param value 需要进行规范化处理的文本
     * @return 进过处理之后的文本
     */
    public static String fixSqlFieldValue(String value) {
        if (value == null) {
            return null;
        }
        int length = value.length();
        StringBuilder fixedValue = new StringBuilder((int) (length *
                1.1));
        for (int i = 0; i < length; i++) {
            char c = value.charAt(i);
            if (c == '\'') {
                fixedValue.append("''");
            } else {
                fixedValue.append(c);
            }
        }
        return fixedValue.toString();
    }
}
