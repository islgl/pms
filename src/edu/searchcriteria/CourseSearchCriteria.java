package edu.searchcriteria;

public class CourseSearchCriteria extends SearchCriteria{
    private String id;              //课程号
    private String name;            //课程名
    private int student_num;     //课程人数
    private String subject;         //所在学科
    private String properties;      //课程性质 选修or必修
    private String Prelect_object;  //授课对象 本科生or研究生
    private String teacher;         //授课老师
    private String time;            //授课时间

    private int hour;               //课程学时

    public CourseSearchCriteria() {
        this.id = null;
        this.name = null;
        this.student_num = -1;
        this.subject = null;
        this.properties = null;
        this.Prelect_object = null;
        this.teacher = null;
        this.time = null;
        this.hour = -1;
    }

    public CourseSearchCriteria(String id, String name, int student_num, String subject, String properties, String prelect_object, String teacher, String time, int hour) {
        this.id = id;
        this.name = name;
        this.student_num = student_num;
        this.subject = subject;
        this.properties = properties;
        this.Prelect_object = prelect_object;
        this.teacher = teacher;
        this.time = time;
        this.hour = hour;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStudent_num() {
        return student_num;
    }

    public void setStudent_num(int student_num) {
        this.student_num = student_num;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public String getPrelect_object() {
        return Prelect_object;
    }

    public void setPrelect_object(String prelect_object) {
        Prelect_object = prelect_object;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        hour = hour;
    }

    @Override
    public String toString() {
        return "CourseSearchCriteria{" +
                "课程号='" + id + '\'' +
                ", 课程名='" + name + '\'' +
                ", 课程人数='" + student_num + '\'' +
                ", 所在学科='" + subject + '\'' +
                ", 课程性质='" + properties + '\'' +
                ", 授课对象='" + Prelect_object + '\'' +
                ", 授课老师='" + teacher + '\'' +
                ", 授课时间='" + time + '\'' +
                ", 课程学时='" + hour + '\'' +
                '}';
    }
}
