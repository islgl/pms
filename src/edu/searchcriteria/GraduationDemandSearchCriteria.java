package edu.searchcriteria;

/**
 * @author Liu Guoli
 * 学科毕业要求查询条件
 */
public class GraduationDemandSearchCriteria extends SearchCriteria {
    private String subjectId;
    private int aspiration;
    private int exchange;
    private int expenditure;

    public GraduationDemandSearchCriteria() {
        subjectId = null;
        aspiration = 0;
        exchange = 0;
        expenditure = 0;
    }

    public GraduationDemandSearchCriteria(String subjectId, int aspiration, int exchange, int expenditure) {
        this.subjectId = subjectId;
        this.aspiration = aspiration;
        this.exchange = exchange;
        this.expenditure = expenditure;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public int getAspiration() {
        return aspiration;
    }

    public void setAspiration(int aspiration) {
        this.aspiration = aspiration;
    }

    public int getExchange() {
        return exchange;
    }

    public void setExchange(int exchange) {
        this.exchange = exchange;
    }

    public int getExpenditure() {
        return expenditure;
    }

    public void setExpenditure(int expenditure) {
        this.expenditure = expenditure;
    }

    @Override
    public String toString() {
        return "GraduationDemandSearchCriteria{" +
                "subjectId='" + subjectId + '\'' +
                ", aspiration=" + aspiration +
                ", exchange=" + exchange +
                ", expenditure=" + expenditure +
                '}';
    }
}
