package edu.searchcriteria;


public class ThesisSearchCriteria extends SearchCriteria {
    //论文编号  论文名称  作者姓名 作者身份 发表刊物名称  状态  发表时间  索引类型 归属库情况  论文扫描或PDF材料 导师审核 管理员审核
    private String thesis_no;
    private String name_thesis;
    private String name_author;
    private String identify_author;
    private String name_publication;
    private String state_thesis;
    private String date_publication;
    private String type_index;
    private String attribution_library;
    private String scan_pdf;
    private String tutor_exam;
    private String manager_exam;

    public ThesisSearchCriteria(String thesis_no, String name_thesis, String name_author,
                                String identify_author, String name_publication, String state_thesis,
                                String date_publication, String type_index, String attribution_library,
                                String scan_pdf, String tutor_exam, String manager_exam) {
        this.thesis_no = thesis_no;
        this.name_thesis = name_thesis;
        this.name_author = name_author;
        this.identify_author = identify_author;
        this.name_publication = name_publication;
        this.state_thesis = state_thesis;
        this.date_publication = date_publication;
        this.type_index = type_index;
        this.attribution_library = attribution_library;
        this.scan_pdf = scan_pdf;
        this.tutor_exam = tutor_exam;
        this.manager_exam = manager_exam;
    }

    public String getThesis_no() {
        return thesis_no;
    }

    public void setThesis_no(String thesis_no) {
        this.thesis_no = thesis_no;
    }

    public String getName_thesis() {
        return name_thesis;
    }

    public void setName_thesis(String name_thesis) {
        this.name_thesis = name_thesis;
    }

    public String getName_author() {
        return name_author;
    }

    public void setName_author(String name_author) {
        this.name_author = name_author;
    }

    public String getIdentify_author() {
        return identify_author;
    }

    public void setIdentify_author(String identify_author) {
        this.identify_author = identify_author;
    }

    public String getName_publication() {
        return name_publication;
    }

    public void setName_publication(String name_publication) {
        this.name_publication = name_publication;
    }

    public String getState_thesis() {
        return state_thesis;
    }

    public void setState_thesis(String state_thesis) {
        this.state_thesis = state_thesis;
    }

    public String getDate_publication() {
        return date_publication;
    }

    public void setDate_publication(String date_publication) {
        this.date_publication = date_publication;
    }

    public String getType_index() {
        return type_index;
    }

    public void setType_index(String type_index) {
        this.type_index = type_index;
    }

    public String getAttribution_library() {
        return attribution_library;
    }

    public void setAttribution_library(String attribution_library) {
        this.attribution_library = attribution_library;
    }

    public String getScan_pdf() {
        return scan_pdf;
    }

    public void setScan_pdf(String scan_pdf) {
        this.scan_pdf = scan_pdf;
    }

    public String getTutor_exam() {
        return tutor_exam;
    }

    public void setTutor_exam(String tutor_exam) {
        this.tutor_exam = tutor_exam;
    }

    public String getManager_exam() {
        return manager_exam;
    }

    public void setManager_exam(String manager_exam) {
        this.manager_exam = manager_exam;
    }

    @Override
    public String toString() {
        return "Thesis{" +
                "thesis_no='" + thesis_no + '\'' +
                ", name_thesis='" + name_thesis + '\'' +
                ", name_author='" + name_author + '\'' +
                ", identify_author='" + identify_author + '\'' +
                ", name_publication='" + name_publication + '\'' +
                ", state_thesis='" + state_thesis + '\'' +
                ", date_publication='" + date_publication + '\'' +
                ", type_index='" + type_index + '\'' +
                ", attribution_library='" + attribution_library + '\'' +
                ", scan_pdf='" + scan_pdf + '\'' +
                ", tutor_exam='" + tutor_exam + '\'' +
                ", manager_exam='" + manager_exam + '\'' +
                '}';
    }
}
