package edu.searchcriteria;

public class PatentSearchCriteria extends SearchCriteria {
    //专利名称、专利类型、专利号、专利发布时间、专利状态、贡献度排名、佐证材料
    private String patent_no;
    private String name_student;
    private String identify_student;
    private String patent_name;
    private String type_patent;
    private String public_date;
    private String patent_zhuangtai;
    private int contribution;
    private String zuozheng;
    private String tutor_exam;
    private String manager_exam;

    public PatentSearchCriteria(String patent_no, String name_student, String identify_student, String patent_name,
                                String type_patent, String public_date, String patent_zhuangtai, int contribution,
                                String zuozheng, String tutor_exam, String manager_exam) {
        this.patent_no = patent_no;
        this.name_student = name_student;
        this.identify_student = identify_student;
        this.patent_name = patent_name;
        this.type_patent = type_patent;
        this.public_date = public_date;
        this.patent_zhuangtai = patent_zhuangtai;
        this.contribution = contribution;
        this.zuozheng = zuozheng;
        this.tutor_exam = tutor_exam;
        this.manager_exam = manager_exam;
    }

    public String getPatent_no() {
        return patent_no;
    }

    public void setPatent_no(String patent_no) {
        this.patent_no = patent_no;
    }

    public String getName_student() {
        return name_student;
    }

    public void setName_student(String name_student) {
        this.name_student = name_student;
    }

    public String getIdentify_student() {
        return identify_student;
    }

    public void setIdentify_student(String identify_student) {
        this.identify_student = identify_student;
    }

    public String getPatent_name() {
        return patent_name;
    }

    public void setPatent_name(String patent_name) {
        this.patent_name = patent_name;
    }

    public String getType_patent() {
        return type_patent;
    }

    public void setType_patent(String type_patent) {
        this.type_patent = type_patent;
    }

    public String getPublic_date() {
        return public_date;
    }

    public void setPublic_date(String public_date) {
        this.public_date = public_date;
    }

    public String getPatent_zhuangtai() {
        return patent_zhuangtai;
    }

    public void setPatent_zhuangtai(String patent_zhuangtai) {
        this.patent_zhuangtai = patent_zhuangtai;
    }

    public int getContribution() {
        return contribution;
    }

    public void setContribution(int contribution) {
        this.contribution = contribution;
    }

    public String getZuozheng() {
        return zuozheng;
    }

    public void setZuozheng(String zuozheng) {
        this.zuozheng = zuozheng;
    }

    public String getTutor_exam() {
        return tutor_exam;
    }

    public void setTutor_exam(String tutor_exam) {
        this.tutor_exam = tutor_exam;
    }

    public String getManager_exam() {
        return manager_exam;
    }

    public void setManager_exam(String manager_exam) {
        this.manager_exam = manager_exam;
    }

    @Override
    public String toString() {
        return "PatentSearchCriteria{" +
                "patent_no='" + patent_no + '\'' +
                ", name_student='" + name_student + '\'' +
                ", identify_student='" + identify_student + '\'' +
                ", patent_name='" + patent_name + '\'' +
                ", type_patent='" + type_patent + '\'' +
                ", public_date='" + public_date + '\'' +
                ", patent_zhuangtai='" + patent_zhuangtai + '\'' +
                ", contribution=" + contribution +
                ", zuozheng='" + zuozheng + '\'' +
                ", tutor_exam='" + tutor_exam + '\'' +
                ", manager_exam='" + manager_exam + '\'' +
                '}';
    }
}
