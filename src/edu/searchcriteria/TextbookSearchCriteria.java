package edu.searchcriteria;

public class TextbookSearchCriteria extends SearchCriteria {
    private String textbook_no;         //编号（主键）
    private String name_author;         //作者姓名
    private String identify_author;     //作者身份（硕士/博士）
    private String name_textbook;       //教材名称
    private String publish_house;       //教材出版社
    private String date_publish;        //教材出版时间
    private int contribution_textbook;  //贡献度（整数）
    private String support_materials;   //佐证材料
    private String tutor_exam;          //导师初审结果
    private String manager_exam;        //管理人员终审结果

    public TextbookSearchCriteria(String textbook_no, String name_author, String identify_author, String name_textbook,
                                  String publish_house, String date_publish, int contribution_textbook,
                                  String support_materials, String tutor_exam, String manager_exam) {
        this.textbook_no = textbook_no;
        this.name_author = name_author;
        this.identify_author = identify_author;
        this.name_textbook = name_textbook;
        this.publish_house = publish_house;
        this.date_publish = date_publish;
        this.contribution_textbook = contribution_textbook;
        this.support_materials = support_materials;
        this.tutor_exam = tutor_exam;
        this.manager_exam = manager_exam;
    }

    public String getTextbook_no() {
        return textbook_no;
    }

    public void setTextbook_no(String textbook_no) {
        this.textbook_no = textbook_no;
    }

    public String getName_author() {
        return name_author;
    }

    public void setName_author(String name_author) {
        this.name_author = name_author;
    }

    public String getIdentify_author() {
        return identify_author;
    }

    public void setIdentify_author(String identify_author) {
        this.identify_author = identify_author;
    }

    public String getName_textbook() {
        return name_textbook;
    }

    public void setName_textbook(String name_textbook) {
        this.name_textbook = name_textbook;
    }

    public String getPublish_house() {
        return publish_house;
    }

    public void setPublish_house(String publish_house) {
        this.publish_house = publish_house;
    }

    public String getDate_publish() {
        return date_publish;
    }

    public void setDate_publish(String date_publish) {
        this.date_publish = date_publish;
    }

    public int getContribution_textbook() {
        return contribution_textbook;
    }

    public void setContribution_textbook(int contribution_textbook) {
        this.contribution_textbook = contribution_textbook;
    }

    public String getSupport_materials() {
        return support_materials;
    }

    public void setSupport_materials(String support_materials) {
        this.support_materials = support_materials;
    }

    public String getTutor_exam() {
        return tutor_exam;
    }

    public void setTutor_exam(String tutor_exam) {
        this.tutor_exam = tutor_exam;
    }

    public String getManager_exam() {
        return manager_exam;
    }

    public void setManager_exam(String manager_exam) {
        this.manager_exam = manager_exam;
    }

    @Override
    public String toString() {
        return "TextbookSearchCriteria{" +
                "textbook_no='" + textbook_no + '\'' +
                ", name_author='" + name_author + '\'' +
                ", identify_author='" + identify_author + '\'' +
                ", name_textbook='" + name_textbook + '\'' +
                ", publish_house='" + publish_house + '\'' +
                ", date_publish='" + date_publish + '\'' +
                ", contribution_textbook=" + contribution_textbook +
                ", support_materials='" + support_materials + '\'' +
                ", tutor_exam='" + tutor_exam + '\'' +
                ", manager_exam='" + manager_exam + '\'' +
                '}';
    }
}
