package edu.searchcriteria;

/**
 * @author: Liu Guoli lewisliugl@outlook.com
 * Date: 2022/11/30
 * Copyright: Copyright ©️ 2022 lewisliugl@outlook.com.All rights reserved.
 */
public class PostgraduateSearchCriteria extends SearchCriteria{
    private String id=null;
    private String name=null;
    private String major=null;
    private String tutorid=null;
    private String application1=null;
    private String application2=null;
    private int type=-1;
    private int allot=-1;

    public PostgraduateSearchCriteria() {
        this.id = null;
        this.name = null;
        this.major = null;
        this.tutorid = null;
        this.application1 = null;
        this.application2 = null;
        this.type = -1;
        this.allot = -1;
    }

    public PostgraduateSearchCriteria(String id, String name, String major, String tutorid, String application1, String application2, int type, int allot) {
        this.id = id;
        this.name = name;
        this.major = major;
        this.tutorid = tutorid;
        this.application1 = application1;
        this.application2 = application2;
        this.type = type;
        this.allot = allot;
    }

    public PostgraduateSearchCriteria(String id, String name, String major, String application1, String application2, int type, int allot) {
        this.id = id;
        this.name = name;
        this.major = major;
        this.application1 = application1;
        this.application2 = application2;
        this.type = type;
        this.allot = allot;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getApplication1() {
        return application1;
    }

    public void setApplication1(String application1) {
        this.application1 = application1;
    }

    public String getApplication2() {
        return application2;
    }

    public void setApplication2(String application2) {
        this.application2 = application2;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAllot() {
        return allot;
    }

    public void setAllot(int allot) {
        this.allot = allot;
    }

    public String getTutorid() {
        return tutorid;
    }

    public void setTutorid(String tutorid) {
        this.tutorid = tutorid;
    }

    @Override
    public String toString() {
        return "PostgraduateSearchCriteria{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", major='" + major + '\'' +
                ", tutorid='" + tutorid + '\'' +
                ", application1='" + application1 + '\'' +
                ", application2='" + application2 + '\'' +
                ", type=" + type +
                ", allot=" + allot +
                '}';
    }
}
