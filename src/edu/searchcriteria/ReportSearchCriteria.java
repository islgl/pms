package edu.searchcriteria;

public class ReportSearchCriteria extends SearchCriteria {
    private String report_no;           //编号
    private String name_student;        //学生姓名
    private String identify_student;    //学生身份
    private String name_report;         //报告名称
    private String type_report;         //报告类型
    private String service_report;      //报告服务单位
    private String date_report;         //报告日期
    private int contribution_ranking;    //贡献度排名
    private String support_materials;   //佐证材料
    private String tutor_exam;          //导师初审
    private String manager_exam;        //管理员终审

    public ReportSearchCriteria(String report_no, String name_student, String identify_student, String name_report,
                                String type_report, String service_report, String date_report, int contribution_ranking,
                                String support_materials, String tutor_exam, String manager_exam) {
        this.report_no = report_no;
        this.name_student = name_student;
        this.identify_student = identify_student;
        this.name_report = name_report;
        this.type_report = type_report;
        this.service_report = service_report;
        this.date_report = date_report;
        this.contribution_ranking = contribution_ranking;
        this.support_materials = support_materials;
        this.tutor_exam = tutor_exam;
        this.manager_exam = manager_exam;
    }

    public String getReport_no() {
        return report_no;
    }

    public void setReport_no(String report_no) {
        this.report_no = report_no;
    }

    public String getName_student() {
        return name_student;
    }

    public void setName_student(String name_student) {
        this.name_student = name_student;
    }

    public String getIdentify_student() {
        return identify_student;
    }

    public void setIdentify_student(String identify_student) {
        this.identify_student = identify_student;
    }

    public String getName_report() {
        return name_report;
    }

    public void setName_report(String name_report) {
        this.name_report = name_report;
    }

    public String getType_report() {
        return type_report;
    }

    public void setType_report(String type_report) {
        this.type_report = type_report;
    }

    public String getService_report() {
        return service_report;
    }

    public void setService_report(String service_report) {
        this.service_report = service_report;
    }

    public String getDate_report() {
        return date_report;
    }

    public void setDate_report(String date_report) {
        this.date_report = date_report;
    }

    public int getContribution_ranking() {
        return contribution_ranking;
    }

    public void setContribution_ranking(int contribution_ranking) {
        this.contribution_ranking = contribution_ranking;
    }

    public String getSupport_materials() {
        return support_materials;
    }

    public void setSupport_materials(String support_materials) {
        this.support_materials = support_materials;
    }

    public String getTutor_exam() {
        return tutor_exam;
    }

    public void setTutor_exam(String tutor_exam) {
        this.tutor_exam = tutor_exam;
    }

    public String getManager_exam() {
        return manager_exam;
    }

    public void setManager_exam(String manager_exam) {
        this.manager_exam = manager_exam;
    }

    @Override
    public String toString() {
        return "ReportSearchCriteria{" +
                "report_no='" + report_no + '\'' +
                ", name_student='" + name_student + '\'' +
                ", identify_student='" + identify_student + '\'' +
                ", name_report='" + name_report + '\'' +
                ", type_report='" + type_report + '\'' +
                ", service_report='" + service_report + '\'' +
                ", date_report='" + date_report + '\'' +
                ", contribution_ranking=" + contribution_ranking +
                ", support_materials='" + support_materials + '\'' +
                ", tutor_exam='" + tutor_exam + '\'' +
                ", manager_exam='" + manager_exam + '\'' +
                '}';
    }
}
