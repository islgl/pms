package edu.searchcriteria;

public class SubjectLeaderSearchCriteria extends SearchCriteria{
    private String subjectleader_no=null;
    private String subjectleader_name=null;
    private String subject=null;

    public SubjectLeaderSearchCriteria() {
        this.subjectleader_no = null;
        this.subjectleader_name = null;
        this.subject=null;
    }

    public SubjectLeaderSearchCriteria(String subjectleader_no, String subjectleader_name,String subject) {
        this.subjectleader_no = subjectleader_no;
        this.subjectleader_name = subjectleader_name;
        this.subject=subject;
    }

    public String getSubjectleader_no() {
        return subjectleader_no;
    }

    public void setSubjectleader_no(String subjectleader_no) {
        this.subjectleader_no = subjectleader_no;
    }

    public String getSubjectleader_name() {
        return subjectleader_name;
    }

    public void setSubjectleader_name(String subjectleader_name) {
        this.subjectleader_name = subjectleader_name;
    }

    public String getSubject() {
        return subject;
    }


    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "SubjectLeaderSearchCriteria{" +
                "subjectleader_no='" + subjectleader_no + '\'' +
                ", subjectleader_name='" + subjectleader_name + '\'' +
                ", subject='" + subject + '\'' +
                '}';
    }
}