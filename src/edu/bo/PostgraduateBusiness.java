package edu.bo;


import edu.dao.DaoFactory;
import edu.subsystem.ActivitySystem;
import edu.subsystem.AssistantSystem;
import edu.subsystem.ProjectSystem;
import edu.vo.*;
import menus.PostgraduateMenu;

import java.util.Scanner;

import static edu.subsystem.AchievementsSystem.doctor;
import static edu.subsystem.AchievementsSystem.master;


/**
 * @author Peng Zhonghui
 * 研究生业务逻辑
 */
public class PostgraduateBusiness {

    private final Postgraduate student;
    private static final Scanner input=new Scanner(System.in);

    public PostgraduateBusiness(Postgraduate student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return student.toString();
    }

    public static void main(String[] args) {
    }

    /**
     * 1.填报个人学术交流情况
     */
    public void academicExchange(){
        //增删改查
        String id=student.getId();
        Postgraduate postgraduate= DaoFactory.getPostgraduateDao().get(id);
        System.out.println("\t\t[1].增\n\t\t[2].删\n\t\t[3].改\n\t\t[4].查\n\t\t[5].返回上一级");
        System.out.println("-------------------------------------------------");
        int role = input.nextInt();
        switch (role) {
            case 1 -> ActivitySystem.Postgraduateadd(postgraduate);
            case 2 -> {
                System.out.println("您的学术交流活动情况为：");
                ActivitySystem.Postgraduateget(id);
                System.out.println("请输入要删除的活动序号：");
                Scanner a_count = new Scanner(System.in);
                ActivitySystem.Postgraduatedelete(id, a_count.nextInt());
            }
            case 3 -> {
                System.out.println("您的学术交流活动情况为：");
                ActivitySystem.Postgraduateget(id);
                System.out.println("请输入要更改的活动序号：");
                Scanner a_count2 = new Scanner(System.in);
                ActivitySystem.Postgraduateupdate(id, a_count2.nextInt());
            }
            case 4 -> {
                System.out.println("您的学术交流活动情况为：");
                ActivitySystem.Postgraduateget(id);
            }
            case 5 -> PostgraduateMenu.studentMenu();
            default -> {
            }
        }
    }


    /**
     * 2.申报助教课程
     */
    public void applyAssistantCourse(){
        AssistantSystem assistant=new AssistantSystem();
        assistant.selectCourses(student);       //填报志愿
    }

    /**
     * 3.研究生填写助教工作自述
     */
    public void workSelfReport(){
        if (student.getAllot()==0){
            System.out.println("你暂时未被分配助教工作，无法完成助教工作自述！");
        }
        else if (student.getAllot()==1){
            AssistantSystem assistant=new AssistantSystem();
            assistant.fillDescription(student);
        }
    }

    /**
     * 4.填写《研究生参与项目认定表》
     */
    public void projectTable(){
        ProjectSystem ps=new ProjectSystem();
        ps.Project();
    }

    /**
     * 5.提交满足毕业成果认定标准的成果及佐证材料
     */
    public static void submitAchievement() {
        System.out.println("""
                \t\t[1].硕士研究生
                \t\t[2].博士研究生
                \t\t[3].返回上一级""");
        System.out.println("请选择你的角色：");
        String selection = input.next();
        switch (selection) {
            case "1" -> master();
            case "2" -> doctor();
            case "3" -> PostgraduateMenu.studentMenu();
            default -> {
                System.out.println("输入错误！请重新输入");
                submitAchievement();
            }
        }
    }


}
