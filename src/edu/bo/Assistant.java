package edu.bo;

import edu.vo.Course;
import edu.vo.Postgraduate;
import edu.vo.Teacher;

/**
 * 助教实体类
 */
public class Assistant {
    private Course course;
    private Postgraduate postgraduate;
    private Teacher teacher;

    /**
     * 学生工作自述
     */
    private String selfDescription;

    /**
     * 教师评价
     */
    private String teacherEvaluation;

    /**
     * 评价结果
     */
    private String evaluationGrade;

    /**
     * 该课程的优先级，是一个非负数
     */
    private int priority;

    public Assistant() {
        course=new Course();
        postgraduate=new Postgraduate();
        teacher=new Teacher();
        selfDescription=null;
        teacherEvaluation=null;
        evaluationGrade=null;
        priority=0;
    }

    public Assistant(Course course, Postgraduate postgraduate, Teacher teacher, String selfDescription, String teacherEvaluation, String evaluationGrade, int priority) {
        this.course = course;
        this.postgraduate = postgraduate;
        this.teacher = teacher;
        this.selfDescription = selfDescription;
        this.teacherEvaluation = teacherEvaluation;
        this.evaluationGrade = evaluationGrade;
        this.priority = priority;
    }

    /**
     * 根据课程的学时数和授课人数计算初始优先级
     * 计算公式为priority=int(hour*0.04+number*0.06)
     * @return 初始优先级
     */
    private int calPriority(){
        int hour=course.getHour();
        int studentNum=course.getStudent_num();
        return (int) (hour*0.04+studentNum*0.06);
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Postgraduate getPostgraduate() {
        return postgraduate;
    }

    public void setPostgraduate(Postgraduate postgraduate) {
        this.postgraduate = postgraduate;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public String getSelfDescription() {
        return selfDescription;
    }


    public void setSelfDescription(String selfDescription) {
        this.selfDescription = selfDescription;
    }

    public String getTeacherEvaluation() {
        return teacherEvaluation;
    }

    public void setTeacherEvaluation(String teacherEvaluation) {
        this.teacherEvaluation = teacherEvaluation;
    }

    public String getEvaluationGrade() {
        return evaluationGrade;
    }

    public void setEvaluationGrade(String evaluationGrade) {
        this.evaluationGrade = evaluationGrade;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Assistant{" +
                "course=" + course +
                ", postgraduate=" + postgraduate +
                ", teacher=" + teacher +
                ", selfDescription='" + selfDescription + '\'' +
                ", teacherEvaluation='" + teacherEvaluation + '\'' +
                ", evaluationGrade='" + evaluationGrade + '\'' +
                ", priority=" + priority +
                '}';
    }
}
