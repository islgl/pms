package edu.bo;

import edu.vo.Subject;

/**
 * @author Liu Guoli
 * 学科毕业要求
 */
public class GraduationDemand {
    private Subject subject;
    /**
     * 助教可选志愿数量
     */
    private int aspiration;

    /**
     * 学术交流次数要求
     */
    private int exchange;

    /**
     * 项目经费要求
     */
    private double expenditure;

    public GraduationDemand() {
        subject = null;
        aspiration = 0;
        exchange = 0;
        expenditure = 0;

    }

    public GraduationDemand(Subject subject, int aspiration, int exchange, double expenditure) {
        this.subject = subject;
        this.aspiration = aspiration;
        this.exchange = exchange;
        this.expenditure = expenditure;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int getAspiration() {
        return aspiration;
    }

    public void setAspiration(int aspiration) {
        this.aspiration = aspiration;
    }

    public int getExchange() {
        return exchange;
    }

    public void setExchange(int exchange) {
        this.exchange = exchange;
    }

    public double getExpenditure() {
        return expenditure;
    }

    public void setExpenditure(double expenditure) {
        this.expenditure = expenditure;
    }

    @Override
    public String toString() {
        return "GraduationDemand{" +
                "subject=" + subject +
                ", aspiration=" + aspiration +
                ", exchange=" + exchange +
                ", expenditure=" + expenditure +
                '}';
    }
}
