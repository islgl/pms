package edu.bo;

import edu.dao.DaoFactory;
import edu.dao.assistant.AssistantDaoImpl;
import edu.dao.graduationdemand.GraduationDemandDao;
import edu.dao.postgraduate.PostgraduateDao;
import edu.dao.tutor.TutorDAOImpl;
import edu.searchcriteria.*;
import edu.vo.*;

import java.util.List;
import java.util.Scanner;

/**
 * @author Liu Guoli
 * 学科负责人业务逻辑
 */
public class SubjectLeaderBusiness {
    private final SubjectLeader leader;

    public SubjectLeaderBusiness(SubjectLeader leader) {
        this.leader = leader;
    }


    @Override
    public String toString() {
        return leader.toString();
    }

    /**
     * 维护当前学科助教课程优先级
     */
    public void setPriority() {
        // 取出该负责人负责学科的所有需要助教的课程
        AssistantDaoImpl dao = DaoFactory.getAssistantDao();
        List<Assistant> assistants = dao.findByMajor(leader.getSubject(), false);
        System.out.println("该学科所有需要助教的课程信息如下:");
        System.out.println("序号, 课程号, 课程名, 授课人数, 授课对象, 课程性质, 当前优先级");
        int i = 0;
        for (Assistant assistant : assistants) {
            Course course = assistant.getCourse();
            System.out.println(++i + ", " + course.getId() + ", " + course.getName() + ", " + course.getStudent_num() + ", " +
                    course.getProperties() + ", " + course.getPrelect_object() + ", " + String.valueOf(assistant.getPriority()));
        }

        Scanner in = new Scanner(System.in);
        String ifContinue = "y";
        while (ifContinue.equals("y")) {
            System.out.println("请输入要设置优先级的课程号:");
            String courseId = in.next();

            // 从课程列表中取出该课程
            Assistant assistant = getAssistant(assistants, courseId);

            // 检查该课程是否在该学科需要助教的课程列表中
            if (assistant == null) {
                System.out.println("该课程不需要助教或不属于您负责的学科!");
            } else {
                System.out.println("请输入该课程新的优先级:");
                int priority = in.nextInt();
                while (priority < 0) {
                    System.out.println("您输入的优先级有误，请重新输入:");
                    priority = in.nextInt();
                }

                // 修改优先级并写入数据库
                assistant.setPriority(priority);
                dao.update(assistant);
                System.out.println("修改成功!");
            }

            System.out.println("是否继续[y/n]");
            ifContinue = in.next();
        }
    }

    /**
     * 维护当前学科助教志愿数量
     */
    public void setAspirationNum() {
        GraduationDemandDao dao = DaoFactory.getInstance().getGraduationDemandDao();
        Scanner in = new Scanner(System.in);
        GraduationDemand demand = dao.get(leader.getSubject());

        System.out.println("请输入新的助教可选志愿数量");
        int aspiration = in.nextInt();
        while (aspiration < 2) {
            System.out.println("您输入的数量有误，请重新输入:");
            aspiration = in.nextInt();
        }

        // 如果该学科的毕业要求尚未录入，则添加至数据库
        if (demand == null) {
            Subject subject = DaoFactory.getInstance().getSubjectDao().get(leader.getSubject());
            demand = new GraduationDemand();
            demand.setSubject(subject);
            demand.setAspiration(aspiration);
            dao.add(demand);
        } else {
            demand.setAspiration(aspiration);
            dao.update(leader.getSubject(), demand);
        }
        System.out.println("修改成功!");
    }

    /**
     * 设置当前学科参与项目经费要求
     */
    public void setExpenditureDemand() {
        GraduationDemandDao dao = DaoFactory.getInstance().getGraduationDemandDao();
        Scanner in = new Scanner(System.in);
        GraduationDemand demand = dao.get(leader.getSubject());
        System.out.println("请输入新的参与项目经费要求:");
        double expenditure = in.nextDouble();
        while (expenditure <= 0) {
            System.out.println("您输入经费要求不合法，请重新输入:");
            expenditure = in.nextDouble();
        }

        if (demand == null) {
            Subject subject = DaoFactory.getInstance().getSubjectDao().get(leader.getSubject());
            demand = new GraduationDemand();
            demand.setSubject(subject);
            demand.setExpenditure(expenditure);
            dao.add(demand);
        } else {
            demand.setExpenditure(expenditure);
            dao.update(leader.getSubject(), demand);
        }

        System.out.println("修改成功!");
    }

    /**
     * 设置当前学科参与学术交流次数要求
     */
    public void setExchangeDemand() {
        Scanner in = new Scanner(System.in);
        GraduationDemandDao dao = DaoFactory.getInstance().getGraduationDemandDao();
        GraduationDemand demand = dao.get(leader.getSubject());
        System.out.println("请输入学术交流次数要求:");
        int exchange = in.nextInt();
        while (exchange <= 0) {
            System.out.println("您输入的次数不合法，请重新输入:");
            exchange = in.nextInt();
        }

        if (demand == null) {
            Subject subject = DaoFactory.getInstance().getSubjectDao().get(leader.getSubject());
            demand = new GraduationDemand();
            demand.setSubject(subject);
            demand.setExchange(exchange);
            dao.add(demand);
        } else {
            demand.setExchange(exchange);
            dao.update(leader.getSubject(), demand);
        }
        System.out.println("修改成功!");
    }

    /**
     * 查看当前学科导师信息
     */
    public void browseTutorInfo() {
        TutorDAOImpl dao = DaoFactory.getTutorDao();
        TutorSearchCriteria criteria = new TutorSearchCriteria(null, null,null, leader.getSubject());
        List<Tutor> tutorList = dao.findTutors(criteria);
        if (tutorList.isEmpty()) {
            System.out.println("该学科当前暂无导师！");
        } else {
            for (Tutor tutor : tutorList) {
                System.out.println(tutor);
            }
        }
    }

    /**
     * 查看当前学科研究生培养情况
     */
    public void browsePostgraduateInfo() {
        PostgraduateDao dao = DaoFactory.getPostgraduateDao();
        PostgraduateSearchCriteria criteria = new PostgraduateSearchCriteria(null, null, leader.getSubject(), null, null, -1, -1);
        List<Postgraduate> postgraduates = dao.find_AND(criteria);
        for (Postgraduate postgraduate : postgraduates) {
            System.out.println(postgraduate);
        }
        System.out.println("请输入要查看具体情况的研究生的学号:");
        Scanner in = new Scanner(System.in);
        String id = in.next();
        Postgraduate postgraduate = getPostgraduate(postgraduates, id);
        while (postgraduate == null) {
            System.out.println("您输入的学号有误，请重新输入!");
            id = in.next();
            postgraduate = getPostgraduate(postgraduates, id);
        }
        showInfo(postgraduate);
    }

    /**
     * 从需要助教的课程列表中取出指定课程号的课程
     *
     * @param assistants 该学科需要助教的课程列表
     * @param courseId   需要取出的课程号
     * @return 如果该课程存在于列表中，则取出并返回；如果不存在则返回null
     */
    public Assistant getAssistant(List<Assistant> assistants, String courseId) {
        for (Assistant assistant : assistants) {
            if (assistant.getCourse().getId().equals(courseId)) {
                return assistant;
            }
        }
        return null;
    }

    /**
     * 从该学科的研究生列表中取出指定的研究生对象
     *
     * @param postgraduates  该学科的研究生列表
     * @param postgraduateId 需要取出的学号
     * @return 如果该研究生信息存在于列表中，则取出并返回；如果不存在则返回null
     */
    public Postgraduate getPostgraduate(List<Postgraduate> postgraduates, String postgraduateId) {
        for (Postgraduate postgraduate : postgraduates) {
            if (postgraduate.getId().equals(postgraduateId)) {
                return postgraduate;
            }
        }
        return null;
    }

    /**
     * 输出研究生的详细培养情况
     *
     * @param postgraduate 研究生对象
     */
    public void showInfo(Postgraduate postgraduate) {
        // 助教担任情况
        showAssistantInfo(postgraduate);
        // 学术交流情况
        showExchangeInfo(postgraduate);
        // 参与项目情况
        showProjectInfo(postgraduate);
    }

    /**
     * 输出该学生的助教担任情况
     *
     * @param postgraduate 研究生对象
     */
    public void showAssistantInfo(Postgraduate postgraduate) {
        System.out.println("该学生的助教情况如下:");
        AssistantSearchCriteria assistantSearchCriteria = new AssistantSearchCriteria(null, postgraduate, null, null, null, null, -1);
        List<Assistant> assistants = DaoFactory.getAssistantDao().find(assistantSearchCriteria);
        if (assistants.isEmpty()) {
            System.out.println("该学生未担任助教。");
        } else {
            for (Assistant assistant : assistants) {
                StringBuilder builder=new StringBuilder("课程号: ");
                builder.append(assistant.getCourse().getId());
                builder.append(", 授课教师工号: ");
                builder.append(assistant.getTeacher().getId());
                builder.append(", 自我评价: ");
                builder.append(assistant.getSelfDescription());
                builder.append(", 教师评价: ");
                builder.append(assistant.getTeacherEvaluation());
                builder.append(", 评价等级: ");
                builder.append(assistant.getEvaluationGrade());
                System.out.println(builder.toString());
            }
        }
        System.out.println();
    }

    /**
     * 输出该学生的学术交流情况
     *
     * @param postgraduate 研究生对象
     */
    public void showExchangeInfo(Postgraduate postgraduate) {
        // 学术交流活动情况
        System.out.println("该学生参与学术交流活动情况如下:");
        ActivitySearchCriteria activitySearchCriteria = new ActivitySearchCriteria(postgraduate.getId(), null, null, null, null, null, null, null, null, null);
        List<Activity> activities = DaoFactory.getInstance().getActivityDao().findActivitys(activitySearchCriteria);
        if (activities.isEmpty()) {
            System.out.println("该学生未参与学术交流活动。");
        }
        System.out.println();
    }

    /**
     * 输出该学生的参与项目情况
     * @param postgraduate 研究生对象
     */
    public void showProjectInfo(Postgraduate postgraduate){
        // 参与项目情况
        System.out.println("该学生参与项目情况如下:");
        ProjectSearchCriteria projectSearchCriteria=new ProjectSearchCriteria(postgraduate.getId(),null,null,null,null,null,null,null,0,null);
        List<Project> projects=DaoFactory.getProjectDao().findProject(projectSearchCriteria);
        if (projects.isEmpty()){
            System.out.println("该学生未参与项目。");
        }else {
            for(Project project:projects){
                StringBuilder builder=new StringBuilder("项目编号: ");
                builder.append(project.getPid());
                builder.append(", 项目类型: ");
                builder.append(project.getPtype());
                builder.append(", 项目名称: ");
                builder.append(project.getPname());
                builder.append(", 参与时间: ");
                builder.append(project.getPtime());
                builder.append(", 承担工作: ");
                builder.append(project.getPwork());
                builder.append(", 折合经费: ");
                builder.append(project.getPexp());
                builder.append(", 导师编号: ");
                builder.append(project.getTid());
                System.out.println(builder.toString());

            }
        }
        System.out.println();
    }
}

