package edu.bo;

import edu.vo.*;
import edu.dao.DaoFactory;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/*
* 成果实体类
* */
public class Achievement {
    private static final Scanner input=new Scanner(System.in);
    //论文
    public static Thesis thesis() {
        List<String>numbers=DaoFactory.getInstance().getThesisDAO().findAllNumber();

        Thesis thesis=new Thesis();//生成一个Thesis对象
        System.out.println("-----论文编号：");
        String thesis_no=input.next();//输入论文编号
        while (numbers.contains(thesis_no)){
            System.out.println("该编号已存在，请重新输入：");
            System.out.println("-----论文编号：");
            thesis_no=input.next();//输入论文编号
        }
        thesis.setThesis_no(thesis_no);

        System.out.println("-----论文名称：");//输入论文名称
        String name_thesis=input.next();
        thesis.setName_thesis(name_thesis);

        System.out.println("-----作者姓名：");//输入作者姓名
        String name_author=input.next();
        thesis.setName_author(name_author);

        System.out.println("-----论文发表刊物名称：");//输入刊物名称
        String name_publication=input.next();
        thesis.setName_publication(name_publication);

        System.out.println("-----论文状态（1.录用未发表; 2.已发表）：");//选择论文状态
        String state_thesis=input.next();
        if (Objects.equals(state_thesis, "1")){//若论文状态未“录用未发表”,则讲该论文的“发表时间”设置为“无”
            thesis.setState_thesis("录用未发表");
            thesis.setDate_publication("2000-01-01");
        }
        if (Objects.equals(state_thesis, "2")){//若论文“已发表”，则输入其发表实践
            thesis.setState_thesis("已发表");
            System.out.println("-----发表时间：");
            String date_publication=input.next();
            thesis.setDate_publication(date_publication);
        }

        System.out.println("-----索引类型：");//输入论文的索引类型
        String type_index=input.next();
        thesis.setType_index(type_index);

        System.out.println("-----论文归属库情况（1.学院高质量论文库; 2.学院核心论文库）：");//选择论文归属库情况
        String library=input.next();
        if (Objects.equals(library, "1")){
            thesis.setAttribution_library("学院高质量论文库");
        }
        if (Objects.equals(library, "2")){
            thesis.setAttribution_library("学院核心论文库");
        }

        System.out.println("-----论文扫描或PDF材料：");//输入论文的的pdf材料
        String scan_pdf=input.next();
        thesis.setScan_pdf(scan_pdf);

        thesis.setTutor_exam("未审核");//默认导师审核的状态未“未通过”
        thesis.setManager_exam("未审核");//默认研究生培养管理人员审核的状态未“未通过”

        return thesis;//返回一个thesis对象
    }

    //奖励
    public static Reward reward(){
        List<String> numbers=DaoFactory.getInstance().getRewardDAO().findAllNumber();
        Reward reward=new Reward();

        //输入奖励编号
        System.out.println("-----奖励编号：");
        String reward_no=input.next();
        while (numbers.contains(reward_no)){
            System.out.println("该编号已存在，请重新输入：");
            System.out.println("-----奖励编号：");
            reward_no=input.next();
        }
        reward.setReward_no(reward_no);     //为对象的编号赋值

        //奖励名称
        System.out.println("-----奖励名称：");
        String name_reward=input.next();
        reward.setName_reward(name_reward);   //为对象的奖励名称赋值

        //获奖者姓名
        System.out.println("-----获奖者姓名：");
        String name_winner=input.next();
        reward.setName_winner(name_winner);   //为对象的获奖者姓名赋值

        //奖励等级（国家级、省部级、市级、其他）
        System.out.println("-----奖励等级(1.国家级 2.省部级 3.市级 4.其他)：");
        //为对象的奖励等级赋值
        String level_reward=input.next();
        if (Objects.equals(level_reward, "1")){
            reward.setLevel_reward("国家级");
        }
        if (Objects.equals(level_reward,"2")){
            reward.setLevel_reward("省部级");
        }
        if (Objects.equals(level_reward,"3")){
            reward.setLevel_reward("市级");
        }
        if (Objects.equals(level_reward,"4")){
            reward.setLevel_reward("其他");
        }

        //获奖等级（特等奖、一等奖、二等奖、三等奖）
        System.out.println("-----获奖等级（1.特等奖 2.一等奖 3.二等奖 4.三等奖）：");
        String level_award=input.next();    //为对象的获奖等级赋值
        if (Objects.equals(level_award, "1")){
            reward.setLevel_award("特等奖");
        }
        if (Objects.equals(level_award, "2")){
            reward.setLevel_award("一等奖");
        }
        if (Objects.equals(level_award, "3")){
            reward.setLevel_award("二等奖");
        }
        if (Objects.equals(level_award, "4")){
            reward.setLevel_award("三等奖");
        }

        //排名
        System.out.println("-----排名：");
        int rank=input.nextInt();
        reward.setRanking(rank);   //为对象的排名赋值

        //获奖时间
        System.out.println("-----获奖时间：");
        String date_award=input.next();
        reward.setDate_award(date_award);   //为对象的获奖时间赋值

        //佐证材料
        System.out.println("-----佐证材料：");
        String material=input.next();
        reward.setSupport_materials(material);   //为对象的佐证材料赋值

        reward.setTutor_exam("未审核");
        reward.setManager_exam("未审核");

        return reward;
    }

    //教材
    public static Textbook textbook(){
        Textbook textbook=new Textbook();
        List<String> number=DaoFactory.getInstance().getTextbookDAO().findAllNumber();

        //编号
        System.out.println("-----教材编号：");
        String book_no=input.next();
        while (number.contains(book_no)){
            System.out.println("该编号已存在，请重新输入：");
            System.out.println("-----教材编号：");
            book_no=input.next();
        }
        textbook.setTextbook_no(book_no);

        //作者姓名
        System.out.println("-----作者姓名：");
        String name_author=input.next();
        textbook.setName_author(name_author);

        //教材名称
        System.out.println("-----教材名称：");
        String name_book=input.next();
        textbook.setName_textbook(name_book);

        //出版社名称
        System.out.println("-----教材出版社名称：");
        String publish=input.next();
        textbook.setPublish_house(publish);

        //出版时间
        System.out.println("-----出版时间：");
        String date=input.next();
        textbook.setDate_publish(date);

        //贡献度
        System.out.println("-----贡献度：");
        int contribution=input.nextInt();
        textbook.setContribution_textbook(contribution);

        //佐证材料
        System.out.println("-----佐证材料：");
        String material=input.next();
        textbook.setSupport_materials(material);

        textbook.setTutor_exam("未审核");
        textbook.setManager_exam("未审核");

        return textbook;
    }

    //标准
    public static Standards standard(){
        Standards standard=new Standards();
        List<String> number=DaoFactory.getInstance().getStandardDAO().findAllNumber();
        System.out.println("-----编号：");          //编号
        String standard_no=input.next();
        while (number.contains(standard_no)){
            System.out.println("该编号已存在，请重新输入：");
            System.out.println("-----编号：");          //编号
            standard_no=input.next();
        }
        standard.setStandard_no(standard_no);

        System.out.println("-----研究生姓名：");         //研究生姓名
        String name_person=input.next();
        standard.setName_person(name_person);

        System.out.println("-----标准名称：");           //标准名称
        String name_standard=input.next();
        standard.setName_standard(name_standard);

        System.out.println("-----标准级别（1.国家标准；2.省部级地方标准；3.行业标准；4.团队标准）：");
        String level=input.next();
        if (Objects.equals(level, "1"))
            standard.setLevel_standard("国家标准");
        if (Objects.equals(level, "2"))
            standard.setLevel_standard("省部级地方标准");
        if (Objects.equals(level, "3"))
            standard.setLevel_standard("行业标准");
        if (Objects.equals(level, "4"))
            standard.setLevel_standard("团队标准");

        System.out.println("-----发布时间：");
        String date=input.next();
        standard.setDate_release(date);

        System.out.println("-----佐证材料：");
        String material=input.next();
        standard.setSupport_materials(material);

        standard.setTutor_exam("未审核");
        standard.setManager_exam("未审核");

        return standard;
    }

    //报告
    public static Report report(){
        Report report=new Report();
        List<String> number=DaoFactory.getInstance().getReportDAO().findAllNumber();
        System.out.println("-----编号：");
        String no=input.next();
        while (number.contains(no)){
            System.out.println("该编号已存在，请重新输入：");
            System.out.println("-----编号：");
            no=input.next();
        }
        report.setReport_no(no);

        System.out.println("-----学生姓名：");
        String name_student=input.next();
        report.setName_student(name_student);

        System.out.println("-----报告名称：");
        String name_report=input.next();
        report.setName_report(name_report);

        System.out.println("-----报告类型（1.规划类；2.设计类；3.服务类；4.其他）：");
        String type=input.next();
        if (Objects.equals(type, "1"))
            report.setType_report("规划类");
        if (Objects.equals(type, "2"))
            report.setType_report("设计类");
        if (Objects.equals(type, "3"))
            report.setType_report("服务类");
        if (Objects.equals(type, "4"))
            report.setType_report("其他");

        System.out.println("-----报告服务单位：");
        String service=input.next();
        report.setService_report(service);

        System.out.println("-----报告时间");
        String date=input.next();
        report.setDate_report(date);

        System.out.println("-----贡献度排名：");
        int rank =input.nextInt();
        report.setContribution_ranking(rank);

        System.out.println("-----佐证材料：");
        String material=input.next();
        report.setSupport_materials(material);

        report.setTutor_exam("未审核");
        report.setManager_exam("未审核");

        return report;
    }

    //专利
    public static Patent patent(){
        Patent patent=new Patent();
        List<String> number=DaoFactory.getInstance().getPatentDAO().findAllNumber();

        //专利号
        System.out.println("-----专利号：");
        String patent_no=input.next();
        while (number.contains(patent_no)){
            System.out.println("该编号已存在，请重新输入：");
            System.out.println("-----专利号：");
            patent_no=input.next();
        }
        patent.setPatent_no(patent_no);

        //学生姓名
        System.out.println("-----学生姓名：");
        String student_name=input.next();
        patent.setName_student(student_name);

        //专利名称
        System.out.println("-----专利名称：");
        String patent_name=input.next();
        patent.setPatent_name(patent_name);

        //专利类型
        System.out.println("-----专利类型（1.发明专利; 2.实用新型专利）：");
        String type_patent=input.next();
        if (Objects.equals(type_patent, "1")){
            patent.setType_patent("发明专利");
        }
        if (Objects.equals(type_patent, "2")){
            patent.setType_patent("实用新型专利");
        }

        //专利状态
        System.out.println("-----专利状态：");//选择专利状态
        String patent_zhuangtai=input.next();
        patent.setPatent_zhuangtai(patent_zhuangtai);

        //专利发布时间
        System.out.println("-----专利发布时间：");
        String public_date=input.next();
        patent.setPublic_date(public_date);

        //贡献度排名
        System.out.println("-----贡献度排名：");
        int contribution=input.nextInt();
        patent.setContribution(contribution);

        //佐证材料
        System.out.println("-----佐证材料：");
        String zuozheng=input.next();
        patent.setZuozheng(zuozheng);

        patent.setTutor_exam("未审核");
        patent.setManager_exam("未审核");

        return patent;

    }

    //软硬件开发平台
    public static Pingtai pingtai(){
        Pingtai pingtai=new Pingtai();
        List<String> number=DaoFactory.getInstance().getPingtaiDAO().findAllNumber();
        //编号
        System.out.println("-----编号:");
        String no=input.next();
        while (number.contains(no)){
            System.out.println("该编号已存在，请重新输入：");
            System.out.println("-----编号：");
            no=input.next();
        }
        pingtai.setPingtai_no(no);

        //学生姓名
        System.out.println("-----学生姓名：");
        String student_name=input.next();
        pingtai.setName_student(student_name);

        //名称
        System.out.println("-----平台名称：");
        String pingtai_name=input.next();
        pingtai.setPingtai_name(pingtai_name);

        //平台服务单位
        System.out.println("-----平台服务单位：");
        String service_unit=input.next();
        pingtai.setService_unit(service_unit);


        //平台上线时间
        System.out.println("-----平台上线时间：");
        String public_date=input.next();
        pingtai.setPublic_date(public_date);

        //贡献度
        System.out.println("-----贡献度：");
        int contribution=input.nextInt();
        pingtai.setContribution(contribution);

        //佐证材料
        System.out.println("-----佐证材料：");
        String zuozheng=input.next();
        pingtai.setZuozheng(zuozheng);

        pingtai.setTutor_exam("未审核");
        pingtai.setManager_exam("未审核");

        return pingtai;

    }
}
