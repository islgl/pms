package edu.bo;

import edu.vo.Expenditure;
import edu.vo.Tutor;
import edu.dao.DaoFactory;

import java.util.Scanner;


public class TutorBusiness {
    private final Tutor leader;

    public TutorBusiness(Tutor leader) {
        this.leader = leader;
    }

    public static void main(String[] args) {
    }

    @Override
    public String toString() {
        return leader.toString();
    }


    /**
     * 审核学术交流情况
     */
    public void testUpdateCheck(){

    }

    /**
     * 填报学生经费数量
     */
    public void expense(){
        Scanner in = new Scanner(System.in);
        System.out.println("请输入要填报的学生学号:");
        String studentId = in.next();
        Expenditure expenditure = DaoFactory.getExpenditureDao().getexpenditure(studentId);
        System.out.println("请输入综合后的经费数量:");
        Float fee = in.nextFloat();
        expenditure.setExpenditure(fee);
        expenditure.setTeacher_signature(1);
        DaoFactory.getExpenditureDao().updateexpenditure(studentId,expenditure);
    }


    /**
     * 成果初审认定
     */
    public void TutorExam(){

    }


}
