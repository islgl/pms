package edu.bo;

import edu.dao.DaoFactory;
import edu.dao.thesis.ThesisDAOImpl;
import edu.searchcriteria.*;
import edu.vo.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class ManagerExam {
    private static final Scanner input=new Scanner(System.in);

    /*
    * 管理人员终审研究生的论文
    * */
    public static void exam_thesis(){
        //查询管理人员审核（终审）未通过 且 导师审核通过（初审通过）的所有数据信息
        ThesisSearchCriteria t= new ThesisSearchCriteria(null,null,null,
                null,null,null,null,null,
                null,null,"审核通过","未审核");
        List<Thesis> theses=new ThesisDAOImpl().findThesis(t);
        List<String> number=new ArrayList<>();//用一个列表存储查询到的数据的论文编号，用来和输入的编号进行比对
        if (theses.isEmpty()){
            System.out.println("-----暂无数据-----");
        }
        else {
            System.out.println("\n待认证的成果数据如下所示：");
            for (Thesis thesis : theses) {
                DaoFactory.getInstance().getThesisDAO().getThesis(thesis.getThesis_no());
                number.add(thesis.getThesis_no());
            }

            System.out.println("\n请输入（终审）要处理的编号：");
            String thesis_no = input.next();
            while (!number.contains(thesis_no)) {
                System.out.println("该论文编号有误，请重新输入：");
                thesis_no = input.next();
            }
            System.out.println("\n----------处理前的数据信息为：");
            Thesis thesis = DaoFactory.getInstance().getThesisDAO().getThesis(thesis_no);
            System.out.println("\n请输入对该条记录的审核结果（1.审核通过；2.审核不通过<会删掉相应的数据>）：");
            String select = input.next();
            if (Objects.equals(select, "1")) {
                thesis.setManager_exam("审核通过");
                DaoFactory.getInstance().getThesisDAO().updateThesis(thesis, thesis_no);
                System.out.println("----------审核通过后的数据信息为(终审)：");
                DaoFactory.getInstance().getThesisDAO().getThesis(thesis_no);
            }
            if (Objects.equals(select, "2")){
                System.out.println("\n该成果终审未通过！！！！！");
                DaoFactory.getInstance().getThesisDAO().deleteThesis(thesis_no);
            }
        }
    }


    /*
    * 研究生培养管理人员审核博士研究生的奖励
    * */
    public static void exam_reward(){
        //通过条件查询，找出符合查询条件的数据，并将之存在一个列表里
        RewardSearchCriteria re=new RewardSearchCriteria(null,null,null, null,
                null,0,null,null,"审核通过","未审核 ");
        List<Reward> rewards=DaoFactory.getInstance().getRewardDAO().findRewards(re);
        List<String> number=new ArrayList<>();  //新建一个String类型的列表，这个列表用来存储条件查询结果的编号
        if (rewards.isEmpty()){
            //如果列表rewards为空，即条件查询的结果为空（表中没有符合查询条件的数据）
            System.out.println("-----暂无数据-----");
        }
        else{
            //用for循环输出列表rewards中的所有数据，并将每一个Reward对象的编号值添加到列表number中
            System.out.println("\n待认证的成果数据如下所示：");
            for (Reward reward:rewards){
                DaoFactory.getInstance().getRewardDAO().getReward(reward.getReward_no());
                number.add(reward.getReward_no()); //将查询到的数据的编号添加到列表numbers中
            }
            //输入要给予审核通过的编号，将之与number列表中的数据进行对比，只能修改/更新number中存在的编号对应的数据，否则重新输入
            System.out.println("\n请输入（终审）要处理的编号：");
            String reward_no = input.next();
            while (!number.contains(reward_no)) {
                System.out.println("该编号有误，请重新输入：");
                reward_no = input.next();
            }
            //将相关的数据的导师审核字段的值修改位“审核通过”，并输出更新前后的数据
            System.out.println("\n----------处理前的数据信息为：");
            Reward reward = DaoFactory.getInstance().getRewardDAO().getReward(reward_no);
            System.out.println("\n请输入对该条记录的审核结果（1.审核通过；2.审核不通过<会删掉相应的数据>）：");
            String select=input.next();
            if (Objects.equals(select, "1")) {
                reward.setManager_exam("审核通过");
                DaoFactory.getInstance().getRewardDAO().updateReward(reward, reward_no);
                System.out.println("\n----------审核通过后的数据信息为(终审)：");
                DaoFactory.getInstance().getRewardDAO().getReward(reward_no);
            }
            if (Objects.equals(select, "2")){
                System.out.println("\n该成果终审未通过！！！！！");
                DaoFactory.getInstance().getRewardDAO().deleteReward(reward_no);
            }
        }
    }


    /*
    * 研究生培养管理员审核研究生的教材
    * */
    public static void exam_textbook(){
        //通过条件查询，找出符合查询条件的数据，并将之存在一个列表里
        TextbookSearchCriteria tb=new TextbookSearchCriteria(null,null,null, null,
                null,null,0,null,"审核通过","未审核");
        List<Textbook> books=DaoFactory.getInstance().getTextbookDAO().findTextbooks(tb);
        List<String> number=new ArrayList<>();  //新建一个String类型的列表，这个列表用来存储条件查询结果的编号
        if (books.isEmpty()){
            //如果列表rewards为空，即条件查询的结果为空（表中没有符合查询条件的数据）
            System.out.println("-----暂无数据-----");
        }
        else{
            //用for循环输出列表books中的所有数据，并将每一个Textbook的对象的编号值添加到列表number中
            System.out.println("\n待认证的成果数据如下所示：");
            for (Textbook book:books){
                DaoFactory.getInstance().getTextbookDAO().getTextbook(book.getTextbook_no());
                number.add(book.getTextbook_no()); //将查询到的数据的编号添加到列表numbers中
            }
            //输入要给予审核通过的编号，将之与number列表中的数据进行对比，只能修改/更新number中存在的编号对应的数据，否则重新输入
            System.out.println("\n请输入（终审）要处理的编号：");
            String book_no = input.next();
            while (!number.contains(book_no)) {
                System.out.println("该编号有误，请重新输入：");
                book_no = input.next();
            }
            //将相关的数据的管理人员审核字段的值修改位“审核通过”，并输出更新前后的数据
            System.out.println("\n----------处理前的数据信息为：");
            Textbook textbook=DaoFactory.getInstance().getTextbookDAO().getTextbook(book_no);
            System.out.println("\n请输入对该条记录的审核结果（1.审核通过；2.审核不通过<会删掉相应的数据>）：");
            String select=input.next();
            if (Objects.equals(select, "1")) {
                textbook.setManager_exam("审核通过");
                DaoFactory.getInstance().getTextbookDAO().updateTextbook(textbook, book_no);
                System.out.println("\n----------审核通过后的数据信息为(终审)：");
                DaoFactory.getInstance().getTextbookDAO().getTextbook(book_no);
            }
            if (Objects.equals(select, "2")){
                System.out.println("\n该成果终审未通过！！！！！");
                DaoFactory.getInstance().getTextbookDAO().deleteTextbook(book_no);
            }
        }
    }


    /*
     * 研究生培养管理员审核研究生的“标准”
     * */
    public static void exam_standard(){
        StandardSearchCriteria ssc=new StandardSearchCriteria(null,null,null,null,
                null,null,null,"审核通过","未审核");
        List<Standards>standards=DaoFactory.getInstance().getStandardDAO().findStandards(ssc);
        List<String> number=new ArrayList<>();  //新建一个String类型的列表，这个列表用来存储条件查询结果的编号
        if (standards.isEmpty()){
            System.out.println("-----暂无数据-----");
        }
        else {
            System.out.println("\n待认证的成果数据如下所示：");
            for (Standards standard:standards){
                DaoFactory.getInstance().getStandardDAO().getStandard(standard.getStandard_no());
                number.add(standard.getStandard_no()); //将查询到的数据的编号添加到列表numbers中
            }
            System.out.println("\n请输入（终审）要处理的编号：");
            String standard_no = input.next();
            while (!number.contains(standard_no)) {
                System.out.println("该编号有误，请重新输入：");
                standard_no = input.next();
            }
            //将相关的数据的导师审核字段的值修改位“审核通过”，并输出更新前后的数据
            System.out.println("\n----------处理前的数据信息为：");
            Standards standards1 = DaoFactory.getInstance().getStandardDAO().getStandard(standard_no);
            System.out.println("\n请输入对该条记录的审核结果（1.审核通过；2.审核不通过<会删掉相应的数据>）：");
            String select=input.next();
            if (Objects.equals(select, "1")) {
                standards1.setManager_exam("审核通过");
                DaoFactory.getInstance().getStandardDAO().updateStandard(standards1, standard_no);
                System.out.println("\n----------审核通过后的数据信息为(终审)：");
                DaoFactory.getInstance().getStandardDAO().getStandard(standard_no);
            }
            if (Objects.equals(select, "2")){
                System.out.println("\n该成果终审未通过！！！！！");
                DaoFactory.getInstance().getStandardDAO().deleteStandard(standard_no);
            }
        }
    }


    /*
     * 研究生培养管理员审核研究生的“报告”
     * */
    public static void exam_report(){
        {
            //条件查询
            ReportSearchCriteria rsc=new ReportSearchCriteria(null,null,null,
                    null,null,null,null,0,
                    null,"审核通过","未审核");
            List<Report> reportList=DaoFactory.getInstance().getReportDAO().findReports(rsc);
            List<String> number=new ArrayList<>();  //新建一个String类型的列表，这个列表用来存储条件查询结果的编号
            if (reportList.isEmpty()){
                System.out.println("-----暂无数据-----");
            }
            else {
                System.out.println("\n待认证的成果数据如下所示：");
                for (Report r : reportList) {
                    DaoFactory.getInstance().getReportDAO().getReport(r.getReport_no());
                    number.add(r.getReport_no()); //将查询到的数据的编号添加到列表numbers中
                }
                System.out.println("\n请输入（终审）要处理的编号：");
                String report_no = input.next();
                while (!number.contains(report_no)) {
                    System.out.println("该编号有误，请重新输入：");
                    report_no = input.next();
                }
                System.out.println("\n----------处理前的数据信息为：");
                Report report1 = DaoFactory.getInstance().getReportDAO().getReport(report_no);
                System.out.println("\n请输入对该条记录的审核结果（1.审核通过；2.审核不通过<会删掉相应的数据>）：");
                String select=input.next();
                if (Objects.equals(select, "1")) {
                    //将相关的数据的导师审核字段的值修改位“审核通过”，并输出更新前后的数据
                    report1.setManager_exam("审核通过");
                    DaoFactory.getInstance().getReportDAO().updateReport(report1,report_no);
                    System.out.println("\n----------审核通过后的数据信息为(终审通过)：");
                    DaoFactory.getInstance().getReportDAO().getReport(report_no);
                }
                if (Objects.equals(select,"2")){
                    //将相关的数据从数据表中删除
                    System.out.println("\n该成果终审未通过！！！！！");
                    DaoFactory.getInstance().getReportDAO().deleteReport(report_no);
                }
            }
        }
    }


    /*
    * 研究生管理人员审核研究生的”专利“
    * */
    public static void exam_patent(){
        //条件查询
        PatentSearchCriteria psc=new PatentSearchCriteria(null,null,null,
                null,null,null,null,0,
                null,"审核通过","未审核");
        List<Patent> patentList=DaoFactory.getInstance().getPatentDAO().findPatents(psc);
        List<String> number=new ArrayList<>();  //新建一个String类型的列表，这个列表用来存储条件查询结果的编号
        if (patentList.isEmpty()){
            System.out.println("-----暂无数据-----");
        }
        else {
            System.out.println("\n待认证的成果数据如下所示：");
            for (Patent p : patentList) {
                DaoFactory.getInstance().getPatentDAO().getPatent(p.getPatent_no());
                number.add(p.getPatent_no()); //将查询到的数据的编号添加到列表numbers中
            }
            System.out.println("\n请输入（终审）要处理的编号：");
            String patent_no = input.next();
            while (!number.contains(patent_no)) {
                System.out.println("该编号有误，请重新输入：");
                patent_no = input.next();
            }
            System.out.println("\n----------处理前的数据信息为：");
            Patent patent1 = DaoFactory.getInstance().getPatentDAO().getPatent(patent_no);
            System.out.println("\n请输入对该条记录的审核结果（1.审核通过；2.审核不通过<会删掉相应的数据>）：");
            String select=input.next();
            if (Objects.equals(select, "1")) {
                //将相关的数据的导师审核字段的值修改位“审核通过”，并输出更新前后的数据
                patent1.setManager_exam("审核通过");
                DaoFactory.getInstance().getPatentDAO().updatePatent(patent1);
                System.out.println("\n----------审核通过后的数据信息为(终审通过)：");
                DaoFactory.getInstance().getPatentDAO().getPatent(patent_no);
            }
            if (Objects.equals(select,"2")){
                //将相关的数据从数据表中删除
                System.out.println("\n该成果终审未通过！！！！！");
                DaoFactory.getInstance().getPatentDAO().deletePatent(patent_no);
            }
        }
    }


    /*
     * 研究生管理人员研究生的软硬件平台开发
     * */
    public static void exam_pingtai(){
        //条件查询
        PingtaiSearchCriteria ptsc=new PingtaiSearchCriteria(null,null,null,
                null,null,null,0,null,
                "审核通过","未审核");
        List<Pingtai> ptList=DaoFactory.getInstance().getPingtaiDAO().findPingtai(ptsc);
        List<String> number=new ArrayList<>();  //新建一个String类型的列表，这个列表用来存储条件查询结果的编号
        if (ptList.isEmpty()){
            System.out.println("-----暂无数据-----");
        }
        else {
            System.out.println("\n待认证的成果数据如下所示：");
            for (Pingtai pt : ptList) {
                DaoFactory.getInstance().getPingtaiDAO().getPingtai(pt.getPingtai_no());
                number.add(pt.getPingtai_no()); //将查询到的数据的编号添加到列表numbers中
            }
            System.out.println("\n请输入（终审）要处理的编号：");
            String pt_no = input.next();
            while (!number.contains(pt_no)) {
                System.out.println("该编号有误，请重新输入：");
                pt_no = input.next();
            }
            System.out.println("\n----------处理前的数据信息为：");
            Pingtai pingtai1=DaoFactory.getInstance().getPingtaiDAO().getPingtai(pt_no);
            System.out.println("\n请输入对该条记录的审核结果（1.审核通过；2.审核不通过<会删掉相应的数据>）：");
            String select=input.next();
            if (Objects.equals(select, "1")) {
                //将相关的数据的导师审核字段的值修改位“审核通过”，并输出更新前后的数据
                pingtai1.setManager_exam("审核通过");
                DaoFactory.getInstance().getPingtaiDAO().updatePingtai(pingtai1);
                System.out.println("\n----------审核通过后的数据信息为(终审通过)：");
                DaoFactory.getInstance().getPingtaiDAO().getPingtai(pt_no);
            }
            if (Objects.equals(select,"2")){
                //将相关的数据从数据表中删除
                System.out.println("\n该成果终审未通过！！！！！");
                DaoFactory.getInstance().getPingtaiDAO().deletePingtai(pt_no);
            }
        }
    }

}
