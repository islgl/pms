package edu.dao;

import edu.dao.Expenditure.ExpenditureDaolmpl;
import edu.dao.activity.ActivityDaoImpl;
import edu.dao.assistant.AssistantDaoImpl;
import edu.dao.course.CourseDaoImpl;
import edu.dao.graduationdemand.GraduationDemandDaoImpl;
import edu.dao.patent.PatentDAOImpl;
import edu.dao.pingtai.PingtaiDAOImpl;
import edu.dao.postgraduate.PostgraduateDaoImpl;
import edu.dao.project.ProjectDaoImpl;
import edu.dao.report.ReportDAOImpl;
import edu.dao.reward.RewardDAOImpl;
import edu.dao.standards.StandardDAOImpl;
import edu.dao.subject.SubjectDaoImpl;
import edu.dao.subjectleader.SubjectLeaderDAO;
import edu.dao.subjectleader.SubjectLeaderDAOImpl;
import edu.dao.teacher.TeacherDAOImpl;
import edu.dao.textbook.TextbookDAOImpl;
import edu.dao.thesis.ThesisDAOImpl;
import edu.dao.tutor.TutorDAOImpl;
import edu.dao.user.UserDaoImpl;

/**
 * DAO工厂类
 */
public class DaoFactory {
    private static final DaoFactory daoFactory;

    static {
        daoFactory = new DaoFactory();
    }

    private DaoFactory() {

    }

    public static DaoFactory getInstance() {
        return daoFactory;
    }

    public static PostgraduateDaoImpl getPostgraduateDao() {
        return new PostgraduateDaoImpl();
    }

    public static CourseDaoImpl getCourseDao() {
        return new CourseDaoImpl();
    }

    public static AssistantDaoImpl getAssistantDao() {
        return new AssistantDaoImpl();
    }

    public static UserDaoImpl getUserDao(){return new UserDaoImpl();}

    public static ExpenditureDaolmpl getExpenditureDao() {
        return new ExpenditureDaolmpl();
    }

    public ThesisDAOImpl getThesisDAO() {
        return new ThesisDAOImpl();
    }

    public RewardDAOImpl getRewardDAO() {
        return new RewardDAOImpl();
    }

    public TextbookDAOImpl getTextbookDAO() {
        return new TextbookDAOImpl();
    }

    public StandardDAOImpl getStandardDAO() {
        return new StandardDAOImpl();
    }

    public ReportDAOImpl getReportDAO() {
        return new ReportDAOImpl();
    }

    public PatentDAOImpl getPatentDAO() {
        return new PatentDAOImpl();
    }

    public static ProjectDaoImpl getProjectDao() {
        return new ProjectDaoImpl();
    }

    public PingtaiDAOImpl getPingtaiDAO() {
        return new PingtaiDAOImpl();
    }

    public ActivityDaoImpl getActivityDao() {
        return new ActivityDaoImpl();
    }


    public SubjectDaoImpl getSubjectDao() {
        return new SubjectDaoImpl();
    }

    public static SubjectLeaderDAOImpl getSubjectLeaderDao(){return new SubjectLeaderDAOImpl();}

    public SubjectLeaderDAO getSubjectLeaderDAO() {
        return new SubjectLeaderDAOImpl();
    }

    public static TeacherDAOImpl getTeacherDao() { return new TeacherDAOImpl(); }
    
    public static TutorDAOImpl getTutorDao() { return new TutorDAOImpl(); }

    public GraduationDemandDaoImpl getGraduationDemandDao() {
        return new GraduationDemandDaoImpl();
    }
}
