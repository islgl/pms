package edu.dao.course;

import edu.searchcriteria.CourseSearchCriteria;
import edu.vo.Course;

import java.util.List;

public interface CourseDao {
    /**
     * 增加课程信息
     *
     * @param course 要增加的课程信息
     */
    void addCourse(Course course);

    /**
     * 更新课程信息
     *
     * @param course 需要更新的课程信息
     */
    void updateCourse(String courseId, Course course);

    /**
     * 删除课程信息
     *
     * @param courseId 需要删除的课程学号
     */
    void deleteCourse(String courseId);

    /**
     * 获取课程信息
     *
     * @param courseId 需要获取的课程学号
     */
    Course getCourse(String courseId);

    /**
     * 查询符合指定条件的课程信息
     *
     * @param searchCriteria 查询条件
     * @return 符合指定条件的课程信息列表
     */
    List<Course> findCourse(CourseSearchCriteria searchCriteria);
}
