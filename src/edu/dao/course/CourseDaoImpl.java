package edu.dao.course;

import edu.dao.DaoFactory;
import edu.db.DbConnection;
import edu.searchcriteria.CourseSearchCriteria;
import edu.searchcriteria.SearchCriteria;
import edu.vo.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CourseDaoImpl implements CourseDao {
    private static final String COURSE_INSERT_SQL = "INSERT INTO Course(id,name,student_num,subject,properties,Prelect_object,teacher,time,hour) VALUES(?,?,?,?,?,?,?,?,?) ";

    @Override
    public void addCourse(Course course) {
        Connection con = null;
        DbConnection dbConnection = new DbConnection();
        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(COURSE_INSERT_SQL);
            psmt.setString(1, course.getId());
            psmt.setString(2, course.getName());
            psmt.setInt(3, course.getStudent_num());
            psmt.setString(4, course.getSubject());
            psmt.setString(5, course.getProperties());
            psmt.setString(6, course.getPrelect_object());
            psmt.setString(7, course.getTeacher());
            psmt.setString(8, course.getTime());
            psmt.setInt(9, course.getHour());
            psmt.executeUpdate();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static final String COURSE_UPDATE_SQL = "UPDATE Course SET id=?,name=?,student_num=?,subject=?,properties=?,Prelect_object=?,teacher=?,time=?,hour=? WHERE id=?";

    @Override
    public void updateCourse(String courseId, Course course) {
        Connection con = null;
        DbConnection dbConnection = new DbConnection();

        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(COURSE_UPDATE_SQL);
            psmt.setString(1, course.getId());
            psmt.setString(2, course.getName());
            psmt.setInt(3, course.getStudent_num());
            psmt.setString(4, course.getSubject());
            psmt.setString(5, course.getProperties());
            psmt.setString(6, course.getPrelect_object());
            psmt.setString(7, course.getTeacher());
            psmt.setString(8, course.getTime());
            psmt.setInt(9, course.getHour());
            psmt.setString(10, courseId);
            psmt.executeUpdate();
            psmt.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static final String COURSE_DELETE_SQL = "DELETE FROM Course WHERE id=?";

    @Override
    public void deleteCourse(String courseId) {
        DbConnection dbConnection = new DbConnection();
        Connection con = null;

        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(COURSE_DELETE_SQL);
            psmt.setString(1, courseId);
            psmt.executeUpdate();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static final String COURSE_GET_SQL = "SELECT * FROM Course WHERE id=?";

    @Override
    public Course getCourse(String courseId) {
        Course course = null;
        DbConnection dbConnection = new DbConnection();
        PreparedStatement psmt = null;
        Connection con = null;
        ResultSet rs = null;

        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(COURSE_GET_SQL);
            psmt.setString(1, courseId);
            psmt.executeQuery();
            rs = psmt.getResultSet();

            while (rs.next()) {
                course = new Course(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getInt("student_num"),
                        rs.getString("subject"),
                        rs.getString("properties"),
                        rs.getString("Prelect_object"),
                        rs.getString("teacher"),
                        rs.getString("time"),
                        rs.getInt("hour")
                );
            }
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return course;
    }

    @Override
    public List<Course> findCourse(CourseSearchCriteria searchCriteria) {
        List<Course> course = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;

        con = dbConnection.getConnection();
        StringBuilder criteriaSql = new StringBuilder(512);
        String COURSE_FIND_SQL = "SELECT * FROM Course WHERE ";
        criteriaSql.append(COURSE_FIND_SQL);

        //构造查询语句
        if (searchCriteria.getId() != null) {
            criteriaSql.append("id='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getId()) + "' AND ");
        }
        if (searchCriteria.getName() != null) {
            criteriaSql.append("name='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getName()) + "' AND ");
        }
        if (searchCriteria.getStudent_num() != -1) {
            criteriaSql.append("student_num=" + SearchCriteria.fixSqlFieldValue(String.valueOf(searchCriteria.getStudent_num())) + " AND ");
        }
        if (searchCriteria.getSubject() != null) {
            criteriaSql.append("subject='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getSubject()) + "' AND ");
        }
        if (searchCriteria.getProperties() != null) {
            criteriaSql.append("properties='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getProperties()) + "' AND ");
        }
        if (searchCriteria.getPrelect_object() != null) {
            criteriaSql.append("Prelect_object='" + SearchCriteria.fixSqlFieldValue(String.valueOf(searchCriteria.getProperties())) + "' AND ");
        }
        if (searchCriteria.getTeacher() != null) {
            criteriaSql.append("teacher='" + SearchCriteria.fixSqlFieldValue(String.valueOf(searchCriteria.getTeacher())) + "' AND ");
        }
        if (searchCriteria.getTime() != null) {
            criteriaSql.append("time='" + SearchCriteria.fixSqlFieldValue(String.valueOf(searchCriteria.getTime())) + "' AND ");
        }
        if (searchCriteria.getHour() != -1) {
            criteriaSql.append("hour=" + SearchCriteria.fixSqlFieldValue(String.valueOf(searchCriteria.getHour())));
        }

        // 去除多余的WHERE和AND关键字
        COURSE_FIND_SQL = searchCriteria.removeRedundancy(criteriaSql);

        try {
            psmt = con.prepareStatement(COURSE_FIND_SQL);
            psmt.executeQuery();
            rs = psmt.getResultSet();

            while (rs.next()) {
                Course cou = new Course(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getInt("student_num"),
                        rs.getString("subject"),
                        rs.getString("properties"),
                        rs.getString("Prelect_object"),
                        rs.getString("teacher"),
                        rs.getString("time"),
                        rs.getInt("hour")
                );
                course.add(cou);
            }

            rs.close();
            psmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return course;
    }

    /**
     * 从视图中获取排序后的课程列表
     *
     * @return 经过排序后的课程列表
     */
    public List<Course> getSortedCourses() {
        List<Course> courses = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            String sql = "SELECT * FROM noAssistantCourseView ORDER BY priority DESC;";
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.executeQuery();
            rs = statement.getResultSet();

            while (rs.next()) {
                String courseId=rs.getString("id");
                Course course= DaoFactory.getCourseDao().getCourse(courseId);
                courses.add(course);
            }
            rs.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courses;
    }

}
