package edu.dao.patent;

import edu.db.DbConnection;
import edu.searchcriteria.PatentSearchCriteria;
import edu.searchcriteria.SearchCriteria;
import edu.vo.Patent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PatentDAOImpl implements PatentDAO {

    /*
     *向专利表中添加专利信息
     * */
    @Override
    public void addPatent(Patent patent) {
        String sql="insert into patent(patent_no, name_student, identify_student, patent_name, type_patent," +
                "public_date, patent_zhuangtai, contribution, zuozheng, tutor_exam, manager_exam) " +
                "values(?,?,?,?,?,?,?,?,?,?,?)";
        DbConnection dbConnection = new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,patent.getPatent_no());
            psmt.setString(2,patent.getName_student());
            psmt.setString(3,patent.getIdentify_student());
            psmt.setString(4,patent.getPatent_name());
            psmt.setString(5,patent.getType_patent());
            psmt.setString(6,patent.getPublic_date());
            psmt.setString(7,patent.getPatent_zhuangtai());
            psmt.setInt(8,patent.getContribution());
            psmt.setString(9,patent.getZuozheng());
            psmt.setString(10,patent.getTutor_exam());
            psmt.setString(11,patent.getManager_exam());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("向patent表中添加数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
     * 根据专利编号更新专利表中的信息
     * */
    @Override
    public void updatePatent(Patent patent) {
        String sql="update patent set tutor_exam=?,manager_exam=? where patent_no=?";
        DbConnection dbConnection = new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,patent.getTutor_exam());
            psmt.setString(2,patent.getManager_exam());
            psmt.setString(3,patent.getPatent_no());
            psmt.executeUpdate();
            psmt.close();
//            System.out.println("更新专利信息成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    /*
     * 根据专利编号删除专利表中的数据
     * */
    @Override
    public void deletePatent(String patent_no) {
        String sql="delete from patent where patent_no=?";
        DbConnection dbConnection = new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,patent_no);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("删除patent表中数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    /*
     * 根据专利编号获取指定专利信息
     * */
    @Override
    public Patent getPatent(String patent_no) {
        String sql="select * from patent where patent_no=?";
        DbConnection dbConnection = new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        ResultSet rs=null;
        Patent patent=new Patent();
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,patent_no);
            rs=psmt.executeQuery();
            while (rs.next()){
                //专利名称、专利类型、专利号、专利发布时间、专利状态、贡献度排名、佐证材料
                String patent_name=rs.getString("patent_name");
                String type_patent=rs.getString("type_patent");
                patent_no = rs.getString("patent_no");
                String public_date=rs.getString("public_date");
                String patent_zhuangtai=rs.getString("patent_zhuangtai");
                int contribution=rs.getInt("contribution");
                String zuozheng=rs.getString("zuozheng");
                String tutor_exam=rs.getString("tutor_exam");
                String manager_exam=rs.getString("manager_exam");
                String name_student=rs.getString("name_student");
                String identify=rs.getString("identify_student");

                patent.setPatent_name(patent_name);
                patent.setType_patent(type_patent);
                patent.setPatent_no(patent_no);
                patent.setPublic_date(public_date);
                patent.setPatent_zhuangtai(patent_zhuangtai);
                patent.setContribution(contribution);
                patent.setZuozheng(zuozheng);
                patent.setTutor_exam(tutor_exam);
                patent.setManager_exam(manager_exam);
                patent.setName_student(name_student);
                patent.setIdentify_student(identify);
                System.out.println("专利号:"+patent_no+" 学生姓名:"+name_student+" 学生身份:"+identify+
                        " 专利名称:"+patent_name+" 专利类型:"+type_patent+" 专利发布时间:"+ public_date+" 专利状态:"+
                        patent_zhuangtai+" 贡献度排名:"+contribution+" 佐证材料:"+zuozheng+
                        " 导师初审:"+tutor_exam+" 管理员终审:"+manager_exam);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return patent;
    }


    /*
     * 查询专利表中符合查询条件的所有专利信息
     * */
    @Override
    public List<Patent> findPatents(PatentSearchCriteria criteria) {
        DbConnection dbConnection = new DbConnection();
        List<Patent> patents=new ArrayList<Patent>();
        Connection con=null;
        ResultSet rs=null;
        PreparedStatement psmt;

        //构造查询语句
        String sql="select * from patent where ";  //查询条件待定
        StringBuilder criteriaSql=new StringBuilder(512);
        criteriaSql.append(sql);
        //专利名称
        if(criteria.getPatent_name()!=null){
            criteriaSql.append("patent_no='").append(SearchCriteria.fixSqlFieldValue(criteria.getPatent_name())).
                    append("' AND ");
        }
        //学生姓名
        if (criteria.getName_student()!=null){
            criteriaSql.append("name_student='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getName_student())).append("' AND ");
        }
        //学生身份
        if (criteria.getIdentify_student()!=null){
            criteriaSql.append("identify_student='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getIdentify_student())).append("' AND ");
        }
        //专利类型
        if(criteria.getType_patent()!=null){
            criteriaSql.append("name_patent='").append(SearchCriteria.fixSqlFieldValue(criteria.getType_patent())).
                    append("' AND ");
        }
        //专利号
        if(criteria.getPatent_no()!=null){
            criteriaSql.append("name_author='").append(SearchCriteria.fixSqlFieldValue(criteria.getPatent_no())).
                    append("' AND ");
        }
        //专利发布时间
        if(criteria.getPublic_date()!=null){
            criteriaSql.append("identify_author='").append(SearchCriteria.fixSqlFieldValue(criteria.getPublic_date())).
                    append("' AND ");
        }
        //专利状态
        if(criteria.getPatent_zhuangtai()!=null){
            criteriaSql.append("name_publication='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getPatent_zhuangtai())).append("' AND ");
        }
        //贡献度排名
        if(criteria.getContribution()!=0){
            criteriaSql.append("state_patent='").append(SearchCriteria.fixSqlFieldValue
                    (String.valueOf(criteria.getContribution()))).append("' AND ");
        }
        //佐证材料
        if(criteria.getZuozheng()!=null){
            criteriaSql.append("date_publication='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getZuozheng())).append("' AND ");
        }
        //导师审核
        if(criteria.getTutor_exam()!=null){
            criteriaSql.append("tutor_exam='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getTutor_exam())).append("' AND ");
        }
        //研究生培养管理人员
        if(criteria.getManager_exam()!=null){
            criteriaSql.append("manager_exam='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getManager_exam())).append("' AND ");
        }

        sql= criteria.removeRedundancy(criteriaSql);
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                Patent patent=new Patent();
                patent.setPatent_name(rs.getString("patent_name"));
                patent.setType_patent(rs.getString("type_patent"));
                patent.setPatent_no(rs.getString("patent_no"));
                patent.setPublic_date(rs.getString("public_date"));
                patent.setPatent_zhuangtai(rs.getString("patent_zhuangtai"));
                patent.setContribution(rs.getInt("contribution"));
                patent.setZuozheng(rs.getString("zuozheng"));
                patent.setTutor_exam(rs.getString("tutor_exam"));
                patent.setManager_exam(rs.getString("manager_exam"));
                patents.add(patent);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return patents;
    }

    /*
     * 获取数据表中所有编号
     * 编号是主键，不允许重复
     * 在添加新数据时先对比主键值是否重复
     * */
    @Override
    public List<String> findAllNumber() {
        DbConnection dbConnection = new DbConnection();
        List<String> numbers=new ArrayList<>();
        Connection con=null;
        ResultSet rs;
        PreparedStatement psmt;

        //构造查询语句
        String sql="select * from patent";
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                String patent_no=rs.getString("patent_no");
                numbers.add(patent_no);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return numbers;
    }
}
