package edu.dao.patent;

import edu.searchcriteria.PatentSearchCriteria;
import edu.vo.Patent;

import java.util.List;

public interface PatentDAO {
    void addPatent(Patent patent);
    void updatePatent(Patent patent);
    void deletePatent(String patent_no);
    Patent getPatent(String patent_no);
    List<Patent> findPatents(PatentSearchCriteria searchCriteria);
    List<String> findAllNumber();
}
