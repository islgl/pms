package edu.dao.textbook;

import edu.searchcriteria.TextbookSearchCriteria;
import edu.vo.Textbook;

import java.util.List;

public interface TextbookDAO {
    void addTextbook(Textbook textbook);
    void updateTextbook(Textbook textbook, String textbook_no);
    void deleteTextbook(String book_no);
    Textbook getTextbook(String book_no);
    List<Textbook> findTextbooks(TextbookSearchCriteria searchCriteria);
    List<String> findAllNumber();
}
