package edu.dao.textbook;

import edu.db.DbConnection;
import edu.searchcriteria.SearchCriteria;
import edu.searchcriteria.TextbookSearchCriteria;
import edu.vo.Textbook;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TextbookDAOImpl implements TextbookDAO {

    /*
    * 向教材数据表中添加数据
    * */
    @Override
    public void addTextbook(Textbook textbook) {
        //编号 作者姓名 作者身份 教材名称 教材出版社 出版时间 贡献度 佐证材料 导师初审结果 管理人员终审结果
        String sql="insert into textbook(textbook_no, name_author, identify_author, name_textbook, publish_house, date_publish, " +
                "contribution, material, tutor_exam, manager_exam) values(?,?,?,?,?,?,?,?,?,?)";
        Connection con=null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();
        try {
            con=dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, textbook.getTextbook_no());            //编号
            psmt.setString(2, textbook.getName_author());            //作者姓名
            psmt.setString(3, textbook.getIdentify_author());        //作者身份
            psmt.setString(4, textbook.getName_textbook());          //教材姓名
            psmt.setString(5, textbook.getPublish_house());          //教材出版社
            psmt.setString(6, textbook.getDate_publish());           //出版时间
            psmt.setInt(7, textbook.getContribution_textbook());     //贡献度
            psmt.setString(8, textbook.getSupport_materials());      //佐证材料
            psmt.setString(9, textbook.getTutor_exam());             //导师初审结果
            psmt.setString(10, textbook.getManager_exam());          //管理人员终审结果
            psmt.executeUpdate();
            psmt.close();
            System.out.println("向textbook表中添加数据成功！");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 更新教材表中的数据
    * */
    @Override
    public void updateTextbook(Textbook textbook, String textbook_no) {
        String sql="update textbook set tutor_exam=?,manager_exam=? where textbook_no=?";
        Connection con=null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();
        try {
            con=dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, textbook.getTutor_exam());     //导师初审结果
            psmt.setString(2, textbook.getManager_exam());   //管理人员终审结果
            psmt.setString(3, textbook_no);                  //编号
            psmt.executeUpdate();
            psmt.close();
//            System.out.println("更新教材数据成功！！");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据编号删除教材表中的信息
    * */
    @Override
    public void deleteTextbook(String textbook_no) {
        String sql="delete from textbook where textbook_no=?";
        Connection con = null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();
        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, textbook_no);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("删除textbook表中数据成功！！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据编号查询指定数据
    * */
    @Override
    public Textbook getTextbook(String textbook_no) {
        String sql="select * from textbook where textbook_no=?";
        Connection con=null;
        PreparedStatement psmt;
        ResultSet rs;
        Textbook textbook=new Textbook();
        DbConnection dbConnection=new DbConnection();
        try {
            con=dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, textbook_no);
            rs = psmt.executeQuery();
            while (rs.next()) {
                //textbook_no, name_author, identify_author, name_book, publish_house, date_publish,
                // contribution,support_materials, tutor_exam, manager_exam
                String book_no = rs.getString("textbook_no");            //编号
                String name_a = rs.getString("name_author");              //作者姓名
                String id_a = rs.getString("identify_author");            //作者身份
                String name_b = rs.getString("name_textbook");            //教材名称
                String publish = rs.getString("publish_house");           //教材出版社
                String date = rs.getString("date_publish");              //出版时间
                int contribution = rs.getInt("contribution");    //贡献度
                String support = rs.getString("material");       //佐证材料
                String tutor = rs.getString("tutor_exam");                //导师审核结果
                String manager = rs.getString("manager_exam");            //管理人员审核结果

                textbook.setTextbook_no(book_no);                   //编号
                textbook.setName_author(name_a);                    //作者姓名
                textbook.setIdentify_author(id_a);                  //作者身份
                textbook.setName_textbook(name_b);                  //教材名称
                textbook.setPublish_house(publish);                 //教材出版社
                textbook.setDate_publish(date);                     //出版时间
                textbook.setContribution_textbook(contribution);    //贡献度
                textbook.setSupport_materials(support);             //佐证材料
                textbook.setTutor_exam(tutor);                      //导师初审结果
                textbook.setManager_exam(manager);                  //管理人员终审结果

                System.out.println("编号:" + book_no + " 作者姓名:" + name_a + " 作者身份:" + id_a + " 教材名称:" + name_b + " 教材出版社:" + publish +
                        " 教材出版时间:" + date + " 贡献度:" + contribution + " 佐证材料:" + support + " 导师审核:" + tutor + " 研究生培养管理人员审核:" + manager);
            }
            rs.close();
            psmt.close();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return textbook;
    }

    /*
    * 条件查询
    * */
    @Override
    public List<Textbook> findTextbooks(TextbookSearchCriteria criteria) {
        DbConnection dbConnection=new DbConnection();
        List<Textbook> books=new ArrayList<>();
        Connection con=null;
        PreparedStatement psmt;
        ResultSet rs;

        //构建查询语句
        String sql="select * from textbook where ";
        StringBuilder criteriaSql=new StringBuilder(512);
        criteriaSql.append(sql);
        if (criteria.getTextbook_no()!=null){//编号
            criteriaSql.append("textbook_no='").append(SearchCriteria.fixSqlFieldValue(criteria.getTextbook_no())).append("' AND ");
        }
        if (criteria.getName_author()!=null){//作者姓名
            criteriaSql.append("name_author='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_author())).append("' AND ");
        }
        if (criteria.getIdentify_author()!=null){//作者身份
            criteriaSql.append("identify_author='").append(SearchCriteria.fixSqlFieldValue(criteria.getIdentify_author())).append("' AND ");
        }
        if (criteria.getName_textbook()!=null){//教材名称
            criteriaSql.append("name_textbook='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_textbook())).append("' AND ");
        }
        if (criteria.getPublish_house()!=null){//教材出版社
            criteriaSql.append("publish_house='").append(SearchCriteria.fixSqlFieldValue(criteria.getPublish_house())).append("' AND ");
        }
        if (criteria.getDate_publish()!=null){//出版时间
            criteriaSql.append("date_publish='").append(SearchCriteria.fixSqlFieldValue(criteria.getDate_publish())).append("' AND ");
        }
        if (criteria.getContribution_textbook()!=0){//贡献度
            criteriaSql.append("contribution=").append(SearchCriteria.fixSqlFieldValue(String.valueOf(criteria.getContribution_textbook()))).append(" AND ");
        }
        if (criteria.getSupport_materials()!=null){//佐证材料
            criteriaSql.append("material='").append(SearchCriteria.fixSqlFieldValue(criteria.getSupport_materials())).append("' AND ");
        }
        if (criteria.getTutor_exam()!=null){//导师初审结果
            criteriaSql.append("tutor_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getTutor_exam())).append("' AND ");
        }
        if (criteria.getManager_exam()!=null){//管理人员终审结果
            criteriaSql.append("manager_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getManager_exam())).append("' AND ");
        }

        //规范化查询语句
        sql=criteria.removeRedundancy(criteriaSql);
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                //textbook_no, name_author, identify_author, name_book, publish_house, date_publish,
                // contribution,material, tutor_exam, manager_exam
                Textbook book=new Textbook();
                book.setTextbook_no(rs.getString("textbook_no"));           //编号
                book.setName_author(rs.getString("name_author"));           //作者姓名
                book.setIdentify_author(rs.getString("identify_author"));   //作者身份
                book.setName_textbook(rs.getString("name_textbook"));       //教材名称
                book.setPublish_house(rs.getString("publish_house"));       //出版社名称
                book.setDate_publish(rs.getString("date_publish"));         //出版时间
                book.setContribution_textbook(rs.getInt("contribution"));   //贡献度
                book.setSupport_materials(rs.getString("material"));//佐证材料
                book.setTutor_exam(rs.getString("tutor_exam"));             //导师初审
                book.setManager_exam(rs.getString("manager_exam"));         //管理人员终审
                books.add(book);
            }
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return books;
    }

    /*
    * 获取教材表中所有的编号（主键值）
    * */
    @Override
    public List<String> findAllNumber() {
        String sql="select * from textbook";
        DbConnection dbConnection=new DbConnection();
        List<String> num=new ArrayList<>();
        Connection con=null;
        PreparedStatement psmt;
        ResultSet rs;
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                String book_no=rs.getString("textbook_no");
                num.add(book_no);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return num;
    }
}
