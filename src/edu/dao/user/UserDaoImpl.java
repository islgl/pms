package edu.dao.user;

import edu.db.DbConnection;
import edu.vo.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户登录注册模块DAO实现类
 */
public class UserDaoImpl implements UserDao {

    /**
     * 添加用户信息
     *
     * @param user 待添加的用户信息
     */
    @Override
    public void add(User user) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            String sql = "INSERT INTO users VALUES(?,?,?);";
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getRoleCode());

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除用户信息
     *
     * @param username 待删除的用户信息
     */
    @Override
    public void delete(String username) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            String sql = "DELETE FROM users WHERE username=?;";
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新用户信息
     *
     * @param username 需要更新的用户的用户名
     * @param user     更新后的用户信息
     */
    @Override
    public void update(String username, User user) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            String sql = "UPDATE users SET username=?,password=?,roleCode=? WHERE username=?;";
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getRoleCode());
            statement.setString(4, username);

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取指定用户名的用户信息
     *
     * @param username 需要获取的用户的用户名
     * @return 指定用户名对应的User对象
     */
    @Override
    public User get(String username) {
        User user = null;
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;


        try {
            String sql = "SELECT * FROM users WHERE username=?;";
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);

            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                user = new User(result.getString("username"), result.getString("password"),
                        result.getString("roleCode"));
            }

            result.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    /**
     * 查询拥有指定角色权限的用户信息
     *
     * @param roleCode 需要查询的用户角色权限
     * @return 拥有指定角色权限的所有用户列表
     */
    @Override
    public List<User> find(String roleCode) {
        List<User> users = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;

        try {
            String sql = "SELECT * FROM users WHERE roleCode=?";
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, roleCode);

            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                User user = new User(result.getString("username"), result.getString("password"),
                        result.getString("roleCode"));
                users.add(user);
            }

            result.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
}
