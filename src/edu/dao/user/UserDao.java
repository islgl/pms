package edu.dao.user;

import edu.vo.User;

import java.util.List;

/**
 * 用户登录注册模块DAO接口
 */
public interface UserDao {
    /**
     * 添加用户信息
     * @param user 待添加的用户信息
     */
    void add(User user);

    /**
     * 删除用户信息
     * @param username 待删除的用户信息
     */
    void delete(String username);

    /**
     * 更新用户信息
     * @param username 需要更新的用户的用户名
     * @param user 更新后的用户信息
     */
    void update(String username,User user);

    /**
     * 获取指定用户名的用户信息
     * @param username 需要获取的用户的用户名
     * @return 指定用户名对应的User对象
     */
    User get(String username);

    /**
     * 查询拥有指定角色权限的用户信息
     * @param roleCode 需要查询的用户角色权限
     * @return 拥有指定角色权限的所有用户列表
     */
    List<User> find(String roleCode);

}
