package edu.dao.postgraduate;

import edu.db.DbConnection;
import edu.searchcriteria.PostgraduateSearchCriteria;
import edu.searchcriteria.SearchCriteria;
import edu.vo.Course;
import edu.vo.Postgraduate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * PostgraduateDao接口实现类，用于实现对研究生表的CRUD操作
 */
public class PostgraduateDaoImpl implements PostgraduateDao {
    /**
     * 增加研究生信息
     *
     * @param postgraduate 要增加的研究生信息
     */
    @Override
    public void add(Postgraduate postgraduate) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = dbConnection.getConnection();
        PreparedStatement statement = null;

        try {
            String sql = "INSERT INTO postgraduate VALUES(?,?,?,?,?,?,?,?);";
            statement = connection.prepareStatement(sql);
            statement.setString(1, postgraduate.getId());
            statement.setString(2, postgraduate.getName());
            statement.setString(3, postgraduate.getMajor());
            statement.setString(4, postgraduate.getTutorid());
            statement.setString(5, postgraduate.getApplications()[0]);
            statement.setString(6, postgraduate.getApplications()[1]);
            statement.setInt(7, postgraduate.getType());
            statement.setInt(8, postgraduate.getAllot());

            statement.executeUpdate();

            statement.close();
            dbConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除研究生信息
     *
     * @param postgraduateId 需要删除的研究生学号
     */
    @Override
    public void delete(String postgraduateId) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = dbConnection.getConnection();
        PreparedStatement statement = null;

        try {
            String sql = "DELETE FROM postgraduate WHERE id=?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1, postgraduateId);

            statement.executeUpdate();

            statement.close();
            dbConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新研究生信息
     *
     * @param postgraduateId 需要更新的研究生学号
     * @param postgraduate   更新后的研究生信息
     */
    @Override
    public void update(String postgraduateId, Postgraduate postgraduate) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = dbConnection.getConnection();
        PreparedStatement statement = null;

        try {
            String sql = "UPDATE postgraduate SET id=?,name=?,major=?,tutorid=?,application1=?,application2=?,type=?,allot=? WHERE id=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, postgraduate.getId());
            statement.setString(2, postgraduate.getName());
            statement.setString(3, postgraduate.getMajor());
            statement.setString(4, postgraduate.getTutorid());
            statement.setString(5, postgraduate.getApplications()[0]);
            statement.setString(6, postgraduate.getApplications()[1]);
            statement.setInt(7, postgraduate.getType());
            statement.setInt(8, postgraduate.getAllot());
            statement.setString(9, postgraduateId);

            statement.executeUpdate();

            statement.close();
            dbConnection.close(connection);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取研究生信息
     *
     * @param postgraduateId 需要获取的研究生的学号
     * @return 获取的研究生信息
     */
    @Override
    public Postgraduate get(String postgraduateId) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = dbConnection.getConnection();
        PreparedStatement statement = null;
        ResultSet result = null;
        Postgraduate postgraduate = null;

        try {
            String sql = "SELECT * FROM postgraduate WHERE id=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, postgraduateId);

            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                postgraduate = new Postgraduate(result.getString("id"),
                        result.getString("name"),
                        result.getString("major"),
                        result.getString("tutorid"),
                        new String[]{result.getString("application1"), result.getString("application2")},
                        result.getInt("type"),
                        result.getInt("allot"));
            }

            result.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return postgraduate;
    }

    /**
     * 查询符合指定条件的研究生信息
     *
     * @param criteria 查询条件
     * @return 符合条件的研究生信息列表
     */
    @Override
    public List<Postgraduate> find_AND(PostgraduateSearchCriteria criteria) {
        List<Postgraduate> postgraduates = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection connection = dbConnection.getConnection();
        PreparedStatement statement = null;
        ResultSet result = null;

        StringBuilder criteriaSql = new StringBuilder(512);
        String sql = "SELECT * FROM postgraduate WHERE ";
        criteriaSql.append(sql);

        //构造查询语句
        if (criteria.getId() != null) {
            criteriaSql.append("id='" + SearchCriteria.fixSqlFieldValue(criteria.getId()) + "' AND ");
        }
        if (criteria.getName() != null) {
            criteriaSql.append("name='" + SearchCriteria.fixSqlFieldValue(criteria.getName()) + "' AND ");
        }
        if (criteria.getMajor() != null) {
            criteriaSql.append("major='" + SearchCriteria.fixSqlFieldValue(criteria.getMajor()) + "' AND ");
        }
        if (criteria.getTutorid() != null) {
            criteriaSql.append("tutorid='" + SearchCriteria.fixSqlFieldValue(criteria.getTutorid()) + "' AND ");
        }
        if (criteria.getApplication1() != null) {
            criteriaSql.append("application1='" + SearchCriteria.fixSqlFieldValue(criteria.getApplication1()) + "' AND ");
        }
        if (criteria.getApplication2() != null) {
            criteriaSql.append("application2='" + SearchCriteria.fixSqlFieldValue(criteria.getApplication2()) + "' AND ");
        }
        if (criteria.getType() != -1) {
            criteriaSql.append("type=" + SearchCriteria.fixSqlFieldValue(String.valueOf(criteria.getType())) + "' AND ");
        }
        if (criteria.getAllot() != -1) {
            criteriaSql.append("allot=" + SearchCriteria.fixSqlFieldValue(String.valueOf(criteria.getAllot())) + "' AND ");
        }

        // 去除多余的WHERE和AND关键字
        sql = criteria.removeRedundancy(criteriaSql);

        try {
            statement = connection.prepareStatement(sql);
            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                Postgraduate postgraduate = new Postgraduate(result.getString("id"),
                        result.getString("name"),
                        result.getString("major"),
                        result.getString("tutorid"),
                        new String[]{result.getString("application1"), result.getString("application2")},
                        result.getInt("type"),
                        result.getInt("allot"));
                postgraduates.add(postgraduate);
            }

            result.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return postgraduates;
    }

    @Override
    public List<Postgraduate> find_OR(PostgraduateSearchCriteria criteria) {
        List<Postgraduate> postgraduates = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection connection = dbConnection.getConnection();
        PreparedStatement statement = null;
        ResultSet result = null;

        StringBuilder criteriaSql = new StringBuilder(512);
        String sql = "SELECT * FROM postgraduate WHERE ";
        criteriaSql.append(sql);

        //构造查询语句
        if (criteria.getId() != null) {
            criteriaSql.append("id='" + SearchCriteria.fixSqlFieldValue(criteria.getId()) + "' OR ");
        }
        if (criteria.getName() != null) {
            criteriaSql.append("name='" + SearchCriteria.fixSqlFieldValue(criteria.getName()) + "' OR ");
        }
        if (criteria.getMajor() != null) {
            criteriaSql.append("major='" + SearchCriteria.fixSqlFieldValue(criteria.getMajor()) + "' OR ");
        }
        if (criteria.getTutorid() != null) {
            criteriaSql.append("tutorid='" + SearchCriteria.fixSqlFieldValue(criteria.getTutorid()) + "' AND ");
        }
        if (criteria.getApplication1() != null) {
            criteriaSql.append("application1='" + SearchCriteria.fixSqlFieldValue(criteria.getApplication1()) + "' OR ");
        }
        if (criteria.getApplication2() != null) {
            criteriaSql.append("application2='" + SearchCriteria.fixSqlFieldValue(criteria.getApplication2()) + "' OR ");
        }
        if (criteria.getType() != -1) {
            criteriaSql.append("type=" + SearchCriteria.fixSqlFieldValue(String.valueOf(criteria.getType())) + "' OR ");
        }
        if (criteria.getAllot() != -1) {
            criteriaSql.append("allot=" + SearchCriteria.fixSqlFieldValue(String.valueOf(criteria.getAllot())) + "' OR ");
        }

        // 去除多余的WHERE和OR关键字
        sql = criteria.removeOR(criteriaSql);

        try {
            statement = connection.prepareStatement(sql);
            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                Postgraduate postgraduate = new Postgraduate(result.getString("id"),
                        result.getString("name"),
                        result.getString("major"),
                        result.getString("tutorid"),
                        new String[]{result.getString("application1"), result.getString("application2")},
                        result.getInt("type"),
                        result.getInt("allot"));
                postgraduates.add(postgraduate);
            }

            result.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return postgraduates;
    }

    /**
     * 从视图中获取所有未分配助教工作且填报了特定课程志愿的学生的信息
     *
     * @param course 用于限定志愿的课程
     * @return 所有未分配助教工作且填报了course作为志愿的学生列表
     */
    public List<Postgraduate> getUnallocated(Course course) {
        List<Postgraduate> postgraduates = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;

        try {
            String courseId = course.getId();
            String sql = "SELECT * FROM noAssistantPostgraduateView WHERE application1=? OR application2=?;";
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, courseId);
            statement.setString(2, courseId);

            statement.executeQuery();
            result = statement.getResultSet();
            while (result.next()) {
                Postgraduate postgraduate = new Postgraduate(result.getString("id"),
                        result.getString("name"),
                        result.getString("major"),
                        new String[]{result.getString("application1"), result.getString("application2")},
                        result.getInt("type"),
                        0);
                postgraduates.add(postgraduate);
            }

            result.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return postgraduates;
    }

    /**
     * 从视图中获取所有未分配助教工作的学生的信息
     * @return 所有未分配助教工作的学生列表
     */
    public List<Postgraduate> getAllUnallocated(){
        List<Postgraduate> postgraduates = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;

        try {
            String sql = "SELECT * FROM noAssistantPostgraduateView;";
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);

            statement.executeQuery();
            result = statement.getResultSet();
            while (result.next()) {
                Postgraduate postgraduate = new Postgraduate(result.getString("id"),
                        result.getString("name"),
                        result.getString("major"),
                        new String[]{result.getString("application1"), result.getString("application2")},
                        result.getInt("type"),
                        0);
                postgraduates.add(postgraduate);
            }

            result.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return postgraduates;

    }
}
