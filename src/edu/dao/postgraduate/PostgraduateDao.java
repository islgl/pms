package edu.dao.postgraduate;

import edu.searchcriteria.PostgraduateSearchCriteria;
import edu.vo.Postgraduate;

import java.util.List;

/**
 * @author: Liu Guoli lewisliugl@outlook.com
 * Date: 2022/11/29
 * Copyright: Copyright ©️ 2022 lewisliugl@outlook.com.All rights reserved.
 */
public interface PostgraduateDao {
    /**
     * 增加研究生信息
     *
     * @param postgraduate 要增加的研究生信息
     */
    void add(Postgraduate postgraduate);

    /**
     * 删除研究生信息
     *
     * @param postgraduateId 需要删除的研究生学号
     */
    void delete(String postgraduateId);

    /**
     * 更新研究生信息
     *
     * @param postgraduateId 需要更新的研究生学号
     * @param postgraduate   更新后的研究生信息
     */
    void update(String postgraduateId, Postgraduate postgraduate);

    /**
     * 获取研究生信息
     *
     * @param postgraduateId 需要获取的研究生的学号
     * @return 该学号对应的研究生信息
     */
    Postgraduate get(String postgraduateId);

    /**
     * 查询符合指定条件的研究生信息(AND)
     *
     * @param criteria 查询条件
     * @return 符合指定条件的研究生信息列表
     */
    List<Postgraduate> find_AND(PostgraduateSearchCriteria criteria);

    /**
     * 查询符合指定条件的研究生信息(OR)
     *
     * @param criteria 查询条件
     * @return 符合指定条件的研究生信息列表
     */
    List<Postgraduate> find_OR(PostgraduateSearchCriteria criteria);
}
