package edu.dao.administrator;

import edu.db.DbConnection;
import edu.searchcriteria.AdSearchCriteria;
import edu.searchcriteria.SearchCriteria;
import edu.vo.Administrator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdministratorImpl implements AdministratorDAO {
    private static final String ADMINISTRATOR_INSERT_SQL = "INSERT INTO administrator(id,name) VALUES(?,?)";

    @Override
    public void addAdministrator(Administrator administrator) {
        DbConnection dbConnection = new DbConnection();
        Connection con = dbConnection.getConnection();
        try {
            PreparedStatement psmt = con.prepareStatement(ADMINISTRATOR_INSERT_SQL);
            psmt.setString(1, administrator.getAdminid());
            psmt.setString(2, administrator.getAdminame());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("添加成功！！！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static final String ADMINISTRATOR_DELETE_SQL = "DELETE FROM administrator WHERE id=?";

    @Override
    public void deleteAdministrator(String ad_id) {
        DbConnection dbConnection = new DbConnection();
        Connection con = dbConnection.getConnection();
        try {
            PreparedStatement psmt = con.prepareStatement(ADMINISTRATOR_DELETE_SQL);
            psmt.setString(1, ad_id);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("删除成功！！！");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static final String ADMINISTRATOR_SELECT_SQL = "SELECT id,name FROM administrator WHERE id=?";

    @Override
    public Administrator getAdministrator(String ad_id) {
        DbConnection dbConnection = new DbConnection();
        Connection con = dbConnection.getConnection();
        Administrator administrator = new Administrator();
        try {
            PreparedStatement psmt = con.prepareStatement(ADMINISTRATOR_SELECT_SQL);
            psmt.setString(1, ad_id);
            ResultSet rs = psmt.executeQuery();
            while (rs.next()) {
                administrator.setAdminid(rs.getString(1));
                administrator.setAdminame(rs.getString(2));
                String id = rs.getString(1);
                String name = rs.getString(2);
                System.out.println(id + "," + name);
            }
            psmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return administrator;
    }

    @Override
    public List<Administrator> findAd(AdSearchCriteria adsearchcriteria) {
        List<Administrator> administrators = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection connection = dbConnection.getConnection();
        PreparedStatement psmt = null;
        ResultSet rs = null;

        StringBuilder criteriaSql = new StringBuilder(512);
        String sql = "SELECT * FROM administrator WHERE ";
        criteriaSql.append(sql);

        //构造查询语句
        if (adsearchcriteria.getAdminid() != null) {
            criteriaSql.append("id='" + SearchCriteria.fixSqlFieldValue(adsearchcriteria.getAdminid()) + "' AND ");
        }
        if (adsearchcriteria.getAdminame() != null) {
            criteriaSql.append("name='" + SearchCriteria.fixSqlFieldValue(adsearchcriteria.getAdminame()) + "' AND ");
        }

        // 去除多余的WHERE和AND关键字
        sql = adsearchcriteria.removeRedundancy(criteriaSql);

        try {
            psmt = connection.prepareStatement(sql);
            psmt.executeQuery();
            rs = psmt.getResultSet();

            while (rs.next()) {
                Administrator administrator = new Administrator(rs.getString("id"),
                        rs.getString("name"));
                administrators.add(administrator);
            }

            rs.close();
            psmt.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return administrators;
    }
}
