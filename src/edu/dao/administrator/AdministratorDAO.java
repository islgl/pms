package edu.dao.administrator;

import edu.searchcriteria.AdSearchCriteria;
import edu.vo.Administrator;
import java.util.List;

public interface AdministratorDAO {
    void addAdministrator(Administrator administor);
    void deleteAdministrator(String ad_id);
    Administrator getAdministrator(String ad_id);
    List<Administrator> findAd(AdSearchCriteria adsearchCriteria);
}
