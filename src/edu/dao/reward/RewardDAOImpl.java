package edu.dao.reward;


import edu.db.DbConnection;
import edu.searchcriteria.RewardSearchCriteria;
import edu.searchcriteria.SearchCriteria;
import edu.vo.Reward;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class RewardDAOImpl implements RewardDAO {
    /*
    * 向奖励表添加数据
    * */
    @Override
    public void addReward(Reward reward) {
        //奖励编号reward_no 奖励名称name_reward 获奖者姓名name_winner 奖励等级（国家级、省部级、市级、其他）level_reward、
        //获奖等级（特等奖、一等奖、二等奖、三等奖）level_award、排名ranking、获奖时间date_award、佐证材料support_materials
        //导师初审结果tutor_exam、管理人员终审结果manager_exam
        String sql="insert into reward(reward_no,name_reward,name_winner,level_reward,level_award,ranking,date_award," +
                "support_materials,tutor_exam,manager_exam) values(?,?,?,?,?,?,?,?,?,?)";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,reward.getReward_no());        //奖励编号（主键）
            psmt.setString(2,reward.getName_reward());      //奖励名称
            psmt.setString(3,reward.getName_winner());      //获奖者姓名
            psmt.setString(4,reward.getLevel_reward());     //奖励等级
            psmt.setString(5,reward.getLevel_award());      //获奖等级
            psmt.setInt(6,reward.getRanking());             //排名
            psmt.setString(7,reward.getDate_award());       //获奖日期
            psmt.setString(8,reward.getSupport_materials());//佐证材料
            psmt.setString(9,reward.getTutor_exam());       //导师初审结果
            psmt.setString(10,reward.getManager_exam());    //管理人员终审结果
            psmt.executeUpdate();
            psmt.close();
            System.out.println("向reward表中添加数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 更新奖励表的数据信息（主要是审核结果的更新）
    * */
    @Override
    public void updateReward(Reward reward, String reward_no) {
        //更新奖励表里的数据，主要更新导师和管理人员的审核结果
        String sql="update reward set tutor_exam=?,manager_exam=? where reward_no=?";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,reward.getTutor_exam());       //导师初审结果
            psmt.setString(2,reward.getManager_exam());     //管理人员终审结果
            psmt.setString(3,reward_no);                    //编号（主键）
            psmt.executeUpdate();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据主键奖励编号删除奖励表中的数据
    * */
    @Override
    public void deleteReward(String reward_no) {
        //根据编号（主键）删除数据表记录
        String sql = "delete from reward where reward_no=?";
        DbConnection dbConnection=new DbConnection();
        Connection con = null;
        PreparedStatement psmt;
        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, reward_no);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("删除reward表中数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据奖励编号查询指定数据信息
    * */
    @Override
    public Reward getReward(String reward_no) {
        //根据编号（主键）查找指定数据信息（只有一条数据）
        String sql="select * from reward where reward_no=?";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        Reward reward=new Reward();
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,reward_no);
            ResultSet rs=psmt.executeQuery();
            while (rs.next()){
                String rno= rs.getString("reward_no");          //奖励编号
                String rname=rs.getString("name_reward");       //奖励名称
                String wname=rs.getString("name_winner");       //获奖者姓名
                String lr=rs.getString("level_reward");         //奖励等级
                String la= rs.getString("level_award");         //获奖等级
                int rank=rs.getInt("ranking");                  //排名
                String date=rs.getString("date_award");         //获奖时间
                String ma=rs.getString("support_materials");    //佐证材料
                String te=rs.getString("tutor_exam");           //导师审核结果
                String me=rs.getString("manager_exam");         //管理人员审核结果

                reward.setReward_no(rno);           //奖励编号
                reward.setName_reward(rname);       //奖励名称
                reward.setName_winner(wname);       //获奖者姓名
                reward.setLevel_reward(lr);         //奖励等级
                reward.setLevel_award(la);          //获奖等级
                reward.setRanking(rank);            //排名
                reward.setDate_award(date);         //获奖时间
                reward.setSupport_materials(ma);    //佐证材料
                reward.setTutor_exam(te);           //导师初审结果
                reward.setManager_exam(me);         //管理人员终审结果

                System.out.println("编号:"+rno+" 奖励名称:"+rname+" 获奖者姓名:"+wname+" 奖励等级:"+lr+" 获奖等级:"+la+
                        " 排名:"+rank+" 获奖时间:"+date+" 佐证材料:"+ma+" 导师审核:"+te+" 研究生培养管理人员审核:"+me);
            }
            rs.close();
            psmt.close();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return reward;
    }

    /*
    * 条件查找
    * */
    @Override
    public List<Reward> findRewards(RewardSearchCriteria criteria) {
        List<Reward> rewards=new ArrayList<>();
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        ResultSet rs=null;

        //构造查询语句
        String sql="select * from reward where ";
        StringBuilder criteriaSql=new StringBuilder(512);
        criteriaSql.append(sql);
        if (criteria.getReward_no()!=null){//奖励编号
            criteriaSql.append("reward_no='").append(SearchCriteria.fixSqlFieldValue(criteria.getReward_no())).append("' AND ");
        }
        if (criteria.getName_reward()!=null){//奖励名称
            criteriaSql.append("name_reward='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_reward())).append("' AND ");
        }
        if (criteria.getName_winner()!=null){//获奖者姓名
            criteriaSql.append("name_reward='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_winner())).append("' AND ");
        }
        if (criteria.getLevel_reward()!=null){//奖励等级
            criteriaSql.append("level_reward='").append(SearchCriteria.fixSqlFieldValue(criteria.getLevel_reward())).append("' AND ");
        }
        if (criteria.getLevel_award()!=null){//获奖等级
            criteriaSql.append("level_award'").append(SearchCriteria.fixSqlFieldValue(criteria.getLevel_award())).append("' AND ");
        }
        if (criteria.getRanking()!=0){//排名
            criteriaSql.append("ranking=").append(SearchCriteria.fixSqlFieldValue(String.valueOf(criteria.getRanking()))).append(" AND ");
        }
        if (criteria.getDate_award()!=null){//获奖时间
            criteriaSql.append("date_award='").append(SearchCriteria.fixSqlFieldValue(criteria.getDate_award())).append("' AND ");
        }
        if (criteria.getSupport_materials()!=null){//佐证材料
            criteriaSql.append("support_materials='").append(SearchCriteria.fixSqlFieldValue(criteria.getSupport_materials())).append("' AND ");
        }
        if (criteria.getTutor_exam()!=null){//导师初审结果
            criteriaSql.append("tutor_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getTutor_exam())).append("' AND ");
        }if (criteria.getManager_exam()!=null){//研究生管理人员终审结果
            criteriaSql.append("manager_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getManager_exam())).append("' AND ");
        }

        //规范化查询语句
        sql= criteria.removeRedundancy(criteriaSql);

        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                Reward reward=new Reward();
                reward.setReward_no(rs.getString("reward_no"));         //编号
                reward.setName_reward(rs.getString("name_reward"));     //奖励名称
                reward.setName_winner(rs.getString("name_winner"));     //获奖者姓名
                reward.setLevel_reward(rs.getString("level_reward"));   //奖励等级
                reward.setLevel_award(rs.getString("level_award"));     //获奖等级
                reward.setRanking(rs.getInt("ranking"));                //排名
                reward.setDate_award(rs.getString("date_award"));       //获奖时间
                reward.setSupport_materials(rs.getString("support_materials"));//佐证材料
                reward.setTutor_exam(rs.getString("tutor_exam"));       //导师初审结果
                reward.setManager_exam(rs.getString("manager_exam"));   //研究生管理人员终审结果
                rewards.add(reward);
            }
            rs.close();
            psmt.close();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return rewards;
    }

    /*
    * 获取所有记录的主键（编号）
    * */
    @Override
    public List<String> findAllNumber() {
        //查询表中所有数据
        String sql="select * from reward";
        DbConnection dbConnection=new DbConnection();
        List<String> numbers=new ArrayList<>();
        Connection con=null;
        PreparedStatement psmt;
        ResultSet rs;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                String no=rs.getString("reward_no");  //获取编号
                numbers.add(no);   //追加到列表numbers中
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return numbers;
    }
}
