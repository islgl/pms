package edu.dao.reward;

import edu.searchcriteria.RewardSearchCriteria;
import edu.vo.Reward;

import java.util.List;

public interface RewardDAO {
    void addReward(Reward reward);
    void updateReward(Reward reward,String reward_no);
    void deleteReward(String reward_no);
    Reward getReward(String reward_no);
    List<Reward> findRewards(RewardSearchCriteria searchCriteria);
    List<String> findAllNumber();
}
