package edu.dao.standards;

import edu.db.DbConnection;
import edu.searchcriteria.SearchCriteria;
import edu.searchcriteria.StandardSearchCriteria;
import edu.vo.Standards;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class StandardDAOImpl implements StandardDAO {
    /*
    * 向数据表中添加新数据
    * */
    @Override
    public void addStandard(Standards standard) {
        //编号 申请人姓名 申请人身份 标准名称、标准级别（国家标准、省部级地方标准、行业标准、团队标准）、标准发布时间、佐证材料 导师初审 管理员终审
        String sql="insert into standards(standard_no,name_person,identify_person,name_standard,level_standard," +
                "date_release,support_materials,tutor_exam,manager_exam) values(?,?,?,?,?,?,?,?,?)";
        Connection con=null;
        DbConnection dbConnection=new DbConnection();
        try {
            con=dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(sql);
            psmt.setString(1, standard.getStandard_no());        //编号
            psmt.setString(2, standard.getName_person());        //申请人姓名
            psmt.setString(3, standard.getIdentify_person());    //申请人身份
            psmt.setString(4, standard.getName_standard());      //标准名称
            psmt.setString(5, standard.getLevel_standard());     //标准级别
            psmt.setString(6, standard.getDate_release());       //发布时间
            psmt.setString(7, standard.getSupport_materials());  //佐证材料
            psmt.setString(8, standard.getTutor_exam());         //导师初审
            psmt.setString(9, standard.getManager_exam());       //管理人员终审
            psmt.executeUpdate();
            psmt.close();
            System.out.println("向standards表中添加数据成功！");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 更新数据表中某些字段的值
    * */
    @Override
    public void updateStandard(Standards standard, String standard_no) {
        String sql="update standards set tutor_exam=?,manager_exam=? where standard_no=?";
        Connection con=null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();
        try {
            con=dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, standard.getTutor_exam());     //导师初审
            psmt.setString(2, standard.getManager_exam());   //管理人员终审
            psmt.setString(3, standard_no);
            psmt.executeUpdate();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据编号删除数据表中的指定记录
    * */
    @Override
    public void deleteStandard(String standard_no) {
        String sql="delete from standards where standard_no=?";
        Connection con=null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();
        try {
            con=dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1,standard_no);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("删除standards表中数据成功！");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据编号查询数据表中指定记录
    * */
    @Override
    public Standards getStandard(String standard_no) {
        String sql="select * from standards where standard_no=?";
        Connection con=null;
        PreparedStatement psmt;
        Standards sdd=new Standards();
        DbConnection dbConnection=new DbConnection();
        try {
            con=dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1,standard_no);
            ResultSet rs=psmt.executeQuery();
            while (rs.next()){
                sdd.setStandard_no(rs.getString("standard_no"));            //编号
                sdd.setName_person(rs.getString("name_person"));            //申请人姓名
                sdd.setIdentify_person(rs.getString("identify_person"));    //申请人身份
                sdd.setName_standard(rs.getString("name_standard"));        //标准名称
                sdd.setLevel_standard(rs.getString("level_standard"));      //标准等级
                sdd.setDate_release(rs.getString("date_release"));          //发布时间
                sdd.setSupport_materials(rs.getString("support_materials"));//佐证材料
                sdd.setTutor_exam(rs.getString("tutor_exam"));              //导师初审
                sdd.setManager_exam(rs.getString("manager_exam"));          //管理员终审

                String no=rs.getString("standard_no");
                String name_p=rs.getString("name_person");
                String id_p=rs.getString("identify_person");
                String name_s=rs.getString("name_standard");
                String level=rs.getString("level_standard");
                String date=rs.getString("date_release");
                String material=rs.getString("support_materials");
                String tutor=rs.getString("tutor_exam");
                String manager=rs.getString("manager_exam");
                System.out.println("编号:"+no+" 研究生姓名:"+name_p+" 研究生身份:"+id_p+" 标准名称:"+name_s+" 标准级别:"+level+
                        " 标准发布日期:"+date+" 佐证材料:"+material+" 导师初审:"+tutor+" 管理人员终审:"+manager);
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return sdd;
    }

    /*
    * 条件查询
    * */
    @Override
    public List<Standards> findStandards(StandardSearchCriteria criteria) {
        String sql="select * from standards where ";
        List<Standards> standards=new ArrayList<>();
        Connection con=null;
        PreparedStatement psmt;
        ResultSet rs;
        DbConnection dbConnection=new DbConnection();

        //构造查询语句
        StringBuilder criteriaSql=new StringBuilder(512);
        criteriaSql.append(sql);
        if (criteria.getStandard_no()!=null){//编号
            criteriaSql.append("standard_no='").append(SearchCriteria.fixSqlFieldValue(criteria.getStandard_no())).append("' and ");
        }
        if (criteria.getName_person()!=null){//研究生姓名
            criteriaSql.append("name_person='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_person())).append("' and ");
        }
        if (criteria.getIdentify_person()!=null){//研究生身份
            criteriaSql.append("identify_person='").append(SearchCriteria.fixSqlFieldValue(criteria.getIdentify_person())).append("' and ");
        }
        if (criteria.getName_standard()!=null){//标准名称
            criteriaSql.append("name_standard='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_standard())).append("' and ");
        }
        if (criteria.getLevel_standard()!=null){//标准级别
            criteriaSql.append("level_standard='").append(SearchCriteria.fixSqlFieldValue(criteria.getLevel_standard())).append("' and ");
        }
        if (criteria.getDate_release()!=null){//标准发布时间
            criteriaSql.append("date_release='").append(SearchCriteria.fixSqlFieldValue(criteria.getDate_release())).append("' and ");
        }
        if (criteria.getSupport_materials()!=null){//佐证材料
            criteriaSql.append("support_materials='").append(SearchCriteria.fixSqlFieldValue(criteria.getStandard_no())).append("' and ");
        }
        if (criteria.getTutor_exam()!=null){//导师初审
            criteriaSql.append("tutor_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getTutor_exam())).append("' and ");
        }
        if (criteria.getManager_exam()!=null){//管理人员终审
            criteriaSql.append("manager_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getManager_exam())).append("' and ");
        }
        //规范化查询语句
        sql=criteria.removeRedundancy(criteriaSql);
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                Standards sdd=new Standards();
                sdd.setStandard_no(rs.getString("standard_no"));            //编号
                sdd.setName_person(rs.getString("name_person"));            //申请人姓名
                sdd.setIdentify_person(rs.getString("identify_person"));    //申请人身份
                sdd.setName_standard(rs.getString("name_standard"));        //标准名称
                sdd.setLevel_standard(rs.getString("level_standard"));      //标准等级
                sdd.setDate_release(rs.getString("date_release"));          //发布时间
                sdd.setSupport_materials(rs.getString("support_materials"));//佐证材料
                sdd.setTutor_exam(rs.getString("tutor_exam"));              //导师初审
                sdd.setManager_exam(rs.getString("manager_exam"));          //管理员终审

                standards.add(sdd);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return standards;
    }

    /*
    * 查询表中所有数据的编号（主键）值
    * */
    @Override
    public List<String> findAllNumber() {
        //查询数据表中所有数据的sql语句
        String sql="select * from standards";
        List<String> number = new ArrayList<>();
        Connection con=null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            ResultSet rs=psmt.executeQuery();
            while (rs.next()){
                String no=rs.getString("standard_no");
                number.add(no);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return number;
    }
}
