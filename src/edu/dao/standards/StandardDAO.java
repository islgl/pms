package edu.dao.standards;

import edu.searchcriteria.StandardSearchCriteria;
import edu.vo.Standards;

import java.util.List;

public interface StandardDAO {
    void addStandard(Standards standard);
    void updateStandard(Standards standard, String standard_no);
    void deleteStandard(String standard_no);
    Standards getStandard(String standard_no);
    List<Standards>findStandards(StandardSearchCriteria criteria);
    List<String> findAllNumber();
}
