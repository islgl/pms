package edu.dao.thesis;

import edu.searchcriteria.ThesisSearchCriteria;
import edu.vo.Thesis;

import java.util.List;

public interface ThesisDAO {
    void addThesis(Thesis thesis);
    void updateThesis(Thesis thesis,String thesis_no);
    void deleteThesis(String thesis_no);
    Thesis getThesis(String thesis_no);
    List<Thesis> findThesis(ThesisSearchCriteria searchCriteria);
    List<String> findAllNumber();
}
