package edu.dao.thesis;

import edu.db.DbConnection;
import edu.searchcriteria.SearchCriteria;
import edu.searchcriteria.ThesisSearchCriteria;
import edu.vo.Thesis;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ThesisDAOImpl implements ThesisDAO {
    /*
    *向论文表中添加论文信息
    * */
    @Override
    public void addThesis(Thesis thesis) {
        //论文编号  论文名称  作者姓名 发表刊物名称  状态  发表时间  索引类型 归属库情况  论文扫描或PDF材料
        String sql="insert into thesis(thesis_no, name_thesis, name_author, identify_author, name_publication, " +
                "state_thesis,date_publication, type_index, attribution_library, scan_pdf, tutor_exam, " +
                "manager_exam) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,thesis.getThesis_no());
            psmt.setString(2,thesis.getName_thesis());
            psmt.setString(3,thesis.getName_author());
            psmt.setString(4,thesis.getIdentify_author());
            psmt.setString(5,thesis.getName_publication());
            psmt.setString(6,thesis.getState_thesis());
            psmt.setString(7,thesis.getDate_publication());
            psmt.setString(8,thesis.getType_index());
            psmt.setString(9,thesis.getAttribution_library());
            psmt.setString(10,thesis.getScan_pdf());
            psmt.setString(11,thesis.getTutor_exam());
            psmt.setString(12,thesis.getManager_exam());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("向thesis表中添加数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据论文编号更新论文表中的信息
    * */
    @Override
    public void updateThesis(Thesis thesis, String thesis_no) {
        String sql="update thesis set tutor_exam=?,manager_exam=? where thesis_no=?";
        Connection con=null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,thesis.getTutor_exam());
            psmt.setString(2,thesis.getManager_exam());
            psmt.setString(3,thesis_no);
            psmt.executeUpdate();
            psmt.close();
//            System.out.println("更新论文信息成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据论文编号删除论文表中的数据
    * */
    @Override
    public void deleteThesis(String thesis_no) {
        String sql = "delete from thesis where thesis_no=?";
        Connection con = null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();
        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, thesis_no);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("删除thesis表中数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据论文编号获取指定论文信息
    * */
    @Override
    public Thesis getThesis(String thesis_no) {
        String sql="select * from thesis where thesis_no=?";
        Connection con=null;
        PreparedStatement psmt;
        ResultSet rs=null;
        Thesis thesis=new Thesis();
        DbConnection dbConnection=new DbConnection();
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,thesis_no);
            rs=psmt.executeQuery();
            while (rs.next()){
                //论文编号  论文名称  发表刊物名称  状态  发表时间  索引类型 归属库情况  论文扫描或PDF材料
                String thno=rs.getString("thesis_no");                  //论文编号
                String name1=rs.getString("name_thesis");               //论文名称
                String name2=rs.getString("name_author");               //作者姓名
                String identify=rs.getString("identify_author");        //作者身份
                String publication=rs.getString("name_publication");    //论文发表刊物名称
                String state=rs.getString("state_thesis");              //论文状态
                String date=rs.getString("date_publication");           //论文发表时间
                String index=rs.getString("type_index");                //索引类型
                String library=rs.getString("attribution_library");     //论文归属库情况
                String pdf=rs.getString("scan_pdf");                    //论文扫描或PDF材料
                String tutor_exam=rs.getString("tutor_exam");           //导师初审
                String manager_exam=rs.getString("manager_exam");       //管理人员终审
                thesis.setThesis_no(thno);
                thesis.setName_thesis(name1);
                thesis.setName_author(name2);
                thesis.setIdentify_author(identify);
                thesis.setName_publication(publication);
                thesis.setState_thesis(state);
                thesis.setDate_publication(date);
                thesis.setType_index(index);
                thesis.setAttribution_library(library);
                thesis.setScan_pdf(pdf);
                thesis.setTutor_exam(tutor_exam);
                thesis.setManager_exam(manager_exam);
                System.out.println("论文编号:"+thno+" 论文名称:"+name1+" 作者姓名:"+name2+" 作者身份:"+identify+
                        " 发表刊物名称:"+publication+" 论文状态:"+state+" 发表时间:"+date+
                        " 索引类型:"+index+" 归属库:"+library+" 扫描或pdf材料:"+pdf+
                        " 导师审核:"+tutor_exam+" 研究生培养管理人员审核:"+manager_exam);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return thesis;
    }

    /*
    * 查询论文表中符合查询条件的所有论文信息
    * */
    @Override
    public List<Thesis> findThesis(ThesisSearchCriteria criteria) {
        List<Thesis> theses=new ArrayList<Thesis>();
        Connection con=null;
        ResultSet rs=null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();

        //构造查询语句
        String sql="select * from thesis where ";  //查询条件待定
        StringBuilder criteriaSql=new StringBuilder(512);
        criteriaSql.append(sql);
        //论文编号
        if(criteria.getThesis_no()!=null){
            criteriaSql.append("thesis_no='").append(SearchCriteria.fixSqlFieldValue(criteria.getThesis_no())).append("' AND ");
        }
        //论文名称
        if(criteria.getName_thesis()!=null){
            criteriaSql.append("name_thesis='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_thesis())).append("' AND ");
        }
        //作者姓名
        if(criteria.getName_author()!=null){
            criteriaSql.append("name_author='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_author())).append("' AND ");
        }
        //作者身份
        if(criteria.getIdentify_author()!=null){
            criteriaSql.append("identify_author='").append(SearchCriteria.fixSqlFieldValue(criteria.getIdentify_author())).append("' AND ");
        }
        //发表刊物名称
        if(criteria.getName_publication()!=null){
            criteriaSql.append("name_publication='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_publication())).append("' AND ");
        }
        //论文状态
        if(criteria.getState_thesis()!=null){
            criteriaSql.append("state_thesis='").append(SearchCriteria.fixSqlFieldValue(criteria.getState_thesis())).append("' AND ");
        }
        //发表时间
        if(criteria.getDate_publication()!=null){
            criteriaSql.append("date_publication='").append(SearchCriteria.fixSqlFieldValue(criteria.getDate_publication())).append("' AND ");
        }
        //索引类型
        if(criteria.getType_index()!=null){
            criteriaSql.append("type_index='").append(SearchCriteria.fixSqlFieldValue(criteria.getType_index())).append("' AND ");
        }
        //归属库情况
        if(criteria.getAttribution_library()!=null){
            criteriaSql.append("attribution_library='").append(SearchCriteria.fixSqlFieldValue(criteria.getAttribution_library())).append("' AND ");
        }
        //论文扫描或PDF材料
        if(criteria.getScan_pdf()!=null){
            criteriaSql.append("scan_pdf='").append(SearchCriteria.fixSqlFieldValue(criteria.getScan_pdf())).append("' AND ");
        }
        //导师审核
        if(criteria.getTutor_exam()!=null){
            criteriaSql.append("tutor_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getTutor_exam())).append("' AND ");
        }
        //研究生培养管理人员
        if(criteria.getManager_exam()!=null){
            criteriaSql.append("manager_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getManager_exam())).append("' AND ");
        }

        sql= criteria.removeRedundancy(criteriaSql);
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                Thesis thesis=new Thesis();
                thesis.setThesis_no(rs.getString("thesis_no"));
                thesis.setName_thesis(rs.getString("name_thesis"));
                thesis.setName_author(rs.getString("name_author"));
                thesis.setName_publication(rs.getString("name_publication"));
                thesis.setState_thesis(rs.getString("state_thesis"));
                thesis.setType_index(rs.getString("type_index"));
                thesis.setDate_publication(rs.getString("date_publication"));
                thesis.setAttribution_library(rs.getString("attribution_library"));
                thesis.setScan_pdf(rs.getString("scan_pdf"));
                thesis.setTutor_exam(rs.getString("tutor_exam"));
                thesis.setManager_exam(rs.getString("manager_exam"));
                theses.add(thesis);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return theses;
    }

    /*
    * 获取数据表中所有编号
    * 编号是主键，不允许重复
    * 在添加新数据时先对比主键值是否重复
    * */
    @Override
    public List<String> findAllNumber() {
        List<String> numbers=new ArrayList<>();
        Connection con=null;
        ResultSet rs=null;
        PreparedStatement psmt;
        DbConnection dbConnection=new DbConnection();

        //构造查询语句
        String sql="select * from thesis";
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                Thesis thesis=new Thesis();
                thesis.setThesis_no(rs.getString("thesis_no"));
                numbers.add(thesis.getThesis_no());
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return numbers;
    }
}
