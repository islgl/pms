package edu.dao.Expenditure;

import edu.db.DbConnection;
import edu.searchcriteria.ExpenditureSearchCriteria;
import edu.searchcriteria.SearchCriteria;
import edu.vo.Expenditure;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExpenditureDaolmpl implements ExpenditureDao{

    @Override
    public void addexpenditure(Expenditure expenditure) {
        String Expenditure_INSERT_SQL = "INSERT INTO Expenditure VALUES(?,?,?,?) ";
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt;
        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(Expenditure_INSERT_SQL);
            psmt.setString(1, expenditure.getId());
            psmt.setFloat(2, expenditure.getExpenditure());
            psmt.setInt(3, expenditure.getTeacher_signature());
            psmt.setInt(4, expenditure.getPrincipal_signature());
            psmt.executeUpdate();
            psmt.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static final String Expenditure_DELETE_SQL = "DELETE FROM Expenditure WHERE id=?";

    @Override
    public void deleteexpenditure(String expenditureId) {
        DbConnection dbConnection = new DbConnection();
        Connection con = null;

        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(Expenditure_DELETE_SQL);
            psmt.setString(1, expenditureId);
            psmt.executeUpdate();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static final String Expenditure_UPDATE_SQL = "UPDATE Expenditure SET id=?,expenditure=?,teacher_signature=?,principal_signature=? WHERE id=?";
    @Override
    public void updateexpenditure(String expenditureId, Expenditure expenditure) {
        Connection con = null;
        DbConnection dbConnection = new DbConnection();

        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(Expenditure_UPDATE_SQL);
            psmt.setString(1, expenditure.getId());
            psmt.setFloat(2, expenditure.getExpenditure());
            psmt.setInt(3, expenditure.getTeacher_signature());
            psmt.setInt(4, expenditure.getPrincipal_signature());
            psmt.setString(5, expenditureId);
            psmt.executeUpdate();
            psmt.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static final String Expenditure_GET_SQL = "SELECT * FROM Expenditure WHERE id=?";
    @Override
    public Expenditure getexpenditure(String expenditureId) {
        Expenditure expenditure = null;
        DbConnection dbConnection = new DbConnection();
        PreparedStatement psmt = null;
        Connection con = null;
        ResultSet rs = null;

        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(Expenditure_GET_SQL);
            psmt.setString(1, expenditureId);
            psmt.executeQuery();
            rs = psmt.getResultSet();

            while (rs.next()) {
                expenditure = new Expenditure(
                        rs.getString("id"),
                        rs.getFloat("expenditure"),
                        rs.getInt("teacher_signature"),
                        rs.getInt("principal_signature")
                );
            }
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return expenditure;

    }

    @Override
    public List<Expenditure> findexpenditure(ExpenditureSearchCriteria searchCriteria) {
        List<Expenditure> expenditure = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;

        con = dbConnection.getConnection();
        StringBuilder criteriaSql = new StringBuilder(512);
        String Expenditure_FIND_SQL = "SELECT * FROM Expenditure WHERE ";
        criteriaSql.append(Expenditure_FIND_SQL);

        //构造查询语句
        if (searchCriteria.getId() != null) {
            criteriaSql.append("id='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getId()) + "' AND ");
        }
        if (searchCriteria.getExpenditure() != 0) {
            criteriaSql.append("expenditure='" + SearchCriteria.fixSqlFieldValue(String.valueOf(searchCriteria.getExpenditure())) + "' AND ");
        }
        if (searchCriteria.getTeacher_signature() != -1) {
            criteriaSql.append("teacher_signature='" + SearchCriteria.fixSqlFieldValue(String.valueOf(searchCriteria.getTeacher_signature())) + "' AND ");
        }
        if (searchCriteria.getPrincipal_signature() != -1) {
            criteriaSql.append("principal_signature='" + SearchCriteria.fixSqlFieldValue(String.valueOf(searchCriteria.getPrincipal_signature())) + "' AND ");
        }

        // 去除多余的WHERE和AND关键字
        Expenditure_FIND_SQL = searchCriteria.removeRedundancy(criteriaSql);

        try {
            psmt = con.prepareStatement(Expenditure_FIND_SQL);
            psmt.executeQuery();
            rs = psmt.getResultSet();

            while (rs.next()) {
                Expenditure expend = new Expenditure(
                        rs.getString("id"),
                        rs.getFloat("expenditure"),
                        rs.getInt("teacher_signature"),
                        rs.getInt("principal_signature")
                );
                expenditure.add(expend);
            }

            rs.close();
            psmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return expenditure;
    }
}
