package edu.dao.Expenditure;

import edu.vo.Project;
import edu.searchcriteria.ExpenditureSearchCriteria;
import edu.searchcriteria.ProjectSearchCriteria;
import edu.vo.Expenditure;

import java.util.List;

public interface ExpenditureDao {
    void addexpenditure(Expenditure expenditure);
    void deleteexpenditure(String expenditureId);
    void updateexpenditure(String expenditureId,Expenditure expenditure);
    Expenditure getexpenditure(String expenditureId);
    List<Expenditure> findexpenditure(ExpenditureSearchCriteria searchCriteria);
}
