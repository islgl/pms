package edu.dao.subject;

import edu.db.DbConnection;
import edu.searchcriteria.SearchCriteria;
import edu.searchcriteria.SubjectSearCriteria;
import edu.vo.Subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SubjectDaoImpl implements SubjectDao {

    private static final String ADD_SQL = "insert into subject values(?,?);";
    private static final String DELETE_SQL = "delete from subject where id=?;";
    private static final String UPDATE_SQL = "update subject set id=?,name=? where id=?;";
    private static final String GET_SQL = "select * from subject where id=?";
    private static final String FIND_SQL = "select * from subject WHERE ";

    /**
     * 增加学科信息
     *
     * @param subject 要增加的学科对象
     */
    @Override
    public void add(Subject subject) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(ADD_SQL);
            statement.setString(1, subject.getId());
            statement.setString(2, subject.getName());

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除学科信息
     *
     * @param id 要删除的学科编号
     */
    @Override
    public void delete(String id) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(DELETE_SQL);
            statement.setString(1, id);

            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 修改学科信息
     *
     * @param id         要修改的学科编号
     * @param newSubject 修改后的学科对象
     */
    @Override
    public void update(String id, Subject newSubject) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(UPDATE_SQL);

            statement.setString(1, newSubject.getId());
            statement.setString(2, newSubject.getName());
            statement.setString(3, id);

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取指定学科信息
     *
     * @param id 要获取的学科编号
     * @return 该学科的对象
     */
    @Override
    public Subject get(String id) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        Subject subject = null;

        try {
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(GET_SQL);

            statement.setString(1, id);
            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                subject = new Subject(result.getString("id"), result.getString("name"));
            }
            result.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subject;
    }

    /**
     * 根据指定条件查询学科信息
     *
     * @param criteria 查询条件
     * @return 符合条件的学科对象列表
     */
    @Override
    public List<Subject> find(SubjectSearCriteria criteria) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        List<Subject> subjectList = new ArrayList<>();

        StringBuilder criteriaSql = new StringBuilder(512);
        criteriaSql.append(FIND_SQL);

        if (criteria.getId() != null) {
            criteriaSql.append("id='").append(SearchCriteria.fixSqlFieldValue(criteria.getId())).append("' AND ");
        }
        if (criteria.getName() != null) {
            criteriaSql.append("name='").append(SearchCriteria.fixSqlFieldValue(criteria.getName())).append("'");
        }

        String sql = criteria.removeRedundancy(criteriaSql);

        try {
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                Subject subject = new Subject(result.getString("id"), result.getString("name"));
                subjectList.add(subject);
            }

            result.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjectList;
    }

}
