package edu.dao.subject;

import edu.searchcriteria.SubjectSearCriteria;
import edu.vo.Subject;

import java.util.List;

public interface SubjectDao {
    /**
     * 增加学科信息
     *
     * @param subject 要增加的学科对象
     */
    void add(Subject subject);

    /**
     * 删除学科信息
     *
     * @param id 要删除的学科编号
     */
    void delete(String id);

    /**
     * 修改学科信息
     *
     * @param id         要修改的学科编号
     * @param newSubject 修改后的学科对象
     */
    void update(String id, Subject newSubject);

    /**
     * 获取指定学科信息
     *
     * @param id 要获取的学科编号
     * @return 该学科的对象
     */
    Subject get(String id);

    /**
     * 根据指定条件查询学科信息
     *
     * @param criteria 查询条件
     * @return 符合条件的学科对象列表
     */
    List<Subject> find(SubjectSearCriteria criteria);
}
