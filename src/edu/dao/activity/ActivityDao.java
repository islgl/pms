package edu.dao.activity;

import edu.bo.Activity;
import edu.searchcriteria.ActivitySearchCriteria;

import java.util.List;

public interface ActivityDao {
    void addActivity(Activity activity);

    void updateActivity(String id, Integer a_count, Activity activity);

    void deleteActivity(String id, Integer a_count);
    Activity getActivity(String id);
    List<Activity> findActivitys(ActivitySearchCriteria searchCriteria);
}
