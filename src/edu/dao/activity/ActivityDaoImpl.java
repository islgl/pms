package edu.dao.activity;

import edu.bo.Activity;
import edu.db.DbConnection;
import edu.searchcriteria.ActivitySearchCriteria;
import edu.searchcriteria.SearchCriteria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ActivityDaoImpl extends DbConnection implements ActivityDao {
    private static final String ACTIVITY_INSERT_SQL = "INSERT INTO Activity(id,name,major,a_count,a_name,a_place,a_time,a_CEname,a_note) VALUES(?,?,?,?,?,?,?,?,?) ";
    @Override
    public void addActivity(Activity activity) {
        Connection con = null;
        try{
            con = getConnection();
            PreparedStatement psmt = con.prepareStatement(ACTIVITY_INSERT_SQL);
            psmt.setString(1, activity.getId());
            psmt.setString(2, activity.getName());
            psmt.setString(3, activity.getMajor());
            psmt.setInt(4, activity.getA_count());
            psmt.setString(5, activity.getA_name());
            psmt.setString(6, activity.getA_place());
            psmt.setString(7, activity.getA_time());
            psmt.setString(8, activity.getCEname());
            psmt.setString(9, activity.getA_note());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("增加成功！");
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            try{
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private static final String ACTIVITY_UPDATE_SQL = "UPDATE Activity SET id=?,name=?,major=?,a_count=?,a_name=?,a_place=?,a_time=?,a_CEname=?,a_note=? WHERE id=? and a_count=?";
    @Override
    public void updateActivity(String id,Integer a_count,Activity activity) {
        Connection con = null;
        DbConnection dbConnection = new DbConnection();
        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(ACTIVITY_UPDATE_SQL);
            psmt.setString(1, activity.getId());
            psmt.setString(2, activity.getName());
            psmt.setString(3, activity.getMajor());
            psmt.setInt(4, activity.getA_count());
            psmt.setString(5, activity.getA_name());
            psmt.setString(6, activity.getA_place());
            psmt.setString(7, activity.getA_time());
            psmt.setString(8, activity.getCEname());
            psmt.setString(9, activity.getA_note());
            psmt.setString(10,id);
            psmt.setInt(11,a_count);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("更新成功！");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static final String ACTIVITY_UPDATE_CHECK_SQL = "UPDATE Activity SET check_state=? WHERE id=? and a_count=?";
    public void updateCheck(String id, Integer a_count,Integer check) {
        Connection con = null;
        DbConnection dbConnection = new DbConnection();
        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(ACTIVITY_UPDATE_CHECK_SQL);
            psmt.setInt(1, check);
            psmt.setString(2,id);
            psmt.setInt(3,a_count);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("审核成功！");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private static final String ACTIVITY_DELETE_SQL="DELETE FROM Activity WHERE id=? and a_count=?";

    @Override
    public void deleteActivity(String id,Integer a_count) {
        DbConnection dbConnection = new DbConnection();
        Connection connection=dbConnection.getConnection();
        PreparedStatement statement = null;
        try{
            statement = connection.prepareStatement(ACTIVITY_DELETE_SQL);
            statement.setString(1, id);
            statement.setInt(2, a_count);
            statement.executeUpdate();
            statement.close();
            dbConnection.close(connection);
            System.out.println("删除成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert connection != null;
                connection.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private static final String ACTIVITY_GET_SQL = "SELECT * FROM Activity WHERE id=?";
    @Override
    public Activity getActivity(String id) {
        Activity activity = null;
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        ResultSet rs = null;
        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(ACTIVITY_GET_SQL);
            psmt.setString(1, id);
            rs = psmt.executeQuery();
            while (rs.next()) {
                activity = new Activity(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("major"),
                        rs.getInt("a_count"),
                        rs.getString("a_name"),
                        rs.getString("a_place"),
                        rs.getString("a_time"),
                        rs.getString("a_CEname"),
                        rs.getString("a_note"),
                        rs.getInt("check_state")
                );
                System.out.println("学号：" + id + "\n姓名：" + rs.getString(2)+ "\n学科：" + rs.getString(3)+ "\n序号：" + rs.getInt(4)+ "\n活动名称：" + rs.getString(5)
                        + "\n活动地点：" + rs.getString(6)+ "\n活动时间：" + rs.getString(7)+ "\n报告中英文名称：" + rs.getString(8)+ "\n备注：" + rs.getString(9));
            }
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return activity;
    }

    private static String ACTIVITY_FIND_SQL = "SELECT * FROM Activity WHERE ";
    @Override
    public List<Activity> findActivitys(ActivitySearchCriteria searchCriteria) {
        List<Activity> activity = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        con = dbConnection.getConnection();
        StringBuilder criteriaSql = new StringBuilder(512);
        criteriaSql.append(ACTIVITY_FIND_SQL);
        if (searchCriteria.getId() != null) {
            criteriaSql.append("id='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getId()) + "' AND ");
        }
        if (searchCriteria.getName() != null) {
            criteriaSql.append("name='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getName()) + "' AND ");
        }
        if (searchCriteria.getMajor() != null) {
            criteriaSql.append("major='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getMajor()) + "' AND ");
        }
//        if (searchCriteria.getA_count() != null) {
//            criteriaSql.append("a_count='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getA_count()) + "' AND ");
//        }
        if (searchCriteria.getA_name() != null) {
            criteriaSql.append("a_name='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getA_name()) + "' AND ");
        }
        if (searchCriteria.getA_place() != null) {
            criteriaSql.append("a_place='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getA_place()) + "' AND ");
        }
        if (searchCriteria.getA_time() != null) {
            criteriaSql.append("a_time='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getA_time()) + "' AND ");
        }
        if (searchCriteria.getCEname() != null) {
            criteriaSql.append("a_CEname='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getCEname()) + "' AND ");
        }
        if (searchCriteria.getA_note() != null) {
            criteriaSql.append("a_note='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getA_note()) + "' AND ");
        }
        if (searchCriteria.getCheck() != null) {
            criteriaSql.append("a_note='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getA_note()) + "' AND ");
        }
        // 去除多余的WHERE和AND关键字
        ACTIVITY_FIND_SQL = searchCriteria.removeRedundancy(criteriaSql);
        try {
            psmt = con.prepareStatement(ACTIVITY_FIND_SQL);
            psmt.executeQuery();
            rs = psmt.getResultSet();
            while (rs.next()) {
                Activity cou = new Activity(rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("major"),
                        rs.getInt("a_count"),
                        rs.getString("a_name"),
                        rs.getString("a_place"),
                        rs.getString("a_time"),
                        rs.getString("a_CEname"),
                        rs.getString("a_note"),
                        rs.getInt("check_state")
                );
                activity.add(cou);
                System.out.println("学号：" + rs.getString(1)+ "\n姓名：" + rs.getString(2)+ "\n学科：" + rs.getString(3)+ "\n序号：" + rs.getInt(4)+ "\n活动名称：" + rs.getString(5)
                        + "\n活动地点：" + rs.getString(6)+ "\n活动时间：" + rs.getString(7)+ "\n报告中英文名称：" + rs.getString(8)+ "\n备注：" + rs.getString(9));
            }
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return activity;
    }
}
