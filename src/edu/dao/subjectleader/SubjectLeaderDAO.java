package edu.dao.subjectleader;

import edu.searchcriteria.SubjectLeaderSearchCriteria;
import edu.vo.SubjectLeader;

import java.util.List;

public interface SubjectLeaderDAO {
    void addSubjectLeader(SubjectLeader subjectleader);
    void updateSubjectLeader(String subjectleader_no, SubjectLeader subjectleader);
    void deleteSubjectLeader(String subjectleader_no);
    SubjectLeader getSubjectLeader(String subjectleader_no);
    List<SubjectLeader> findSubjectLeaders(SubjectLeaderSearchCriteria searchCriteria);
}
