package edu.dao.subjectleader;

import edu.db.DbConnection;
import edu.searchcriteria.SearchCriteria;
import edu.searchcriteria.SubjectLeaderSearchCriteria;
import edu.vo.SubjectLeader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SubjectLeaderDAOImpl extends DbConnection implements SubjectLeaderDAO {
    private static final String SUBJECTLEADER_INSERT_SQL = "INSERT INTO SubjectLeader(subjectleader_no,subjectleader_name,subject) VALUES(?,?,?) ";
    @Override
    public void addSubjectLeader(SubjectLeader subjectleader) {
        Connection con = null;
        try{
            con = getConnection();
            PreparedStatement psmt = con.prepareStatement(SUBJECTLEADER_INSERT_SQL);
            psmt.setString(1, subjectleader.getSubjectleader_no());
            psmt.setString(2, subjectleader.getSubjectleader_name());
            psmt.setString(3, subjectleader.getSubject());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("增加成功！");
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            try{
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private static final String SUBJECTLEADER_UPDATE_SQL = "UPDATE SubjectLeader SET subjectleader_name=?,subject=? WHERE subjectleader_no=?";
    @Override
    public void updateSubjectLeader(String subjectleader_no, SubjectLeader subjectleader) {
        Connection con = null;
        DbConnection dbConnection = new DbConnection();
        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(SUBJECTLEADER_UPDATE_SQL);
//            psmt.setString(1, subjectleader.getSubjectleader_no());
            psmt.setString(1, subjectleader.getSubjectleader_name());
            psmt.setString(2, subjectleader.getSubject());
            psmt.setString(3, subjectleader.getSubjectleader_no());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("更新成功！");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static final String SUBJECTLEADER_DELETE_SQL="DELETE FROM SubjectLeader WHERE subjectleader_no=?";
    @Override
    public void deleteSubjectLeader(String subjectleader_no) {
        DbConnection dbConnection = new DbConnection();
        Connection connection=dbConnection.getConnection();
        PreparedStatement statement = null;
        try{
            statement = connection.prepareStatement(SUBJECTLEADER_DELETE_SQL);
            statement.setString(1, subjectleader_no);
            statement.executeUpdate();
            statement.close();
            dbConnection.close(connection);
            System.out.println("删除成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert connection != null;
                connection.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private static final String SUBJECTLEADER_GET_SQL = "SELECT * FROM SubjectLeader WHERE subjectleader_no=?";
    @Override
    public SubjectLeader getSubjectLeader(String subjectleader_no) {
        SubjectLeader subjectleader = null;
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        ResultSet rs = null;
        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(SUBJECTLEADER_GET_SQL);
            psmt.setString(1, subjectleader_no);
            rs = psmt.executeQuery();
            while (rs.next()) {
                subjectleader = new SubjectLeader(
                        rs.getString("subjectleader_no"),
                        rs.getString("subjectleader_name"),
                        rs.getString("subject")
                );
                //System.out.println("工号：" + subjectleader_no + "\n姓名：" + rs.getString(2));
            }
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return subjectleader;
    }

    private static String SUBJECTLEADER_FIND_SQL = "SELECT * FROM SubjectLeader WHERE ";
    @Override
    public List<SubjectLeader> findSubjectLeaders(SubjectLeaderSearchCriteria searchCriteria) {
        List<SubjectLeader> subjectleader = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        con = dbConnection.getConnection();
        StringBuilder criteriaSql = new StringBuilder(512);
        criteriaSql.append(SUBJECTLEADER_FIND_SQL);
        if (searchCriteria.getSubjectleader_no() != null) {
            criteriaSql.append("subjectleader_no='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getSubjectleader_no()) + "' AND ");
        }
        if (searchCriteria.getSubjectleader_name() != null) {
            criteriaSql.append("subjectleader_name='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getSubjectleader_name()) + "' AND ");
        }
        if (searchCriteria.getSubject() != null) {
            criteriaSql.append("subject='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getSubject()) + "' AND ");
        }
        // 去除多余的WHERE和AND关键字
        SUBJECTLEADER_FIND_SQL = searchCriteria.removeRedundancy(criteriaSql);
        try {
            psmt = con.prepareStatement(SUBJECTLEADER_FIND_SQL);
            psmt.executeQuery();
            rs = psmt.getResultSet();
            while (rs.next()) {
                SubjectLeader cou = new SubjectLeader(rs.getString("subjectleader_no"),
                        rs.getString("subjectleader_name"),
                        rs.getString("subject")
                );
                subjectleader.add(cou);
                String subjectleader_no = rs.getString(1);
                String subjectleader_name = rs.getString(2);
                String subject = rs.getString(3);

            }
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return subjectleader;
    }
}