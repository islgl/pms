package edu.dao.pingtai;

import edu.searchcriteria.PingtaiSearchCriteria;
import edu.vo.Pingtai;

import java.util.List;

public interface PingtaiDAO {
    void addPingtai(Pingtai pingtai);
    void updatePingtai(Pingtai pingtai);
    void deletePingtai(String pingtai_no);
    Pingtai getPingtai(String pingtai_no);

    List<Pingtai> findPingtai(PingtaiSearchCriteria searchCriteria);

    List<String> findAllNumber();
}
