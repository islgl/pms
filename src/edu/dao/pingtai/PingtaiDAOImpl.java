package edu.dao.pingtai;


import edu.dao.pingtai.PingtaiDAO;
import edu.db.DbConnection;
import edu.searchcriteria.PingtaiSearchCriteria;
import edu.searchcriteria.SearchCriteria;
import edu.vo.Pingtai;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PingtaiDAOImpl implements PingtaiDAO {

    /*
     *向软硬件平台表中添加软硬件平台信息
     * */
    @Override
    public void addPingtai(Pingtai pingtai) {
        //名称、平台服务单位、平台上线时间、贡献度（整数）、佐证材料
        String sql="insert into pingtai(pingtai_no, name_student, identify_student, pingtai_name, service_unit, " +
                "public_date, contribution, zuozheng, tutor_exam, manager_exam) values(?,?,?,?,?,?,?,?,?,?)";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,pingtai.getPingtai_no());
            psmt.setString(2,pingtai.getName_student());
            psmt.setString(3,pingtai.getIdentify_student());
            psmt.setString(4,pingtai.getPingtai_name());
            psmt.setString(5,pingtai.getService_unit());
            psmt.setString(6,pingtai.getPublic_date());
            psmt.setInt(7,pingtai.getContribution());
            psmt.setString(8,pingtai.getZuozheng());
            psmt.setString(9,pingtai.getTutor_exam());
            psmt.setString(10,pingtai.getManager_exam());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("向pingtai表中添加数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    /*
     * 根据平台名称更新平台表中的信息
     * */
    @Override
    public void updatePingtai(Pingtai pingtai) {
        String sql="update pingtai set tutor_exam=?,manager_exam=? where pingtai_no=?";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,pingtai.getTutor_exam());
            psmt.setString(2,pingtai.getManager_exam());
            psmt.setString(3,pingtai.getPingtai_no());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("更新平台信息成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    /*
     * 根据平台名称删除平台表中的数据
     * */
    @Override
    public void deletePingtai(String pingtai_no) {
        String sql="delete from pingtai where pingtai_no=?";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,pingtai_no);
            psmt.executeUpdate();
            psmt.close();
//            System.out.println("平台信息删除成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public Pingtai getPingtai(String pingtai_no) {
        String sql="select * from pingtai where pingtai_no=?";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        ResultSet rs=null;
        Pingtai pingtai=new Pingtai();
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,pingtai_no);
            rs=psmt.executeQuery();
            while (rs.next()){
                //名称、平台服务单位、平台上线时间、贡献度（整数）、佐证材料
                String no=rs.getString("pingtai_no");
                String name_student=rs.getString("name_student");
                String identify=rs.getString("identify_student");
                String pingtai_name = rs.getString("pingtai_name");
                String service_unit=rs.getString("service_unit");               //
                String public_date=rs.getString("public_date");               //
                int contribution=rs.getInt("contribution");        //
                String zuozheng=rs.getString("zuozheng");    //
                String tutor_exam=rs.getString("tutor_exam");           //导师初审
                String manager_exam=rs.getString("manager_exam");       //管理人员终审

                pingtai.setPingtai_no(no);
                pingtai.setPingtai_name(pingtai_name);
                pingtai.setName_student(name_student);
                pingtai.setIdentify_student(identify);
                pingtai.setService_unit(service_unit);
                pingtai.setPublic_date(public_date);
                pingtai.setContribution(contribution);
                pingtai.setZuozheng(zuozheng);
                pingtai.setTutor_exam(tutor_exam);
                pingtai.setManager_exam(manager_exam);
                System.out.println("编号:"+no+" 学生姓名:"+name_student+" 学生身份:"+identify+" 名称:"+pingtai_name+
                        " 平台服务单位:"+service_unit+" 平台上线时间:"+public_date+" 贡献度:"+contribution+
                        " 佐证材料:"+zuozheng+" 导师初审:"+tutor_exam+" 管理人员终审:"+manager_exam);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return pingtai;
    }



    /*
     * 根据平台名称获取指定平台信息
     * */
    @Override
    public List<Pingtai> findPingtai(PingtaiSearchCriteria criteria) {
        DbConnection dbConnection=new DbConnection();
        List<Pingtai> pts=new ArrayList<Pingtai>();
        Connection con=null;
        ResultSet rs=null;
        PreparedStatement psmt;

        //构造查询语句
        String sql="select * from pingtai where ";  //查询条件待定
        StringBuilder criteriaSql=new StringBuilder(512);
        criteriaSql.append(sql);
        //编号
         if (criteria.getPingtai_no()!=null){
             criteriaSql.append("pingtai_no='").append(SearchCriteria.fixSqlFieldValue
                     (criteria.getPingtai_no())).append("' AND ");
         }
         //学生姓名
        if (criteria.getName_student()!=null){
            criteriaSql.append("name_student='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getName_student())).append("' AND ");
        }
        //学生身份
        if (criteria.getIdentify_student()!=null){
            criteriaSql.append("identify_student='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getIdentify_student())).append("' AND ");
        }
        //名称
        if(criteria.getPingtai_name()!=null){
            criteriaSql.append("pingtai_name='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getPingtai_name())).append("' AND ");
        }
        //平台服务单位
        if(criteria.getService_unit()!=null){
            criteriaSql.append("service_unit='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getService_unit())).append("' AND ");
        }
        //平台上线时间
        if(criteria.getPublic_date()!=null){
            criteriaSql.append("public_date='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getPublic_date())).append("' AND ");
        }
        //贡献度（整数）
        if(criteria.getContribution()!=0){
            criteriaSql.append("contribution='").append(SearchCriteria.fixSqlFieldValue
                    (String.valueOf(criteria.getContribution()))).append("' AND ");
        }
        //佐证材料
        if(criteria.getZuozheng()!=null){
            criteriaSql.append("zuozheng='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getZuozheng())).append("' AND ");
        }
        //导师审核
        if(criteria.getTutor_exam()!=null){
            criteriaSql.append("tutor_exam='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getTutor_exam())).append("' AND ");
        }
        //研究生培养管理人员
        if(criteria.getManager_exam()!=null){
            criteriaSql.append("manager_exam='").append(SearchCriteria.fixSqlFieldValue
                    (criteria.getManager_exam())).append("' AND ");
        }

        sql= criteria.removeRedundancy(criteriaSql);
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                Pingtai pingtai=new Pingtai();
                pingtai.setPingtai_no(rs.getString("pingtai_no"));
                pingtai.setPingtai_name(rs.getString("pingtai_name"));
                pingtai.setService_unit(rs.getString("service_unit"));
                pingtai.setPublic_date(rs.getString("public_date"));
                pingtai.setContribution(rs.getInt("contribution"));
                pingtai.setZuozheng(rs.getString("zuozheng"));
                pingtai.setTutor_exam(rs.getString("tutor_exam"));
                pingtai.setManager_exam(rs.getString("manager_exam"));
                pts.add(pingtai);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return pts;
    }

    /*
     * 获取数据表中所有名称
     * 名称是主键，不允许重复
     * 在添加新数据时先对比主键值是否重复
     * */
    @Override
    public List<String> findAllNumber() {
        DbConnection dbConnection=new DbConnection();
        List<String> numbers=new ArrayList<>();
        Connection con=null;
        ResultSet rs=null;
        PreparedStatement psmt;

        //构造查询语句
        String sql="select * from pingtai";
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                String no=rs.getString("pingtai_no");
                numbers.add(no);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return numbers;
    }
}
