package edu.dao.project;

import edu.vo.Project;
import edu.db.DbConnection;
import edu.searchcriteria.ProjectSearchCriteria;
import edu.searchcriteria.SearchCriteria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProjectDaoImpl implements ProjectDao {

    /*
        添加项目认定表
         */
    @Override
    public void addproject(Project project) {
        String sql = "INSERT INTO project1 VALUES(?,?,?,?,?,?,?,?,?,?);";
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt;
        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, project.getSid());
            psmt.setString(2, project.getSname());
            psmt.setString(3, project.getSmajor());
            psmt.setString(4, project.getPid());
            psmt.setString(5, project.getPtype());
            psmt.setString(6, project.getPname());
            psmt.setString(7, project.getPtime());
            psmt.setString(8, project.getPwork());
            psmt.setFloat(9, project.getPexp());
            psmt.setString(10, project.getTid());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("一张项目认定表添加成功！");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
    删除项目认定表
     */
    @Override
    public void deleteproject(String pid) {
        String sql = "DELETE FROM project1 WHERE pid=?;";
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt;
        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, pid);
            psmt.executeUpdate();
            psmt.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
    更新项目认定表中信息
     */
    @Override
    public void updateproject(Project project) {
        String sql = "UPDATE project1 SET sid =?,sname=?,smajor=?,ptype=?,pname=?,pdate=?,pwork=?,expenditure=?,tid=? WHERE pid=?";
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt;
        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, project.getSid());
            psmt.setString(2, project.getSname());
            psmt.setString(3, project.getSmajor());
            psmt.setString(4, project.getPtype());
            psmt.setString(5, project.getPname());
            psmt.setString(6, project.getPtime());
            psmt.setString(7, project.getPwork());
            psmt.setFloat(8, project.getPexp());
            psmt.setString(9, project.getTid());
            psmt.setString(10, project.getPid());

            psmt.executeUpdate();
            psmt.close();
            System.out.println("认定表信息更新成功！");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
    获取所有信息
     */
    @Override
    public Project getProject(String pid) {
        String sql = "SELECT * FROM project1 WHERE pid=?";
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt;
        Project project = new Project();
        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, pid);
            ResultSet rs = psmt.executeQuery();
            while (rs.next()) {
                project.setSid(rs.getString("sid"));
                project.setSname(rs.getString("sname"));
                project.setSmajor(rs.getString("smajor"));
                project.setPid(rs.getString("pid"));
                project.setPtype(rs.getString("ptype"));
                project.setPname(rs.getString("pname"));
                project.setPtime(rs.getString("pdate"));
                project.setPwork(rs.getString("work"));
                project.setPexp(rs.getFloat("expenditure"));
                project.setTid(rs.getString("tid"));
                String s_id=rs.getString("sid");
                String s_name=rs.getString("sname");
                String s_major=rs.getString("smajor");
                String id=rs.getString("pid");
                String type=rs.getString("ptype");
                String name=rs.getString("pname");
                String date=rs.getString("pdate");
                String pwork=rs.getString("work");
                float exp=rs.getFloat("expenditure");
                String tu_id=rs.getString("tid");
                System.out.println("学生编号:"+s_id+" 学生姓名:"+s_name+" 学生学科:"+s_major+" 项目编号:"+id+" 项目类别:"+type+" 项目名称:"+name+" 参与项目时间:"+date+" 承担工作:"+pwork+" 承担工作的折合经费:"+exp+" 导师编号:"+tu_id);
            }
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return project;
    }
    @Override
    public Project getProject_tutor(String tid) {
        String sql = "SELECT * FROM project1 WHERE tid=?";
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt;
        Project project = new Project();
        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(sql);
            psmt.setString(1, tid);
            ResultSet rs = psmt.executeQuery();
            while (rs.next()) {
                project.setSid(rs.getString("sid"));
                project.setSname(rs.getString("sname"));
                project.setSmajor(rs.getString("smajor"));
                project.setPid(rs.getString("pid"));
                project.setPtype(rs.getString("ptype"));
                project.setPname(rs.getString("pname"));
                project.setPtime(rs.getString("pdate"));
                project.setPwork(rs.getString("work"));
                project.setPexp(rs.getFloat("expenditure"));
                project.setTid(rs.getString("tid"));
                String s_id=rs.getString("sid");
                String s_name=rs.getString("sname");
                String s_major=rs.getString("smajor");
                String id=rs.getString("pid");
                String type=rs.getString("ptype");
                String name=rs.getString("pname");
                String date=rs.getString("pdate");
                String pwork=rs.getString("work");
                float exp=rs.getFloat("expenditure");
                String tu_id=rs.getString("tid");
                System.out.println("学生编号:"+s_id+" 学生姓名:"+s_name+" 学生学科:"+s_major+"项目编号:"+id+" 项目类别:"+type+" 项目名称:"+name+" 参与项目时间:"+date+" 承担工作:"+pwork+" 承担工作的折合经费:"+exp+" 导师编号:"+tu_id);
            }
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return project;
    }
    /*
    查询符合条件的认定表
     */
    @Override
    public List<Project> findProject(ProjectSearchCriteria projectcriteria) {
        List<Project> projects = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection con = dbConnection.getConnection();
        PreparedStatement psmt = null;
        //构造查询语句
        String sql = "SELECT * FROM project1 WHERE ";  //查询条件待定
        StringBuilder criteriaSql = new StringBuilder(512);
        criteriaSql.append(sql);
        if (projectcriteria.getPid() != null) {
            criteriaSql.append("sid='").append(SearchCriteria.fixSqlFieldValue
                    (projectcriteria.getPid())).append("' AND ");
        }
        if (projectcriteria.getPid() != null) {
            criteriaSql.append("sname='").append(SearchCriteria.fixSqlFieldValue
                    (projectcriteria.getPid())).append("' AND ");
        }
        if (projectcriteria.getPid() != null) {
            criteriaSql.append("smajor='").append(SearchCriteria.fixSqlFieldValue
                    (projectcriteria.getPid())).append("' AND ");
        }
        if (projectcriteria.getPid() != null) {
            criteriaSql.append("pid='").append(SearchCriteria.fixSqlFieldValue
                    (projectcriteria.getPid())).append("' AND ");
        }
        if (projectcriteria.getPtype() != null) {
            criteriaSql.append("ptype='").append(SearchCriteria.fixSqlFieldValue
                    (projectcriteria.getPtype())).append("' AND ");
        }
        if (projectcriteria.getPname() != null) {
            criteriaSql.append("pname='").append(SearchCriteria.fixSqlFieldValue
                    (projectcriteria.getPname())).append("' AND ");
        }

        if (projectcriteria.getPtime() != null) {
            criteriaSql.append("ptime='").append(SearchCriteria.fixSqlFieldValue
                    (projectcriteria.getPtime())).append("' AND ");
        }
        if (projectcriteria.getPwork() != null) {
            criteriaSql.append("pwork='").append(SearchCriteria.fixSqlFieldValue
                    (projectcriteria.getPwork())).append("' AND ");
        }
        if (projectcriteria.getPexp() != 0) {
            criteriaSql.append("expenditure=").append(SearchCriteria.fixSqlFieldValue
                    (String.valueOf(projectcriteria.getPexp()))).append(" AND ");
        }
        if (projectcriteria.getTid() != null) {
            criteriaSql.append("tid='").append(SearchCriteria.fixSqlFieldValue
                    (projectcriteria.getTid())).append("' AND ");
        }
        // 去除多余的WHERE和AND关键字
        sql = projectcriteria.removeRedundancy(criteriaSql);
        try {
            psmt = con.prepareStatement(sql);
            ResultSet re = psmt.executeQuery();
            while (re.next()) {
                Project project = new Project();
                project.setSid(re.getString("sid"));
                project.setSname(re.getString("sname"));
                project.setSmajor(re.getString("smajor"));
                project.setPid(re.getString("pid"));
                project.setPtype(re.getString("ptype"));
                project.setPname(re.getString("pname"));
                project.setPtime(re.getString("pdate"));
                project.setPwork(re.getString("work"));
                project.setPexp(re.getFloat("expenditure"));
                project.setTid(re.getString("tid"));
                projects.add(project);
            }
            re.close();
            psmt.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projects;
    }

    @Override
    public List<String> findall() {
        DbConnection dbConnection = new DbConnection();
        List<String> numbers=new ArrayList<>();
        Connection con=null;
        ResultSet rs;
        PreparedStatement psmt;

        //构造查询语句
        String sql="select * from project1";
        try{
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            rs=psmt.executeQuery();
            while (rs.next()){
                String pid=rs.getString("pid");
                numbers.add(pid);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return numbers;
    }
}
