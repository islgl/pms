package edu.dao.project;
import edu.searchcriteria.ProjectSearchCriteria;
import edu.vo.Project;
import java.util.List;
public interface  ProjectDao {
    void addproject(Project project);
    void deleteproject(String pid);
    void updateproject(Project project);
    Project getProject(String pid);

    Project getProject_tutor(String tid);

    List<Project> findProject(ProjectSearchCriteria projectcriteria);
    List<String> findall();
}
