package edu.dao.teacher;
import java.sql.Connection;

public interface DAO {
    Connection getConnection();

}
