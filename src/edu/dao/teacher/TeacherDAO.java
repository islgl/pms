package edu.dao.teacher;
import edu.searchcriteria.TeacherSearchCriteria;
import edu.vo.Teacher;

import java.util.List;

public interface TeacherDAO {
    void addTeacher(Teacher teacher);
    void updateTeacher(String teacher_no, Teacher teacher);
    void deleteTeacher(String teacherr_no);
    Teacher getTeacher(String teacher_no);
    List<Teacher> findTeachers(TeacherSearchCriteria searchCriteria);
}
