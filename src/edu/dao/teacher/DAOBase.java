package edu.dao.teacher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOBase implements DAO {
    @Override
    public Connection getConnection() {
        String dbURL = "jdbc:sqlserver://host:43.143.169.18:1433;databaseName=Postgraduate";
        String userName = "SA";
        String userPwd = "superStrongPwd123";
        Connection con = null;
        try {
            con = DriverManager.getConnection(dbURL, userName, userPwd);

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("数据库连接失败！");
        }
        return con;
    }
}
