package edu.dao.teacher;

import edu.db.DbConnection;
import edu.searchcriteria.SearchCriteria;
import edu.searchcriteria.TeacherSearchCriteria;
import edu.vo.Teacher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TeacherDAOImpl extends DAOBase implements TeacherDAO {
    private static final String Teacher_INSERT_SQL = "INSERT INTO teacher(id,name,sex,age) VALUES(?,?,?,?) ";
    @Override
    public void addTeacher(Teacher teacher) {
        Connection con = null;
        DbConnection dbConnection = new DbConnection();
        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(Teacher_INSERT_SQL);
            psmt.setString(1, teacher.getId());
            psmt.setString(2, teacher.getName());
            psmt.setString(3, teacher.getSex());
            psmt.setInt(4, teacher.getAge());
            psmt.executeUpdate();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static final String Teacher_UPDATE_SQL = "UPDATE teacher SET id=?,name=?,sex=?,age=? WHERE id=?";
    @Override
    public void updateTeacher(String teacher_no, Teacher teacher) {
        Connection con = null;
        DbConnection dbConnection = new DbConnection();
        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(Teacher_UPDATE_SQL);
            psmt.setString(1, teacher.getId());
            psmt.setString(2, teacher.getName());
            psmt.setString(3, teacher.getSex());
            psmt.setInt(4, teacher.getAge());
            psmt.setString(5, teacher_no);
            psmt.executeUpdate();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static final String Teacher_DELETE_SQL = "DELETE FROM teacher WHERE id=?";
    @Override
    public void deleteTeacher(String teacherr_no) {
        DbConnection dbConnection = new DbConnection();
        Connection con = null;

        try {
            con = dbConnection.getConnection();
            PreparedStatement psmt = con.prepareStatement(Teacher_DELETE_SQL);
            psmt.setString(1, teacherr_no);
            psmt.executeUpdate();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static final String Teacher_GET_SQL = "SELECT * FROM teacher WHERE id=?";
    @Override
    public Teacher getTeacher(String teacher_no) {
        Teacher teacher= null;
        DbConnection dbConnection = new DbConnection();
        PreparedStatement psmt = null;
        Connection con = null;
        ResultSet rs = null;

        try {
            con = dbConnection.getConnection();
            psmt = con.prepareStatement(Teacher_GET_SQL);
            psmt.setString(1, teacher_no);
            psmt.executeQuery();
            rs = psmt.getResultSet();

            while (rs.next()) {
                teacher = new Teacher(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("sex"),
                        rs.getInt("age")
                );
            }
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConnection.close(con);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return teacher;
    }

    @Override
    public List<Teacher> findTeachers(TeacherSearchCriteria searchCriteria) {
        List<Teacher> teachers = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;

        con = dbConnection.getConnection();
        StringBuilder criteriaSql = new StringBuilder(512);
        String Teacher_FIND_SQL = "SELECT * FROM teacher WHERE ";
        criteriaSql.append(Teacher_FIND_SQL);

        //构造查询语句
        if (searchCriteria.getId() != null) {
            criteriaSql.append("id='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getId()) + "' AND ");
        }
        if (searchCriteria.getName() != null) {
            criteriaSql.append("name='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getName()) + "' AND ");
        }
        if (searchCriteria.getSex() != null) {
            criteriaSql.append("sex='" + SearchCriteria.fixSqlFieldValue(searchCriteria.getSex()) + "' AND ");
        }
        if (searchCriteria.getAge() != -1) {
            criteriaSql.append("age=" + SearchCriteria.fixSqlFieldValue(String.valueOf(searchCriteria.getAge())) + " AND ");
        }

        // 去除多余的WHERE和AND关键字
        Teacher_FIND_SQL = searchCriteria.removeRedundancy(criteriaSql);

        try {
            psmt = con.prepareStatement(Teacher_FIND_SQL);
            psmt.executeQuery();
            rs = psmt.getResultSet();

            while (rs.next()) {
                Teacher teacher = new Teacher(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("sex"),
                        rs.getInt("age")
                );
                teachers.add(teacher);
            }

            rs.close();
            psmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return teachers;
    }
}
