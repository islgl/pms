package edu.dao.assistant;

import edu.bo.Assistant;
import edu.db.DbConnection;
import edu.searchcriteria.AssistantSearchCriteria;
import edu.searchcriteria.SearchCriteria;
import edu.vo.Course;
import edu.vo.Postgraduate;
import edu.vo.Teacher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 助教DAO接口实现类，用于具体实现对数据库中助教相关表的操作
 */
public class AssistantDaoImpl implements AssistantDao {

    /**
     * 添加某一课程的助教信息
     *
     * @param assistant 助教实体信息
     */
    @Override
    public void add(Assistant assistant) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            String sql = "INSERT INTO assistant VALUES(?,?,?,?,?,?,?);";
            connection=dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, assistant.getCourse().getId());
            statement.setString(2, assistant.getPostgraduate().getId());
            statement.setString(3, assistant.getTeacher().getId());
            statement.setString(4, assistant.getSelfDescription());
            statement.setString(5, assistant.getTeacherEvaluation());
            statement.setString(6, assistant.getEvaluationGrade());
            statement.setInt(7,assistant.getPriority());

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除某一课程的助教信息
     *
     * @param courseId 需要删除助教信息的课程号
     */
    @Override
    public void delete(String courseId) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            String sql = "DELETE FROM assistant WHERE courseId=?;";
            connection=dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, courseId);

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改某一课程的助教信息
     *
     * @param assistant 需要修改助教信息的课程所对应的新助教信息
     */
    @Override
    public void update(Assistant assistant) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            String sql = "UPDATE assistant SET postgraduateId=?,teacherId=?," +
                    "selfDescription=?,teacherEvaluation=?,evaluationGrade=?,priority=? WHERE " +
                    "courseId=?";

            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, assistant.getPostgraduate().getId());
            statement.setString(2, assistant.getTeacher().getId());
            statement.setString(3, assistant.getSelfDescription());
            statement.setString(4, assistant.getTeacherEvaluation());
            statement.setString(5, assistant.getEvaluationGrade());
            statement.setInt(6,assistant.getPriority());
            statement.setString(7, assistant.getCourse().getId());

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取某一课程的助教信息
     *
     * @param courseId 需要获取助教信息的课程号
     */
    @Override
    public Assistant get(String courseId) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        Assistant assistant = null;

        try {
            String sql = "SELECT * FROM assistant WHERE courseId=?";
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, courseId);

            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                Course course = new Course();
                course.setId(result.getString("courseId"));

                Postgraduate postgraduate = new Postgraduate();
                postgraduate.setId(result.getString("postgraduateId"));

                Teacher teacher = new Teacher();
                //teacher.setId(result.getString("teacherId"));

                assistant = new Assistant(
                        course,
                        postgraduate,
                        teacher,
                        result.getString("selfDescription"),
                        result.getString("teacherEvaluation"),
                        result.getString("evaluationGrade"),
                        result.getInt("priority"));
            }

            result.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return assistant;
    }

    /**
     * 查询符合指定条件的课程助教信息
     *
     * @param criteria 查询条件
     */
    @Override
    public List<Assistant> find(AssistantSearchCriteria criteria) {
        List<Assistant> assistants = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection connection = dbConnection.getConnection();
        PreparedStatement statement = null;
        ResultSet result = null;

        StringBuilder criteriaSql = new StringBuilder(512);
        String sql = "SELECT * FROM assistant WHERE ";
        criteriaSql.append(sql);

        //构造查询语句
        if (criteria.getCourse()!= null) {
            criteriaSql.append("courseId='" + SearchCriteria.fixSqlFieldValue(criteria.getCourse().getId()) + "' AND ");
        }
        if (criteria.getPostgraduate() != null) {
            criteriaSql.append("postgraduateId='" + SearchCriteria.fixSqlFieldValue(criteria.getPostgraduate().getId()) + "' AND ");
        }
        if (criteria.getTeacher() != null) {
            criteriaSql.append("teacherId='" + SearchCriteria.fixSqlFieldValue(criteria.getTeacher().getId()) + "' AND ");
        }
        if (criteria.getSelfDescription() != null) {
            criteriaSql.append("selfDescription='" + SearchCriteria.fixSqlFieldValue(criteria.getSelfDescription()) + "' AND ");
        }
        if (criteria.getTeacherEvaluation() != null) {
            criteriaSql.append("teacherEvaluation='" + SearchCriteria.fixSqlFieldValue(criteria.getTeacherEvaluation()) + "' AND ");
        }
        if (criteria.getEvaluationGrade() != null) {
            criteriaSql.append("evaluationGrade='" + SearchCriteria.fixSqlFieldValue(criteria.getEvaluationGrade()) + "' AND ");
        }
        if (criteria.getPriority()!=-1){
            criteriaSql.append("priority="+SearchCriteria.fixSqlFieldValue(String.valueOf(criteria.getPriority())));
        }

        // 去除多余的WHERE和AND关键字
        sql = criteria.removeRedundancy(criteriaSql);

        try {
            statement = connection.prepareStatement(sql);
            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                Course course = new Course();
                course.setId(result.getString("courseId"));

                Postgraduate postgraduate = new Postgraduate();
                postgraduate.setId(result.getString("postgraduateId"));

                Teacher teacher = new Teacher();
                //teacher.setId(result.getString("teacherId"));

                Assistant assistant = new Assistant(
                        course,
                        postgraduate,
                        teacher,
                        result.getString("selfDescription"),
                        result.getString("teacherEvaluation"),
                        result.getString("evaluationGrade"),
                        result.getInt("priority"));
                assistants.add(assistant);
            }

            result.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return assistants;
    }


    /**
     * 查询指定学科的课程助教信息
     *
     * @param major 需要查询的学科
     * @param noAssistant 是否只查询缺失助教的课程
     * @return 该学科的所有课程助教信息
     */
    public List<Assistant> findByMajor(String major,boolean noAssistant) {
        List<Assistant> assistants = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;

        try {
            String sql="";
            if (noAssistant){
                sql = "SELECT * FROM noAssistantCourseView WHERE subject=?;";
            }else {
                sql="SELECT * FROM assistantCourseView WHERE subject=?;";
            }
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, major);

            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                Assistant assistant = get(result.getString("id"));
                assistants.add(assistant);
            }

            result.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return assistants;
    }
}
