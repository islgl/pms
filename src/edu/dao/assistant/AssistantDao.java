package edu.dao.assistant;

import edu.searchcriteria.AssistantSearchCriteria;
import edu.bo.Assistant;

import java.util.List;

/**
 * 助教DAO数据库操作接口
 */
public interface AssistantDao {
    /**
     * 添加某一课程的助教信息
     * @param assistant 助教实体信息
     */
    void add(Assistant assistant);

    /**
     * 删除某一课程的助教信息
     * @param courseId 需要删除助教信息的课程号
     */
    void delete(String courseId);

    /**
     * 修改某一课程的助教信息
     * @param assistant 需要修改助教信息的课程所对应的新助教信息
     */
    void update(Assistant assistant);

    /**
     * 获取某一课程的助教信息
     * @param courseId 需要获取助教信息的课程号
     */
    Assistant get(String courseId);

    /**
     * 查询符合指定条件的课程助教信息
     * @param criteria 查询条件
     */
    List<Assistant> find(AssistantSearchCriteria criteria);
}
