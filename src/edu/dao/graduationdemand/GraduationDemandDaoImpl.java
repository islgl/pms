package edu.dao.graduationdemand;

import edu.bo.GraduationDemand;
import edu.dao.DaoFactory;
import edu.db.DbConnection;
import edu.vo.Subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Liu Guoli
 * 学科毕业要求DAO实现类
 */
public class GraduationDemandDaoImpl implements GraduationDemandDao {
    private static final String ADD_SQL = "insert into graduation_demand values(?,?,?,?)";
    private static final String DELETE_SQL = "delete from graduation_demand where subject_id=?";
    private static final String UPDATE_SQL = "update graduation_demand set subject_id=?,aspiration=?,exchange=?,expenditure=? where subject_id=?";
    private static final String GET_SQL = "select * from graduation_demand where subject_id=?";


    /**
     * 增加学科毕业要求
     *
     * @param demand 学科毕业要求信息
     */
    @Override
    public void add(GraduationDemand demand) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(ADD_SQL);

            statement.setString(1, demand.getSubject().getId());
            statement.setInt(2, demand.getAspiration());
            statement.setInt(3, demand.getExchange());
            statement.setDouble(4, demand.getExpenditure());

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除学科毕业要求
     *
     * @param subjectId 要删除的学科编号
     */
    @Override
    public void delete(String subjectId) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(DELETE_SQL);

            statement.setString(1, subjectId);

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新学科毕业要求
     *
     * @param subjectId 要更新的学科编号
     * @param demand    更新后的学科毕业要求
     */
    @Override
    public void update(String subjectId, GraduationDemand demand) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(UPDATE_SQL);

            statement.setString(1, demand.getSubject().getId());
            statement.setInt(2, demand.getAspiration());
            statement.setInt(3, demand.getExchange());
            statement.setDouble(4, demand.getExpenditure());
            statement.setString(5, subjectId);

            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取学科毕业要求
     *
     * @param subjectId 要获取的学科编号
     * @return 学科毕业要求
     */
    @Override
    public GraduationDemand get(String subjectId) {
        DbConnection dbConnection = new DbConnection();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        GraduationDemand demand = null;

        try {
            connection = dbConnection.getConnection();
            statement = connection.prepareStatement(GET_SQL);

            statement.setString(1, subjectId);

            statement.executeQuery();
            result = statement.getResultSet();

            while (result.next()) {
                Subject subject = DaoFactory.getInstance().getSubjectDao().get(result.getString("subject_id"));
                demand = new GraduationDemand(subject, result.getInt("aspiration"), result.getInt("exchange"), result.getDouble("expenditure"));
            }

            result.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return demand;
    }
}
