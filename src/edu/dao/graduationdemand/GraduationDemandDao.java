package edu.dao.graduationdemand;

import edu.bo.GraduationDemand;

/**
 * @author Liu Guoli
 * 学科毕业要求DAO
 */
public interface GraduationDemandDao {
    /**
     * 增加学科毕业要求
     *
     * @param demand 学科毕业要求信息
     */
    void add(GraduationDemand demand);

    /**
     * 删除学科毕业要求
     *
     * @param subjectId 要删除的学科编号
     */
    void delete(String subjectId);

    /**
     * 更新学科毕业要求
     *
     * @param subjectId 要更新的学科编号
     * @param demand    更新后的学科毕业要求
     */
    void update(String subjectId, GraduationDemand demand);

    /**
     * 获取学科毕业要求
     *
     * @param subjectId 要获取的学科编号
     * @return 学科毕业要求
     */
    GraduationDemand get(String subjectId);
}
