package edu.dao.tutor;

import edu.db.DbConnection;
import edu.searchcriteria.SearchCriteria;
import edu.searchcriteria.TutorSearchCriteria;
import edu.vo.Tutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TutorDAOImpl implements TutorDAO{
    private static final String TUTOR_INSERT_SQL="INSERT INTO tutor(tutor_no,tutor_name,tutor_sex," +
            "tutor_major) VALUES(?,?,?,?)";
    @Override
    public void addTutor(Tutor tutor) {
        DbConnection dbConnection=new DbConnection();
        Connection con=dbConnection.getConnection();
        try{
            PreparedStatement psmt=con.prepareStatement(TUTOR_INSERT_SQL);
            psmt.setString(1,tutor.getTutor_no());
            psmt.setString(2,tutor.getTutor_name());
            psmt.setString(3,tutor.getTutor_sex());
            psmt.setString(4,tutor.getTutor_major());
            psmt.executeUpdate();
            psmt.close();
            System.out.println("添加成功!");

        }catch(Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private static final String TUTOR_UPDATE_SQL="UPDATE tutor SET tutor_no=?,tutor_name=?," +
            "tutor_sex=?,tutor_major=? WHERE tutor_no=?";
    @Override
    public void updateTutor(Tutor tutor,String tno) {
        DbConnection dbConnection=new DbConnection();
        Connection con=dbConnection.getConnection();
        try{
            PreparedStatement psmt=con.prepareStatement(TUTOR_UPDATE_SQL);
            psmt.setString(1,tutor.getTutor_no());
            psmt.setString(2,tutor.getTutor_name());
            psmt.setString(3,tutor.getTutor_sex());
            psmt.setString(4,tutor.getTutor_major());
            psmt.setString(5,tno);

            psmt.executeUpdate();
            psmt.close();
            System.out.println("更新成功!");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    private static final String TUTOR_DELETE_SQL="DELETE FROM tutor WHERE tutor_no=?";
    @Override
    public void deleteTutor(String tutor_no) {
        DbConnection dbConnection=new DbConnection();
        Connection con=dbConnection.getConnection();
        try{
            PreparedStatement psmt=con.prepareStatement(TUTOR_DELETE_SQL);
            psmt.setString(1,tutor_no);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("删除成功!");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private static final String TUTOR_SELECT_SQL="SELECT * FROM tutor WHERE tutor_no=?";
    @Override
    public Tutor getTutor(String tutor_no) {
        DbConnection dbConnection=new DbConnection();
        Connection con=dbConnection.getConnection();
        Tutor tutor=null;
        try{
            PreparedStatement psmt=con.prepareStatement(TUTOR_SELECT_SQL);
            psmt.setString(1, tutor_no);
            psmt.executeQuery();
            ResultSet rs=psmt.getResultSet();
            while(rs.next()){
                tutor = new Tutor(
                        rs.getString("tutor_no"),
                        rs.getString("tutor_name"),
                        rs.getString("tutor_sex"),
                        rs.getString("tutor_major")
                );
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return tutor;
    }

    private static String TUTOR_SEARCH_SQL="SELECT * FROM tutor WHERE ";
    @Override
    public List<Tutor> findTutors(TutorSearchCriteria criteria) {
        List<Tutor> tutors=new ArrayList<Tutor>();
        DbConnection dbConnection=new DbConnection();
        Connection con=dbConnection.getConnection();
        PreparedStatement psmt=null;
        ResultSet rs=null;

        //构造查询语句
        StringBuilder criteriaSql=new StringBuilder(512);
        criteriaSql.append(TUTOR_SEARCH_SQL);
        if(criteria.getTutor_name()!=null){
            criteriaSql.append("tutor_no='").append(SearchCriteria.fixSqlFieldValue(criteria.getTutor_name())).
                    append("' AND ");
        }
        if(criteria.getTutor_name()!=null){
            criteriaSql.append("tutor_name='").append(SearchCriteria.fixSqlFieldValue(criteria.getTutor_name())).
                    append("' AND ");
        }
        if(criteria.getTutor_sex()!=null){
            criteriaSql.append("tutor_sex='").append(SearchCriteria.fixSqlFieldValue(criteria.getTutor_sex())).
                    append("' AND ");
        }
        if (criteria.getTutor_major()!=null){
            criteriaSql.append("tutor_major='").append(SearchCriteria.fixSqlFieldValue(criteria.getTutor_major())).
                    append("' AND ");
        }


        TUTOR_SEARCH_SQL=criteria.removeRedundancy(criteriaSql);

        try{
            psmt=con.prepareStatement(TUTOR_SEARCH_SQL);
            rs=psmt.executeQuery();
            while (rs.next()){
                Tutor tutor=new Tutor();
                tutor.setTutor_no(rs.getString("tutor_no"));
                tutor.setTutor_name(rs.getString("tutor_name"));
                tutor.setTutor_sex(rs.getString("tutor_sex"));
                tutor.setTutor_major(rs.getString("tutor_major"));
                tutors.add(tutor);
            }
            rs.close();
            psmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try{
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return tutors;
    }
}
