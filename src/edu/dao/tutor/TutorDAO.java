package edu.dao.tutor;

import edu.searchcriteria.TutorSearchCriteria;
import edu.vo.Tutor;

import java.util.List;

public interface TutorDAO {
    void addTutor(Tutor tutor);
    void updateTutor(Tutor tutor,String tno);
    void deleteTutor(String tutor_no);
    Tutor getTutor(String tutor_no);
    List<Tutor> findTutors(TutorSearchCriteria searchCriteria);
}
