package edu.dao.report;

import edu.db.DbConnection;
import edu.searchcriteria.ReportSearchCriteria;
import edu.searchcriteria.SearchCriteria;
import edu.vo.Report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ReportDAOImpl implements ReportDAO {
    /*
    * 向报告数据表中添加新数据
    * */
    @Override
    public void addReport(Report report) {
        //1编号 2学生姓名 3研究生身份 4报告名称 5报告类型 6报告服务单位 7报告时间 8贡献度排名（整数） 9佐证材料 10导师初审 11管理员终审
        String sql="insert into report(report_no,name_student,identify_student,name_report,type_report,service_report," +
                "date_report,contribution_ranking,support_materials,tutor_exam,manager_exam) values(?,?,?,?,?,?,?,?,?,?,?)";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,report.getReport_no());        //编号
            psmt.setString(2,report.getName_student());     //学生姓名
            psmt.setString(3,report.getIdentify_student()); //学生身份
            psmt.setString(4,report.getName_report());      //报告名称
            psmt.setString(5,report.getType_report());      //报告类型
            psmt.setString(6,report.getService_report());   //报告服务单位
            psmt.setString(7,report.getDate_report());      //报告时间
            psmt.setInt(8,report.getContribution_ranking());//贡献度排名
            psmt.setString(9,report.getSupport_materials());//佐证材料
            psmt.setString(10,report.getTutor_exam());      //导师初审
            psmt.setString(11,report.getManager_exam());    //管理员终审
            psmt.executeUpdate();
            psmt.close();
            System.out.println("向report表中添加数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 更新数据表中的数据
    * */
    @Override
    public void updateReport(Report report, String report_no) {
        String sql="update report set tutor_exam=?,manager_exam=? where report_no=?";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,report.getTutor_exam());       //导师初审
            psmt.setString(2,report.getManager_exam());     //管理员终审
            psmt.setString(3,report_no);                    //编号
            psmt.executeUpdate();
            psmt.close();
//            System.out.println("更新report表数据成功！！！");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据编号删除数据表中的指定数据
    * */
    @Override
    public void deleteReport(String report_no) {
        String sql="delete from report where report_no=?";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,report_no);
            psmt.executeUpdate();
            psmt.close();
            System.out.println("删除report表中数据成功!");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /*
    * 根据编号查询表中指定的一条数据并返回
    * */
    @Override
    public Report getReport(String report_no) {
        String sql="select * from report where report_no=?";
        DbConnection dbConnection=new DbConnection();
        Connection con=null;
        PreparedStatement psmt;
        Report report=new Report();
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            psmt.setString(1,report_no);
            ResultSet rs=psmt.executeQuery();
            while (rs.next()){
                report.setReport_no(rs.getString("report_no"));
                report.setName_student(rs.getString("name_student"));
                report.setIdentify_student(rs.getString("identify_student"));
                report.setName_report(rs.getString("name_report"));
                report.setType_report(rs.getString("type_report"));
                report.setService_report(rs.getString("service_report"));
                report.setDate_report(rs.getString("date_report"));
                report.setContribution_ranking(rs.getInt("contribution_ranking"));
                report.setSupport_materials(rs.getString("support_materials"));
                report.setTutor_exam(rs.getString("tutor_exam"));
                report.setManager_exam(rs.getString("manager_exam"));
                String no=rs.getString("report_no");
                String name_s=rs.getString("name_student");
                String id_s=rs.getString("identify_student");
                String name_r=rs.getString("name_report");
                String type=rs.getString("type_report");
                String date=rs.getString("date_report");
                String service=rs.getString("service_report");
                int rank=rs.getInt("contribution_ranking");
                String material=rs.getString("support_materials");
                String tutor=rs.getString("tutor_exam");
                String manager=rs.getString("manager_exam");
                System.out.println("编号:"+no+" 学生姓名:"+name_s+" 学生身份:"+id_s+" 报告名称:"+name_r+" 报告类型:"+type+
                        " 报告服务单位:"+service+" 报告发布时间:"+date+" 贡献度排名:"+rank+" 佐证材料:"+material+
                        " 导师初审:"+tutor+" 管理人员终审:"+manager);
            }
            rs.close();
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return report;
    }

    /*
    * 条件查询，会返回一个列表
    * */
    @Override
    public List<Report> findReports(ReportSearchCriteria criteria) {
        String sql="select * from report where ";
        DbConnection dbConnection=new DbConnection();
        List<Report>reports=new ArrayList<>();
        Connection con=null;
        PreparedStatement psmt;

        //构造查询语句
        StringBuilder criteriaSql=new StringBuilder(512);
        criteriaSql.append(sql);
        if (criteria.getReport_no()!=null){//编号
            criteriaSql.append("report_no='").append(SearchCriteria.fixSqlFieldValue(criteria.getReport_no())).append("' and ");
        }
        if (criteria.getName_student()!=null){//学生姓名
            criteriaSql.append("name_student='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_student())).append("' and ");
        }
        if (criteria.getIdentify_student()!=null){//研究生身份
            criteriaSql.append("identify_student='").append(SearchCriteria.fixSqlFieldValue(criteria.getIdentify_student())).append("' and ");
        }
        if (criteria.getName_report()!=null){//报告名称
            criteriaSql.append("name_report='").append(SearchCriteria.fixSqlFieldValue(criteria.getName_report())).append("' and ");
        }
        if (criteria.getType_report()!=null){//报告类型
            criteriaSql.append("type_report='").append(SearchCriteria.fixSqlFieldValue(criteria.getType_report())).append("' and ");
        }
        if (criteria.getService_report()!=null){//报告服务单位
            criteriaSql.append("service_report='").append(SearchCriteria.fixSqlFieldValue(criteria.getService_report())).append("' and ");
        }
        if (criteria.getDate_report()!=null){//报告日期
            criteriaSql.append("date_report='").append(SearchCriteria.fixSqlFieldValue(criteria.getDate_report())).append("' and ");
        }
        if (criteria.getContribution_ranking()!=0){//贡献度排名
            criteriaSql.append("contribution_ranking").append(SearchCriteria.fixSqlFieldValue
                    (String.valueOf(criteria.getContribution_ranking()))).append("' and ");
        }
        if (criteria.getSupport_materials()!=null){//佐证材料
            criteriaSql.append("support_materials").append(SearchCriteria.fixSqlFieldValue(criteria.getSupport_materials())).append("' and ");
        }
        if (criteria.getTutor_exam()!=null){//导师初审
            criteriaSql.append("tutor_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getTutor_exam())).append("' and ");
        }
        if (criteria.getManager_exam()!=null){//管理员终审
            criteriaSql.append("manager_exam='").append(SearchCriteria.fixSqlFieldValue(criteria.getManager_exam())).append("' and ");
        }
        //规范化查询语句（去掉多余的where和and及空格）
        sql=criteria.removeRedundancy(criteriaSql);
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            ResultSet rs=psmt.executeQuery();
            while (rs.next()){
                Report report=new Report();
                report.setReport_no(rs.getString("report_no"));
                report.setName_student(rs.getString("name_student"));
                report.setIdentify_student(rs.getString("identify_student"));
                report.setName_report(rs.getString("name_report"));
                report.setType_report(rs.getString("type_report"));
                report.setService_report(rs.getString("service_report"));
                report.setDate_report(rs.getString("date_report"));
                report.setContribution_ranking(rs.getInt("contribution_ranking"));
                report.setSupport_materials(rs.getString("support_materials"));
                report.setTutor_exam(rs.getString("tutor_exam"));
                report.setManager_exam(rs.getString("manager_exam"));

                reports.add(report);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return reports;
    }

    /*
    * 获取report数据表中的所有编号值*/
    @Override
    public List<String> findAllNumber() {
        String sql="select * from report";
        DbConnection dbConnection=new DbConnection();
        List<String>number=new ArrayList<>();
        Connection con=null;
        PreparedStatement psmt;
        try {
            con=dbConnection.getConnection();
            psmt=con.prepareStatement(sql);
            ResultSet rs=psmt.executeQuery();
            while (rs.next()){
                String no=rs.getString("report_no");
                number.add(no);
            }
            psmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                assert con != null;
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return number;
    }
}
