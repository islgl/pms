package edu.dao.report;

import edu.searchcriteria.ReportSearchCriteria;
import edu.vo.Report;

import java.util.List;

public interface ReportDAO {
    void addReport(Report report);
    void updateReport(Report report,String report_no);
    void deleteReport(String report_no);
    Report getReport(String report_no);
    List<Report> findReports(ReportSearchCriteria criteria);
    List<String> findAllNumber();
}
