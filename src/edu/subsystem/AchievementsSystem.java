package edu.subsystem;

import edu.bo.Achievement;
import edu.bo.ManagerExam;
import edu.bo.PostgraduateBusiness;
import edu.bo.TutorExam;
import edu.dao.DaoFactory;
import edu.vo.*;

import java.util.Scanner;

public class AchievementsSystem {
    private static final Scanner input=new Scanner(System.in);

    /*
     * 选择身份
     * 硕士研究生or博士研究生or导师or管理人员
     * */
    public static void selectRole() {
        System.out.println("-----------欢迎使用研究生成果认证子系统-----------");
        System.out.println("\t\t\t\t1.硕士研究生\n\t\t\t\t2.博士研究生\n" +
                "\t\t\t\t3.导师\n\t\t\t\t4.研究生培养管理人员\n\t\t\t\t-1.返回上一级");
        System.out.println("请选择你的角色：");
        String selection = input.next();
        switch (selection) {
            case "1":
                master();
                break;
            case "2":
                doctor();
                break;
            case "3":
                tutor();
                break;
            case "4":
                manager();
                break;
            case "-1":
                //上一个界面的函数xxxx
                System.out.println("感谢使用本系统！祝您生活愉快！再见！");
                return;
            default:
                System.out.println("输入错误！！");
                selectRole();
                break;
        }
    }

    /*
     * 硕士研究生
     * */
    public static void master() {
        System.out.println("-------------------------------------------------");
        System.out.println("\t\t\t\t0.返回上一级\n\t\t\t\t1.论文\n\t\t\t\t2.教材\n\t\t\t\t3.标准\n" +
                "\t\t\t\t4.专利\n\t\t\t\t5.报告\n\t\t\t\t6.软硬件平台开发证明");
        System.out.println("请选择您要认证的成果类型：");
        String selection = input.next();
        switch (selection) {
            case "0":
                PostgraduateBusiness.submitAchievement();
                break;
            case "1":
                Thesis thesis= Achievement.thesis();
                thesis.setIdentify_author("硕士研究生");
                DaoFactory.getInstance().getThesisDAO().addThesis(thesis);//将thesis添加到数据表中
                master();
                break;
            case "2":
                Textbook textbook=Achievement.textbook();
                textbook.setIdentify_author("硕士研究生");
                DaoFactory.getInstance().getTextbookDAO().addTextbook(textbook);
                master();
                break;
            case "3":
                Standards standard=Achievement.standard();
                standard.setIdentify_person("硕士研究生");
                DaoFactory.getInstance().getStandardDAO().addStandard(standard);
                master();
                break;
            case "4":
                Patent patent=Achievement.patent();
                patent.setIdentify_student("硕士研究生");
                DaoFactory.getInstance().getPatentDAO().addPatent(patent);
                master();
                break;
            case "5":
                Report report=Achievement.report();
                report.setIdentify_student("硕士研究生");
                DaoFactory.getInstance().getReportDAO().addReport(report);
                master();
                break;
            case "6":
                Pingtai pingtai=Achievement.pingtai();
                pingtai.setIdentify_student("硕士研究生");
                DaoFactory.getInstance().getPingtaiDAO().addPingtai(pingtai);
                master();
                break;
            default:
                System.out.println("输入错误！！");
                master();
                break;
        }
    }

    /*
     * 博士研究生
     * */
    public static void doctor() {
        System.out.println("-------------------------------------------------");
        System.out.println("\t\t\t\t0.返回上一级\n\t\t\t\t1.论文\n\t\t\t\t2.奖励\n\t\t\t\t3.教材\n" +
                "\t\t\t\t4.标准\n\t\t\t\t5.专利\n\t\t\t\t6.报告\n\t\t\t\t7.软硬件平台开发证明");
        System.out.println("请选择您想要认证的成果类型：");
        String selection = input.next();
        switch (selection) {
            case "0":
                PostgraduateBusiness.submitAchievement();
                break;
            case "1"://论文
                Thesis thesis=Achievement.thesis();
                thesis.setIdentify_author("博士研究生");
                DaoFactory.getInstance().getThesisDAO().addThesis(thesis);//将thesis添加到数据表中
                doctor();
                break;
            case "2"://奖励
                Reward reward=Achievement.reward();
                DaoFactory.getInstance().getRewardDAO().addReward(reward);//将reward添加到数据表中
                doctor();
            case "3"://教材
                Textbook textbook=Achievement.textbook();
                textbook.setIdentify_author("博士研究生");
                DaoFactory.getInstance().getTextbookDAO().addTextbook(textbook);
                doctor();
                break;
            case "4"://标准
                Standards standard=Achievement.standard();
                standard.setIdentify_person("博士研究生");
                DaoFactory.getInstance().getStandardDAO().addStandard(standard);
                doctor();
                break;
            case "5"://专利
                Patent patent=Achievement.patent();
                patent.setIdentify_student("博士研究生");
                DaoFactory.getInstance().getPatentDAO().addPatent(patent);
                doctor();
                break;
            case "6"://报告
                Report report=Achievement.report();
                report.setIdentify_student("博士研究生");
                DaoFactory.getInstance().getReportDAO().addReport(report);
                doctor();
                break;
            case "7"://软硬件平台开发
                Pingtai pingtai=Achievement.pingtai();
                pingtai.setIdentify_student("博士研究生");
                DaoFactory.getInstance().getPingtaiDAO().addPingtai(pingtai);
                doctor();
                break;
            default:
                System.out.println("输入错误！！");
                doctor();
                break;
        }
    }


    /*
    * 导师
    * 完成研究生成果的初步审核
    * */
    public static void tutor(){
        System.out.println("-------------------------------------------------");
        System.out.println("\t\t\t\t0.返回上一级\n\t\t\t\t1.论文\n\t\t\t\t2.奖励\n\t\t\t\t3.教材\n" +
                "\t\t\t\t4.标准\n\t\t\t\t5.专利\n\t\t\t\t6.报告\n\t\t\t\t7.软硬件平台开发证明");
        System.out.println("请选择您审核的成果类型：");
        String selection = input.next();
        switch (selection) {
            case "0":
                // selectRole();
                break;
            case "1":
                TutorExam.exam_thesis();    //论文
                tutor();
                break;
            case "2":
                TutorExam.exam_reward();    //奖励（博士）
                tutor();
                break;
            case "3":
                TutorExam.exam_textbook();  //教材
                tutor();
                break;
            case "4":
                TutorExam.exam_standard();  //标准
                tutor();
                break;
            case "5":
                TutorExam.exam_patent();    //专利
                tutor();
                break;
            case "6":
                TutorExam.exam_report();    //报告
                tutor();
                break;
            case "7":
                TutorExam.exam_pingtai();   //软硬件平台开发
                tutor();
                break;
            default:
                System.out.println("输入错误！！");
                tutor();
                break;
        }
    }


    /*
    * 研究生管理员
    * 完成研究生成果认证的最终审核
    * */
    public static void manager(){
        System.out.println("-------------------------------------------------");
        System.out.println("\t\t\t\t0.返回上一级\n\t\t\t\t1.论文\n\t\t\t\t2.奖励\n\t\t\t\t3.教材\n" +
                "\t\t\t\t4.标准\n\t\t\t\t5.专利\n\t\t\t\t6.报告\n\t\t\t\t7.软硬件平台开发证明");
        System.out.println("请选择您审核的成果类型：");
        String selection = input.next();
        switch (selection) {
            case "0":
                // selectRole();
                break;
            case "1":
                ManagerExam.exam_thesis();  //教材
                manager();
                break;
            case "2":
                ManagerExam.exam_reward();  //奖励（博士）
                manager();
                break;
            case "3":
                ManagerExam.exam_textbook();    //教材
                manager();
                break;
            case "4":
                ManagerExam.exam_standard();    //标准
                manager();
                break;
            case "5":
                ManagerExam.exam_patent();      //专利
                manager();
                break;
            case "6":
                ManagerExam.exam_report();      //报告
                manager();
                break;
            case "7":
                ManagerExam.exam_pingtai();     //软硬件平台开发
                manager();
            default:
                System.out.println("输入错误！");
                manager();
                break;
        }
    }

}

