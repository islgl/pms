package edu.subsystem;


import edu.bo.Activity;
import edu.dao.DaoFactory;
import edu.dao.activity.ActivityDaoImpl;
import edu.vo.Postgraduate;

import javax.swing.*;
import java.util.Scanner;

/**
 * 学术交流活动子系统
 */
public class ActivitySystem {
    public static void main(String[] args) throws Exception {
        Integer role=null;
        System.out.println("-----------欢迎使用学术交流活动子系统-----------");
        System.out.println("1.研究生培养管理员\r\n2.学科负责人\r\n3.导师\r\n4.研究生");
        System.out.println("-------------------------------------------------");
        System.out.println("你的身份是：");
        Scanner scanner = new Scanner(System.in);
        role = scanner.nextInt();
        switch (role) {
            case 1:
            case 2:
            case 3:
                testUpdateCheck();//审核学生的学术交流情况 修改check
                break;
            case 4:
            //增删改查
            System.out.println("你的学号是：");
            Scanner postgraduateid = new Scanner(System.in);
            String id=postgraduateid.next();
            Postgraduate postgraduate= DaoFactory.getPostgraduateDao().get(id);
            System.out.println("1.增\r\n2.删\r\n3.改\r\n4.查\r\n5.退出");
            System.out.println("-------------------------------------------------");
            Scanner scanner3 = new Scanner(System.in);
            role = scanner3.nextInt();
            switch (role) {
                case 1:
                    Postgraduateadd(postgraduate);
                    break;
                case 2:
                    System.out.println("您的学术交流活动情况为：");
                    Postgraduateget(id);
                    System.out.println("请输入要删除的活动序号：");
                    Scanner a_count=new Scanner(System.in);
                    Postgraduatedelete(id,a_count.nextInt());
                    break;
                case 3:
                    System.out.println("您的学术交流活动情况为：");
                    Postgraduateget(id);
                    System.out.println("请输入要更改的活动序号：");
                    Scanner a_count2=new Scanner(System.in);
                    Postgraduateupdate(id,a_count2.nextInt());
                    break;
                case 4:
                    System.out.println("您的学术交流活动情况为：");
                    Postgraduateget(id);
                    break;
                case 5:
                    break;
        default:
            break;
            }
        }
    }

    public static void Postgraduateadd(Postgraduate postgraduate) {
        System.out.println("请输入活动次数");
        Scanner str0=new Scanner(System.in);
        Integer a_count=str0.nextInt();
        System.out.println("请输入活动名称");
        Scanner str1=new Scanner(System.in);
        String a_name=str1.next();
        System.out.println("请输入活动地点");
        Scanner str2=new Scanner(System.in);
        String a_place=str2.next();
        System.out.println("请输入活动时间");
        Scanner str3=new Scanner(System.in);
        String a_time=str3.next();
        System.out.println("请输入报告中英文名称");
        Scanner str4=new Scanner(System.in);
        String a_CEname=str4.next();
        System.out.println("请输入备注");
        Scanner str5=new Scanner(System.in);
        String a_note=str5.next();
        Activity activity = new Activity(postgraduate.getId(),postgraduate.getName(),postgraduate.getMajor(),a_count,a_name,a_place,a_time,a_CEname,a_note,0);
        new ActivityDaoImpl().addActivity(activity);
        JOptionPane.showMessageDialog(null,"请确保已保存至少两次学术交流活动！","提示",JOptionPane.PLAIN_MESSAGE);
    }

    public static void Postgraduatedelete(String id, Integer a_count){
        DaoFactory.getInstance().getActivityDao().deleteActivity(id,a_count);
    }

    public static void Postgraduateupdate(String id, Integer a_count){
        DaoFactory.getInstance().getActivityDao().deleteActivity(id,a_count);
        Postgraduate postgraduate= DaoFactory.getPostgraduateDao().get(id);
        System.out.println("请输入更新后的活动名称");
        Scanner str1=new Scanner(System.in);
        String a_name=str1.next();
        System.out.println("请输入更新后的活动地点");
        Scanner str2=new Scanner(System.in);
        String a_place=str2.next();
        System.out.println("请输入更新后的活动时间");
        Scanner str3=new Scanner(System.in);
        String a_time=str3.next();
        System.out.println("请输入更新后的报告中英文名称");
        Scanner str4=new Scanner(System.in);
        String a_CEname=str4.next();
        System.out.println("请更新后的输入备注");
        Scanner str5=new Scanner(System.in);
        String a_note=str5.next();
        Activity activity = new Activity(id, postgraduate.getName(),postgraduate.getMajor(),a_count,a_name,a_place,a_time,a_CEname,a_note,0);
        new ActivityDaoImpl().addActivity(activity);
    }

    public static void Postgraduateget(String id){
        Activity activity = new ActivityDaoImpl().getActivity(id);
    }

    public static void testUpdateCheck() throws Exception {
        System.out.println("您要审核的学生学号为：");
        Scanner inid=new Scanner(System.in);
        String id=inid.next();
        System.out.println("此学生提交的学术交流活动情况如下：");
        Activity activity = new ActivityDaoImpl().getActivity(id);
        System.out.println("您要审核的活动序号为：");
        Scanner incount=new Scanner(System.in);
        Integer a_count=incount.nextInt();
        System.out.println("请审核此次交流活动成果（1为合格，0为不合格）");
        Scanner incheck=new Scanner(System.in);
        Integer check=incheck.nextInt();
        DaoFactory.getInstance().getActivityDao().updateCheck(id,a_count,check);
    }


}
