package edu.subsystem;

import edu.bo.Assistant;
import edu.dao.DaoFactory;
import edu.searchcriteria.CourseSearchCriteria;
import edu.searchcriteria.PostgraduateSearchCriteria;
import edu.vo.Course;
import edu.vo.Postgraduate;
import edu.vo.Teacher;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * 研究生助教工作子系统
 */
public class AssistantSystem {
    /**
     * 研究生填报志愿
     *
     * @param postgraduate 填报志愿的研究生对象
     */
    public void selectCourses(Postgraduate postgraduate) {
        List<Course> courses = DaoFactory.getCourseDao().getSortedCourses();

        //输出当前可选课程信息
        System.out.println("当前可选择的课程情况如下:");
        System.out.println("课程号, 课程名, 授课人数, 学科, 授课对象, 课程类型, 授课教师, 授课时间");
        for (Course course : courses) {
            String courseInfo = course.getId().replace(" ", "") + ", " + course.getName().replace(" ", "") + ", " + course.getStudent_num() + ", "
                    + course.getSubject() + ", " + course.getProperties() + ", " + course.getPrelect_object() + ", "
                    + course.getTeacher() + ", " + course.getTime();
            System.out.println(courseInfo);
        }

        //研究生填报助教志愿
        Scanner in = new Scanner(System.in);
        System.out.println("请输入志愿1的课程号:");
        String application1 = in.next();
        System.out.println("请输入志愿2的课程号:");
        String application2 = in.next();

        //将志愿信息更新至数据库
        postgraduate.setApplications(new String[]{application1, application2});
        DaoFactory.getPostgraduateDao().update(postgraduate.getId(), postgraduate);

        System.out.println("志愿填报成功!");
    }

    /**
     * 教师(课程)选择助教
     */
    public void selectAssistant(Teacher teacher) {
        // 查询该教师讲授的所有课程
        String teacherId = teacher.getId();
        CourseSearchCriteria criteria = new CourseSearchCriteria(null, null, -1, null, null, null,
                teacherId, null, -1);
        List<Course> courses = DaoFactory.getCourseDao().findCourse(criteria);
        System.out.println("您目前讲授" + courses.size() + "门课程:");
        int i = 0;
        for (Course course : courses) {
            System.out.println("序号: " + (++i) + ", 课程号: " + course.getId().replace(" ", "") +
                    ", 课程名: " + course.getName().replace(" ", ""));
        }

        // 选择要进行助教选择的课程
        System.out.println("请输入要进行助教选择的课程的序号:");
        Scanner in = new Scanner(System.in);
        int index = in.nextInt();
        Course course = courses.get(index - 1);

        // 获取并显示填报了该课程志愿的学生名单
        List<Postgraduate> postgraduates = DaoFactory.getPostgraduateDao().getUnallocated(course);
        System.out.println("填报了该课程作为志愿的学生如下:");
        for (Postgraduate postgraduate : postgraduates) {
            System.out.println(postgraduate);
        }

        // 教师选择学生作为助教
        System.out.println("请输入您要选择的学生的学号:");
        String postgraduateId = in.next();

        //获取研究生信息
        Postgraduate postgraduate = DaoFactory.getPostgraduateDao().get(postgraduateId);

        //更新研究生表中的落选志愿信息
        flushAspiration(postgraduate, course);

        // 更新研究生表中被选学生的分配状态为已分配
        postgraduate.setAllot(1);
        DaoFactory.getPostgraduateDao().update(postgraduateId, postgraduate);

        // 更新助教表中该课程的助教信息
        Assistant assistant = DaoFactory.getAssistantDao().get(course.getId().replace(" ", ""));
        assistant.setPostgraduate(postgraduate);
        DaoFactory.getAssistantDao().update(assistant);

        System.out.println("助教信息更新成功!");
    }

    /**
     * 刷新研究生的志愿信息
     */
    public void flushAspiration(Postgraduate postgraduate, Course course) {
        //获取所有研究生信息
        PostgraduateSearchCriteria postgraduateSearchCriteria = new PostgraduateSearchCriteria(null,null,null, course.getId(), course.getId(),-1,0);
        List<Postgraduate> postgraduates = DaoFactory.getPostgraduateDao().find_OR(postgraduateSearchCriteria);

        //更新所有落选研究生对应志愿
        for(Postgraduate graduate : postgraduates){
            if(!graduate.getId().equals(postgraduate.getId())){
                String[] applications = graduate.getApplications();
                if(applications[0].equals(course.getId())){
                    graduate.setApplications(new String[]{null, applications[1]});
                }
                if(applications[1].equals(course.getId())){
                    graduate.setApplications(new String[]{applications[0], null});
                }
                DaoFactory.getPostgraduateDao().update(graduate.getId(), graduate);
            }
        }
    }

    /**
     * 对没有分配助教工作的学生分配助教课程
     *
     * @param postgraduate 需要进行助教工作分配的研究生对象
     */
    public void allotAssistant(Postgraduate postgraduate) {
        // 查询该学生所在学科尚无助教的课程
        String major = postgraduate.getMajor();
        List<Assistant> assistants = DaoFactory.getAssistantDao().findByMajor(major, true);

        // 如果没有相应课程则报错
        if (assistants.size() == 0) {
            System.out.println("抱歉，" + major + "学科下暂无缺少助教的课程，无法为" + postgraduate.getName() + "同学分配助教工作!");
            return;
        }

        // 随机为该学生分配一个课程
        int randomIndex = new Random().nextInt(assistants.size());
        Assistant assistant = assistants.get(randomIndex);

        // 更新相关信息
        postgraduate.setAllot(1);
        DaoFactory.getPostgraduateDao().update(postgraduate.getId(), postgraduate);
        assistant.setPostgraduate(postgraduate);
        DaoFactory.getAssistantDao().update(assistant);
    }

    /**
     * 对所有没有分配助教工作的学生分配助教
     */
    public void allotAll() {
        List<Postgraduate> postgraduates = DaoFactory.getPostgraduateDao().getAllUnallocated();
        for (Postgraduate postgraduate : postgraduates) {
            allotAssistant(postgraduate);
        }
        System.out.println("助教分配工作完成!");
    }

    /**
     * 研究生填写助教工作自述
     */
    public void fillDescription(Postgraduate postgraduate) {
        //查询研究生的所有助教课程
        List<Course> courses = new ArrayList<>();
        CourseSearchCriteria searchCriteria = new CourseSearchCriteria(null, null, -1, null, null, null, null, postgraduate.getId(), 0-1);
        courses = DaoFactory.getCourseDao().findCourse(searchCriteria);

        //显示该学期个人助教课程列表
        System.out.println("本学期助教课程:");
        for(Course course : courses){
            System.out.println(course.getId() + "  " + course.getName());
        }

        //选择评教课程
        System.out.println("请输入你选择评教的课程号:");
        Scanner in = new Scanner(System.in);
        String courseId = in.nextLine();

        //判断输入是否正确,课程号是否存在
        while(true) {
            int i = 0;
            for (Course course : courses) {
                if (!course.getId().equals(String.valueOf(in))) {
                    ++i;
                }
            }
            if (i == 2) {
                System.out.println("你输入的课程号有误, 请重新输入(按q键退出):");
                courseId = in.nextLine();
                if(String.valueOf(in).equals("q") | String.valueOf(in).equals("Q")){
                    break;
                }
            }
            else{
                break;
            }
        }

        //获取研究生自述
        System.out.println("请输入自述:");
        String content = in.next();

        //更新助教表
        Assistant assistant = DaoFactory.getAssistantDao().get(courseId);
        assistant.setSelfDescription(content);
        DaoFactory.getAssistantDao().update(assistant);

        //输出成功提示
        System.out.println("自评录入成功!");
    }

    /**
     * 教师填写研究生助教工作评定表
     */
    public void fillAssessment(Teacher teacher) {
        // 查询该教师讲授的所有课程
        String teacherId = teacher.getId();
        CourseSearchCriteria criteria = new CourseSearchCriteria(null, null, -1, null, null, null,
                teacherId, null, -1);
        List<Course> courses = DaoFactory.getCourseDao().findCourse(criteria);
        System.out.println("您目前讲授" + courses.size() + "门课程:");
        int i = 0;
        for (Course course : courses) {
            System.out.println("序号: " + (++i) + ", 课程号: " + course.getId().replace(" ", "") +
                    ", 课程名: " + course.getName().replace(" ", ""));
        }

        // 选择要进行评价的课程
        System.out.println("请输入要进行评价的课程的序号:");
        Scanner in = new Scanner(System.in);
        int index = in.nextInt();
        Course course = courses.get(index - 1);

        //检查该课程是否有助教
        Assistant assistant = DaoFactory.getAssistantDao().get(course.getId());
        if (assistant.getPostgraduate().getId() == null) {
            System.out.println("抱歉，该课程没有助教，无法填写评价!");
            return;
        }

        //教师输入评价
        System.out.println("请输入评价:");
        String evaluation = in.next();
        System.out.println("请输入评价结果(1表示合格，0表示不合格):");
        int grade = in.nextInt();

        //将评价录入数据库

        assistant.setTeacherEvaluation(evaluation);
        assistant.setEvaluationGrade(grade == 1 ? "合格" : "不合格");
        DaoFactory.getAssistantDao().update(assistant);

        System.out.println("评价录入成功!");
    }


    public static void main(String[] args) {
        AssistantSystem system = new AssistantSystem();
        Postgraduate postgraduate=DaoFactory.getPostgraduateDao().get("0433");
        system.selectCourses(postgraduate);
    }

}
