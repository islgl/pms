package edu.subsystem;
import edu.vo.Project;
import edu.dao.DaoFactory;
import edu.vo.Expenditure;

import java.util.List;
import java.util.Scanner;
//研究生参与项目认定表
public class ProjectSystem {
    private static final Scanner in=new Scanner(System.in);

    public void Project(){
        List<String> project = DaoFactory.getProjectDao().findall();
        Project projects = new Project();
        Expenditure exp = new Expenditure();

        System.out.println("请输入学生编号:");
        String sid = in.next();
        projects.setSid(sid);

        System.out.println("请输入学生姓名:");
        String sname = in.next();
        projects.setSname(sname);

        System.out.println("请输入学生学科:");
        String smajor = in.next();
        projects.setSmajor(smajor);

        System.out.println("请输入项目编号:");
        String pid = in.next();
        while (project.contains(pid)){
            System.out.println("该项目已存在，请重新输入：");
            System.out.println("请输入项目编号：");
            pid=in.next();
        }
        projects.setPid(pid);
        exp.setId(pid);


        System.out.println("请输入项目类型:");
        String ptype = in.next();
        projects.setPtype(ptype);

        System.out.println("请输入项目名称:");
        String pname = in.next();
        projects.setPname(pname);

        System.out.println("请输入参与项目时间（格式：xxxx.x-xxxx.x）:");
        String ptime = in.next();
        projects.setPtime(ptime);

        System.out.println("请输入承担工作:");
        String pwork = in.next();
        projects.setPwork(pwork);

        System.out.println("请输入承担工作的折和经费（万元）:");
        float pexp = in.nextFloat();
        projects.setPexp(pexp);
        exp.setExpenditure(pexp);

        System.out.println("指导老师是否签字（签字为1，未签字为0:");
        int t_signature = in.nextInt();
        exp.setTeacher_signature(t_signature);

        System.out.println("项目负责人是否签字（签字为1，未签字为0:");
        int p_signature = in.nextInt();
        exp.setPrincipal_signature(p_signature);

        if(pexp == exp.getExpenditure() && (exp.getTeacher_signature()!=1 || exp.getPrincipal_signature()!=1)){
            System.out.println("录入失败，经费签字认证未通过!");
        } else if (pexp != exp.getExpenditure()) {
            System.out.println("录入失败，经费经额不一致!");
        } else {
            projects.setPexp(pexp);

            System.out.println("请输入导师编号:");
            String tid = in.next();
            projects.setTid(tid);
            //将认定表信息更新至数据库
            DaoFactory.getProjectDao().addproject(projects);
            //将认定表签名信息更新至数据库
            DaoFactory.getExpenditureDao().addexpenditure(exp);
        }
    }


    public static void main(String[] args) {
        ProjectSystem system = new ProjectSystem();
        system.Project();
    }
}
