package edu.vo;

import java.util.Arrays;
import java.util.Objects;

/**
 * 研究生类
 */
public class Postgraduate {
    private String id;
    private String name;
    private String major;
    private String tutorid;
    /**
     * 助教志愿，2个课程号的数组
     */
    private String[] applications;
    /**
     * 研究生类型
     * 1 博士
     * 2 学硕
     * 3 专硕
     */
    private int type;
    /**
     * 是否已分配助教工作
     * 0 未分配
     * 1 已分配
     */
    private int allot;

    /**
     * 该研究生对应用户的用户名
     */
    private String username;


    public Postgraduate() {
        id = null;
        name = null;
        major = null;
        applications = new String[2];
        type = -1;
        allot = 0;
        username=null;
    }

    public Postgraduate(String id, String name, String major, String tutorid, String[] applications, int type, int allot) {
        this.id = id;
        this.name = name;
        this.major = major;
        this.tutorid = tutorid;
        this.applications = applications;
        this.type = type;
        this.allot = allot;
    }

    public Postgraduate(String id, String name, String major, int type) {
        this.id = id;
        this.name = name;
        this.major = major;
        this.applications=new String[2];
        this.type = type;
        this.allot=0;
    }

    public Postgraduate(String id, String name, String major, String[] applications, int type, int allot) {
        this.id = id;
        this.name = name;
        this.major = major;
        this.applications = applications;
        this.type = type;
        this.allot = allot;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String[] getApplications() {
        return applications;
    }

    public void setApplications(String[] applications) {
        this.applications = applications;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAllot() {
        return allot;
    }

    public void setAllot(int allot) {
        this.allot = allot;
    }

    public String getTutorid() {
        return tutorid;
    }

    public void setTutorid(String tutorid) {
        this.tutorid = tutorid;
    }

    @Override
    public String toString() {
        String typeStr = "";
        switch (type) {
            case 1 -> typeStr = "博士研究生";
            case 2 -> typeStr = "学术型硕士";
            case 3 -> typeStr = "专业学位型硕士";
            default -> {
            }
        }
        return "Postgraduate{" +
                "学号='" + id + '\'' +
                ", 姓名='" + name + '\'' +
                ", 专业='" + major + '\'' +
                ", 研究生类别='" + typeStr + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Postgraduate that)) {
            return false;
        }
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static void main(String[] args) {
        // 测试创建学生
        Postgraduate postgraduate = new Postgraduate("0433", "张三", "金融学", 2);
        System.out.println(postgraduate);
    }
}
