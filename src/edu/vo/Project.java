package edu.vo;
import edu.vo.Postgraduate;
import edu.vo.Course;
public class Project {
    private String sid; //学生编号
    private String sname; //学生姓名
    private String smajor;//学生专业
    private String pid;  //项目编号
    private String ptype; //项目类型
    private String pname; //项目名称
    private String ptime; //参与实间
    private String pwork; //承担工作
    private float pexp; //折合经费
    private String tid; //导师编号

    public Project(){
        sid = null;
        sname = null;
        smajor = null;
        pid = null;
        ptype = null;
        pname = null;
        ptime = null;
        pwork = null;
        pexp = 0;
        tid = null;
    }

    public Project(String sid, String sname, String smajor, String pid, String ptype, String pname, String ptime, String pwork, float pexp, String tid) {
        this.sid = sid;
        this.sname = sname;
        this.smajor = smajor;
        this.pid = pid;
        this.ptype = ptype;
        this.pname = pname;
        this.ptime = ptime;
        this.pwork = pwork;
        this.pexp = pexp;
        this.tid = tid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getSmajor() {
        return smajor;
    }

    public void setSmajor(String smajor) {
        this.smajor = smajor;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPtime() {
        return ptime;
    }

    public void setPtime(String ptime) {
        this.ptime = ptime;
    }

    public String getPwork() {
        return pwork;
    }

    public void setPwork(String pwork) {
        this.pwork = pwork;
    }

    public float getPexp() {
        return pexp;
    }

    public void setPexp(float pexp) {
        this.pexp = pexp;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    @Override
    public String toString() {
        return "Project{" +
                "sid='" + sid + '\'' +
                ", sname='" + sname + '\'' +
                ", smajor='" + smajor + '\'' +
                ", pid='" + pid + '\'' +
                ", ptype='" + ptype + '\'' +
                ", pname='" + pname + '\'' +
                ", ptime='" + ptime + '\'' +
                ", pwork='" + pwork + '\'' +
                ", pexp=" + pexp +
                ", tid='" + tid + '\'' +
                '}';
    }
}
