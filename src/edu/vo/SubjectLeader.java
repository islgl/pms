package edu.vo;

/**
 * 学科负责人类
 */
public class SubjectLeader {
    private String subjectleader_no;
    private String subjectleader_name;
    private String subject;

    public SubjectLeader() {
    }

    public SubjectLeader(String subjectleader_no, String subjectleader_name,String subject) {
        this.subjectleader_no = subjectleader_no;
        this.subjectleader_name = subjectleader_name;
        this.subject=subject;
    }

    public String getSubjectleader_no() {
        return subjectleader_no;
    }

    public void setSubjectleader_no(String subjectleader_no) {
        this.subjectleader_no = subjectleader_no;
    }

    public String getSubjectleader_name() {
        return subjectleader_name;
    }

    public void setSubjectleader_name(String subjectleader_name) {
        this.subjectleader_name = subjectleader_name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "工号='" + subjectleader_no + '\'' +
                ", 姓名='" + subjectleader_name + '\'' +
                ", 学科='" + subject + '\'';
    }
}