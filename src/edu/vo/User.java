package edu.vo;

/**
 * 用户类
 */
public class User {
    private String username;
    private String password;
    /**
     * 用户的角色代码，其中的每一位数字代表该用户对应的一个角色
     * 1 研究生培养管理员
     * 2 学科负责人
     * 3 授课教师
     * 4 导师
     * 5 研究生
     */
    private String roleCode;

    public User() {
        username=null;
        password=null;
        roleCode=null;
    }

    public User(String username, String password, String roleCode) {
        this.username = username;
        this.password = password;
        this.roleCode = roleCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roleCode='" + roleCode + '\'' +
                '}';
    }

    public static void main(String[] args) {

    }


}
