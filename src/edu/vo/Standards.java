package edu.vo;

public class Standards {
    //编号 申请人姓名 申请人身份 标准名称、标准级别（国家标准、省部级地方标准、行业标准、团队标准）、标准发布时间、佐证材料 导师初审 管理员终审
    private String standard_no;         //编号
    private String name_person;         //申请人姓名
    private String identify_person;     //申请人身份
    private String name_standard;       //标准名称
    private String level_standard;      //标准级别
    private String date_release;        //发布时间
    private String support_materials;   //佐证材料
    private String tutor_exam;          //导师初审
    private String manager_exam;        //管理员终审

    public Standards() {
    }

    public Standards(String standard_no, String name_person, String identify_person, String name_standard,
                    String level_standard, String date_release, String support_materials,
                    String tutor_exam, String manager_exam) {
        this.standard_no = standard_no;
        this.name_person = name_person;
        this.identify_person = identify_person;
        this.name_standard = name_standard;
        this.level_standard = level_standard;
        this.date_release = date_release;
        this.support_materials = support_materials;
        this.tutor_exam = tutor_exam;
        this.manager_exam = manager_exam;
    }

    public String getStandard_no() {
        return standard_no;
    }

    public void setStandard_no(String standard_no) {
        this.standard_no = standard_no;
    }

    public String getName_person() {
        return name_person;
    }

    public void setName_person(String name_person) {
        this.name_person = name_person;
    }

    public String getIdentify_person() {
        return identify_person;
    }

    public void setIdentify_person(String identify_person) {
        this.identify_person = identify_person;
    }

    public String getTutor_exam() {
        return tutor_exam;
    }

    public void setTutor_exam(String tutor_exam) {
        this.tutor_exam = tutor_exam;
    }

    public String getManager_exam() {
        return manager_exam;
    }

    public void setManager_exam(String manager_exam) {
        this.manager_exam = manager_exam;
    }

    public String getName_standard() {
        return name_standard;
    }

    public void setName_standard(String name_standard) {
        this.name_standard = name_standard;
    }

    public String getLevel_standard() {
        return level_standard;
    }

    public void setLevel_standard(String level_standard) {
        this.level_standard = level_standard;
    }

    public String getDate_release() {
        return date_release;
    }

    public void setDate_release(String date_release) {
        this.date_release = date_release;
    }

    public String getSupport_materials() {
        return support_materials;
    }

    public void setSupport_materials(String support_materials) {
        this.support_materials = support_materials;
    }

    @Override
    public String toString() {
        return "Standard{" +
                "standard_no='" + standard_no + '\'' +
                ", name_person='" + name_person + '\'' +
                ", identify_person='" + identify_person + '\'' +
                ", name_standard='" + name_standard + '\'' +
                ", level_standard='" + level_standard + '\'' +
                ", date_release='" + date_release + '\'' +
                ", support_materials='" + support_materials + '\'' +
                ", tutor_exam='" + tutor_exam + '\'' +
                ", manager_exam='" + manager_exam + '\'' +
                '}';
    }
}
