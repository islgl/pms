package edu.vo;

public class  Administrator {
    /**
     * 管理员
     */
    private String adminid;   //管理员序号
    private String adminame;  //名称

    public Administrator(String adminid, String adminame) {
        this.adminid = adminid;
        this.adminame = adminame;
    }

    public Administrator() {

    }

    public String getAdminid() {
        return adminid;
    }

    public void setAdminid(String adminid) {
        this.adminid = adminid;
    }

    public String getAdminame() {
        return adminame;
    }

    public void setAdminame(String adminame) {
        this.adminame = adminame;
    }

    @Override
    public String toString() {
        return "Administrator{" +
                "adminid='" + adminid + '\'' +
                ", adminame='" + adminame + '\'' +
                '}';
    }
}
