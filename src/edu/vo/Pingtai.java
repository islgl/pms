package edu.vo;

public class Pingtai {
    //名称、平台服务单位、平台上线时间、贡献度（整数）、佐证材料
    private String pingtai_no;
    private String name_student;
    private String identify_student;
    private String pingtai_name;
    private String service_unit;
    private String public_date;
    private int contribution;
    private String zuozheng;
    private String tutor_exam;
    private String manager_exam;

    public Pingtai(String pingtai_no, String name_student, String identify_student, String pingtai_name,
                   String service_unit, String public_date, int contribution, String zuozheng,
                   String tutor_exam, String manager_exam) {
        this.pingtai_no = pingtai_no;
        this.name_student = name_student;
        this.identify_student = identify_student;
        this.pingtai_name = pingtai_name;
        this.service_unit = service_unit;
        this.public_date = public_date;
        this.contribution = contribution;
        this.zuozheng = zuozheng;
        this.tutor_exam = tutor_exam;
        this.manager_exam = manager_exam;
    }

    public Pingtai() {

    }

    public String getName_student() {
        return name_student;
    }

    public void setName_student(String name_student) {
        this.name_student = name_student;
    }

    public String getIdentify_student() {
        return identify_student;
    }

    public void setIdentify_student(String identify_student) {
        this.identify_student = identify_student;
    }

    public String getPingtai_no() {
        return pingtai_no;
    }

    public void setPingtai_no(String pingtai_no) {
        this.pingtai_no = pingtai_no;
    }

    public int getContribution() {
        return contribution;
    }

    public void setContribution(int contribution) {
        this.contribution = contribution;
    }

    public String getPingtai_name() {
        return pingtai_name;
    }

    public void setPingtai_name(String pingtai_name) {
        this.pingtai_name = pingtai_name;
    }

    public String getService_unit() {
        return service_unit;
    }

    public void setService_unit(String service_unit) {
        this.service_unit = service_unit;
    }

    public String getPublic_date() {
        return public_date;
    }

    public void setPublic_date(String public_date) {
        this.public_date = public_date;
    }

    public String getZuozheng() {
        return zuozheng;
    }

    public void setZuozheng(String zuozheng) {
        this.zuozheng = zuozheng;
    }

    public String getTutor_exam() {
        return tutor_exam;
    }

    public void setTutor_exam(String tutor_exam) {
        this.tutor_exam = tutor_exam;
    }

    public String getManager_exam() {
        return manager_exam;
    }

    public void setManager_exam(String manager_exam) {
        this.manager_exam = manager_exam;
    }

    @Override
    public String toString() {
        return "Pingtai{" +
                "pingtai_no='" + pingtai_no + '\'' +
                ", name_student='" + name_student + '\'' +
                ", identify_student='" + identify_student + '\'' +
                ", pingtai_name='" + pingtai_name + '\'' +
                ", service_unit='" + service_unit + '\'' +
                ", public_date='" + public_date + '\'' +
                ", contribution=" + contribution +
                ", zuozheng='" + zuozheng + '\'' +
                ", tutor_exam='" + tutor_exam + '\'' +
                ", manager_exam='" + manager_exam + '\'' +
                '}';
    }
}

