package users;

import edu.dao.DaoFactory;
import edu.vo.User;

/**
 * 用户管理模块
 */
public class UserManager {

    /**
     * 检查指定用户名的用户是否已经存在
     * @param username 需要检查的用户名
     * @return 该用户是否存在
     */
    public static boolean checkExist(String username){
        User user= DaoFactory.getUserDao().get(username);
        return user != null;
    }

    /**
     * 使用MD5算法对字符串进行加密
     * @param password 明文字符串
     * @return 密文字符串
     */
    public static String encryption(String password){
        MD5 md5=new MD5();
        md5.start(password);
        return md5.resultMessage;
    }

    /**
     * 检查指定用户的密码是否正确
     * @param username 需要检查的用户名
     * @param password 用户输入的密码
     * @return 用户密码是否正确
     */
    public static boolean checkPwd(String username,String password){
        // MD5是不可逆的加密算法，因此需要将用户输入的明文进行加密后与数据库中存储的密码进行校验
        // 获取实际密码
        User user=DaoFactory.getUserDao().get(username);
        String realPwd=user.getPassword();

        // 对密码进行加密
        password=encryption(password);

        return password.equals(realPwd);
    }

    /**
     * 根据角色代码获取对应的角色名称(该方法只能解析一位角色代码)
     * @param roleCode 一位角色代码
     * @return 对应的角色名
     */
    public static String getRole(String roleCode){
        return switch (roleCode) {
            case "1" -> "学科负责人";
            case "2" -> "授课教师";
            case "3" -> "导师";
            case "4" -> "研究生";
            case "5" -> "研究生培养管理员";
            default -> null;
        };
    }

    /**
     * 用户注册接口，供研究生培养管理员调用
     * @param username 用户名
     * @param password 用户密码
     * @param roleCode 用户角色代码
     */
    public static void register(String username,String password,String roleCode){
        // 检查该用户是否存在
        if (checkExist(username)){
            System.out.println("该用户已存在!");
            return;
        }

        // 对密码进行加密
        password=encryption(password);

        // 将用户添加至数据库
        User user=new User(username,password,roleCode);
        DaoFactory.getUserDao().add(user);
        System.out.println("注册成功!");
    }

    /**
     * 用户登录
     * @param username 用户名
     * @param password 用户密码
     * @return 用户的角色代码
     */
    public static String login(String username,String password){
        // 检查用户是否存在
        if (!checkExist(username)){
            System.out.println("该用户不存在，请先注册!");
            return null;
        }

        // 检查密码是否正确
        if (!checkPwd(username, password)){
            System.out.println("密码错误!");
            return null;
        }

        // 登录成功后选择角色
        return DaoFactory.getUserDao().get(username).getRoleCode();
    }

    public static void main(String[] args) {
        System.out.println(getRole("5"));
    }
}
