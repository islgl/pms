## 研究生培养环节和成果认定综合管理系统

[![Typing SVG](https://readme-typing-svg.demolab.com?font=Fira+Code&pause=1000&width=435&lines=%E5%AE%8C%E7%BB%93%E6%92%92%E8%8A%B1!!!%F0%9F%8E%89)](https://git.io/typing-svg)

> 该项目已于2023年初完结，2023年8月做最后一次整理。仓库仅用作合影留念及代码归档。📸

<span> 
    <a href="https://www.oracle.com/cn/java/technologies/java-se-glance.html">
        <img src="https://img.shields.io/badge/Java-17.0.5-007396?style=flat-square&logo=java&logoColor=white" alt="Java">
    </a>
    <a href="https://www.microsoft.com/zh-cn/sql-server/sql-server-downloads">
        <img src="https://img.shields.io/badge/SQL%20Server-2019-CC2927?style=flat-square&logo=microsoft-sql-server&logoColor=white" alt="MS SQL Server">
    </a>
    <a href="https://www.jetbrains.com/zh-cn/idea/">
        <img src="https://img.shields.io/badge/IntelliJ%20IDEA-2022.3.1-0003456?style=flat-square&logo=intellij-idea&logoColor=white" alt="IntelliJ IDEA">
    </a>    
</span>


<div style="display: flex; justify-content: space-between; align-items: center;">
    <img src="resources/Java.png" width=20% height=20% alt="Java">
    <img src="resources/SQL-Server.png" width=20% height=20% alt="MS SQL Server">
    <img src="resources/IDEA.png" width=15% height=15% alt="IntelliJ IDEA">
</div>

---

本项目为BJFU 2022~2023第一学期**数据库系统**课程设计小组任务，是一个基于CLI的管理系统，主要使用Java开发，数据库采用Microsoft
SQL Server。