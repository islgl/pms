CREATE TABLE dbo.postgraduate
(
    id           CHAR(4)      NOT NULL
        PRIMARY KEY,
    name         NVARCHAR(20) NOT NULL,
    major        CHAR(4)
        CONSTRAINT postgraduate_subject_id_fk
            REFERENCES dbo.subject,
    tutorid      CHAR(10)
        CONSTRAINT postgraduate_tutor_tutor_no_fk
            REFERENCES dbo.tutor,
    application1 CHAR(4),
    application2 CHAR(4),
    type         INT
        CONSTRAINT check_type
            CHECK ([type] >= 1 AND [type] <= 3),
    allot        INT
        CONSTRAINT check_allot
            CHECK ([allot] >= 0 AND [allot] <= 1),
    CONSTRAINT check_application
        CHECK ([application1] <> [postgraduate].[application2])
)
    go

