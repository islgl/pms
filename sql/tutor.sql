CREATE TABLE tutor
(tutor_no CHAR(10) NOT NULL PRIMARY KEY,
tutor_name VARCHAR(50) NOT NULL UNIQUE,
tutor_sex CHAR(10),
tutor_project VARCHAR(50);
tutor_major VARCHAR(50)
)