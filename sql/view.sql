create view noAssistantCourseView as
select id, subject,priority
        from Course,
        assistant
        where Course.id = assistant.courseId
        and assistant.postgraduateId IS NULL;

create view noAssistantPostgraduateView as
select id,name,major,application1,application2,type from postgraduate
where allot=0;

create view assistantCourseView as
select id,subject from Course,assistant where Course.id=assistant.courseId