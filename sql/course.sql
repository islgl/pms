create table Course
(
    id             nchar(10) not null
        constraint Course_pk
            primary key,
    name           nchar(50),
    student_num    int,
    subject        nchar(50),
    properties     nchar(50),
    Prelect_object nchar(50),
    teacher        nchar(50),
    time           nchar(10),
    hour           int
)
    go