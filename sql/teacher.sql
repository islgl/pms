CREATE TABLE teacher
( id CHAR(6) not null CONSTRAINT Teacher_Prim PRIMARY KEY,
  name  VARCHAR(10)not null,
  sex NCHAR(1),
  age INT,
)