create table users
(
    username varchar(20)
    constraint user_pk
    primary key,
    password varchar(32),
    roleCode varchar(5)
    )
    go