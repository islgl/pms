CREATE TABLE dbo.graduation_demand
(
    subject_id  CHAR(4) NOT NULL
        CONSTRAINT graduation_demand_pk
            PRIMARY KEY,
    aspiration  INT,
    exchange    INT,
    expenditure FLOAT,
    CONSTRAINT count_check
        CHECK ([aspiration] >= 2 AND [exchange] > 0 AND [expenditure] > 0)
)
GO

