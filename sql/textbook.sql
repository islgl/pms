create table textbook(
textbook_no nvarchar(20) not null primary key,
name_author nvarchar(50) not null,
identify_author nvarchar(50) not null,
name_textbook nvarchar(50) not null,
publish_house nvarchar(50) not null,
date_publish datetime not null,
contribution int not null,
material nvarchar(1000) not null,
tutor_exam nvarchar(50) not null,
manager_exam nvarchar(50) not null,
)