create table thesis(
thesis_no nvarchar(20) not null primary key,
name_thesis nvarchar(100) not null,
name_author nvarchar(50) not null,
identify_author nvarchar(20) not null,
name_publication nvarchar(50) not null,
state_thesis nvarchar(20) not null,
date_publication date not null,
type_index nvarchar(50) not null,
attribution_library nvarchar(50) not null,
scan_pdf nvarchar(1000) not null,
tutor_exam nvarchar(20) not null,
manager_exam nvarchar(20) not null
)