create table project1(
	sid CHAR(4) NOT NULL references postgraduate(id),
    sname NVARCHAR(20) NOT NULL,
    smajor NVARCHAR(20),
	pid char(10) NOT NULL primary key,
	ptype nvarchar(20) NOT NULL,
	pname nvarchar(20) NOT NULL,
	pdate nvarchar(20) NOT NULL,
	work nvarchar(20) NOT NULL,
	expenditure float(2) check(expenditure>=6) NOT NULL,
	tid char(10) NOT NULL references tutor(tutor_no)
	)