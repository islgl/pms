create table Activity(
    id char(4) not null,
    name nvarchar(20) not null,
    major nvarchar(20),
    a_count int not null,
    a_name nvarchar(20) not null,
    a_place nvarchar(20) not null,
    a_time date not null,
    a_CEname nvarchar(20) not null,
    a_photo image,
    a_note nvarchar(20),
    check_state int
)