create table reward(
reward_no nvarchar(20) not null primary key,
name_reward nvarchar(50) not null,
name_winner nvarchar(50) not null,
level_reward nvarchar(50) not null,
level_award nvarchar(20) not null,
ranking int not null,
date_award datetime not null,
tutor_exam nvarchar(50) not null,
manager_exam nvarchar(50) not null,
)