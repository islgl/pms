create table expenditure(
	id char(4) NOT NULL primary key,
	expenditure float(2) check(expenditure>=6) NOT NULL,
	teacher_signature int NOT NULL,
	principal_signature int NOT NULL
)