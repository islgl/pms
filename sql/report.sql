create table report(
report_no nvarchar(20) not null primary key,
name_student nvarchar(50) not null,
identify_student nvarchar(50) not null,
name_report nvarchar(50) not null,
type_report nvarchar(50) not null,
service_report nvarchar(50) not null,
date_report datetime not null,
contribution_ranking int not null,
support_materials nvarchar(1000) not null,
tutor_exam nvarchar(50) not null,
manager_exam nvarchar(50) not null
)