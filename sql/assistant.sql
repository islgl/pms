CREATE TABLE dbo.assistant
(
    courseId          CHAR(4) NOT NULL
        PRIMARY KEY,
    postgraduateId    CHAR(4)
        CONSTRAINT assistant_postgraduate_id_fk
            REFERENCES dbo.postgraduate,
    teacherId         CHAR(6)
        CONSTRAINT assistant_teacher_id_fk
        REFERENCES dbo.teacher,
    selfDescription   TEXT,
    teacherEvaluation TEXT,
    evaluationGrade   NCHAR(3),
    priority          INT DEFAULT 0
        CONSTRAINT chech_priority
            CHECK ([priority] >= 0)
    )
    go

