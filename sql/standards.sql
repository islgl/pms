create table standards(
standard_no nvarchar(20) not null primary key,
name_person nvarchar(50) not null,
identify_person nvarchar(50) not null,
name_standard nvarchar(50) not null,
level_standard nvarchar(50) not null,
date_release datetime not null,
support_materials nvarchar(1000) not null,
tutor_exam nvarchar(50) not null,
manager_exam nvarchar(50) not null,
)